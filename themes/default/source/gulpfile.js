var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minCSS = require('gulp-csso'),
    minJS = require('gulp-uglifyjs'),
    scss = require('gulp-sass');


gulp.task('style:dev', function() {
    return gulp.src([
            './vendor/fontawesome/font-awesome.scss',
            './bootstrap/style/bootstrap.scss',
            './app/style/app.scss',

            './vendor/slick/slick.css',

            './../../../modules/system/assets/css/framework.extras.css'
        ])
        .pipe(scss())
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('../assets'));
});

gulp.task('style:prod', ['style:dev'], function () {
    return gulp.src('../assets/app.min.css')
        .pipe(minCSS())
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('../assets'));
});


gulp.task('script:dev', function () {
    return gulp.src([
        './vendor/jquery/jquery-3.2.1.min.js',
        './vendor/inview/jquery.inview.js',
        // './bootstrap/script/tether.js',
        // './bootstrap/script/popper.js',
        // './bootstrap/script/bootstrap.js',
        './app/script/app.js',

        './vendor/slick/slick.js',
        // './vendor/masonry/masonry.pkgd.js',

        './../../../modules/system/assets/js/framework.js',
        './../../../modules/system/assets/js/framework.extras.js'
    ])
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('../assets'));
});

gulp.task('script:prod', ['script:dev'], function () {
    return gulp.src('../assets/app.min.js')
        .pipe(minJS())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('../assets'));
});


gulp.task('development', ['style:dev', 'script:dev']);
gulp.task('production', ['style:prod', 'script:prod']);