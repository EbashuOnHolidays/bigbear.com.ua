!function ($) {
    'use strict';

    if ($.th === undefined)
        $.th = {};

    // LIVELOADER CLASS FDEFINITION
    // ============================
    var LiveLoader = function (element, options) {
        this.options = options;
        this.$element = $(element);

        // Init
        this.init();
    };

    LiveLoader.DEFAULTS = {};

    LiveLoader.prototype.init = function () {
        // Public properties
        this.$element.on('inview', $.proxy(this.onViewChange, this));
    };

    LiveLoader.prototype.onViewChange = function (event, isInView) {
        var $element = this.$element,
            currentPage = $element.data('current-page'),
            lastPage = $element.data('last-page'),
            isComplete = currentPage >= lastPage;

        $element.toggle(!isComplete);

        if (isComplete || $element.hasClass('pager-is-loading')) {
            return;
        }

        if (isInView) {
            currentPage++;

            $element.on('ajaxSetup', function (event, context) {
                console.log('ajaxsetup');
                console.log(context.options.loading);
                context.options.loading = null;
                console.log(context.options.loading);
            })
            .addClass('pager-is-loading')
            .data('request-data', {page: currentPage})
            .request()
            .done(function () {
                $element.data('current-page', currentPage)
                .removeClass('pager-is-loading');
            });
        }
    };

    // LIVELOADER PLUGIN DEFINITION
    // ============================
    var old = $.fn.liveLoader;

    $.fn.liveLoader = function (option) {
        var args = Array.prototype.slice.call(arguments, 1), result;

        this.each(function () {
            var $this = $(this);
            var data = $this.data('oc.liveloader');
            var options = $.extend({}, LiveLoader.DEFAULTS, $this.data(), typeof option == 'object' && option);

            if (!data) {
                $this.data('oc.liveloader', (data = new LiveLoader(this, options)));
            }

            if (typeof option == 'string') {
                result = data[option].apply(data, args);
            }

            if (typeof result != 'undefined') {
                return false;
            }
        });

        return result ? result : this;
    };

    $.fn.liveLoader.Constructor = LiveLoader;

    // LIVELOADER NO CONFLICT
    // =================
    $.fn.liveLoader.noConflict = function () {
        $.fn.liveLoader = old;
        return this;
    };

    // LIVELOADER DATA-API
    // ===============
    $(document).ready(function () {
        $('[data-control="liveloader"]').liveLoader();
    });

    $(document).on('ajaxSuccess', function (event, context, data, status) {
        $('[data-control="liveloader"]').liveLoader();
    });


    /*
        Overlay Loader Class
     */
    var Loader = function () {
        this.body = $('body');
        this.overlay = $('#overlay');
        this.loader = $('#loader');
    };

    Loader.prototype.showOverlay = function () {
        this.overlay.fadeIn('fast');
    };

    Loader.prototype.showLoader = function () {
        this.loader.fadeIn('fast');
    };

    Loader.prototype.hideOverlay = function () {
        this.overlay.fadeOut('fast');
    };

    Loader.prototype.hideLoader = function () {
        this.loader.fadeOut('fast');
    };

    var $Loader = new Loader();


    var AJAXIndicator = function () {
        this.Loader = $Loader;
    };

    AJAXIndicator.prototype.show = function () {
        this.Loader.showOverlay();
        this.Loader.showLoader();
    };

    AJAXIndicator.prototype.hide = function () {
        this.Loader.hideOverlay();
        this.Loader.hideLoader();
    };

    var $AJAXIndicator = new AJAXIndicator();

    $(document).on('ajaxSetup', function (event, context) {
        context.options.flash = true;
        context.options.validation = true;
        context.options.loading = $AJAXIndicator;
    });


    /*
        Google Maps
     */
    google.maps.event.addDomListener(window, 'load', function () {
        $('[data-control="googlemaps"]').each(function (key, element) {
            var $this = $(element);

            var center = new google.maps.LatLng($this.data('latitude'), $this.data('longitude'));

            var map = new google.maps.Map(element, {
                zoom: $this.data('zoom'),
                scrollwheel: false,
                center: center
            });

            new google.maps.Marker({
                position: center,
                map: map
            });
        });
    });

    var sidebarBasket = function () {
        var overlay = $('#sidebar_basket_overlay');
        var sidebar = $('#sidebar_basket');
        var openBtn = $('.js-sidebar-basket-open');
        var closeBtn = $('.js-sidebar-basket-close');

        var funcOpen = function (event) {
            overlay.fadeIn(500);
            sidebar.addClass('sidebar-open');
        };

        var funcClose = function (event) {
            overlay.fadeOut(500);
            sidebar.removeClass('sidebar-open');
        };

        openBtn.on('click', funcOpen);
        overlay.on('click', funcClose);
        closeBtn.on('click', funcClose);
    };

    var sidebarFilter = function () {
        var overlay = $('#sidebar_filter_overlay');
        var sidebar = $('#sidebar_filter');
        var openBtn = $('.js-sidebar-filter-open');
        var closeBtn = $('.js-sidebar-filter-close');

        var funcOpen = function (event) {
            overlay.fadeIn(500);
            sidebar.addClass('sidebar-open');
        };

        var funcClose = function (event) {
            overlay.fadeOut(500);
            sidebar.removeClass('sidebar-open');
        };

        openBtn.on('click', funcOpen);
        overlay.on('click', funcClose);
        closeBtn.on('click', funcClose);
    };

    $(function () {
        if ($(window).width() > 752) {
            var overlay = $('#popup_overlay');
            var popup = $('#popup');
            var close = $('.js-popup-close');

            var funcOpen = function (event) {
                popup.css('top', Math.round($(window).height() / 2) - Math.round(popup.outerHeight() / 2));

                overlay.fadeIn(500);
                popup.fadeIn(500);
            };

            var funcClose = function (event) {
                event.preventDefault();

                overlay.fadeOut(500);
                popup.fadeOut(500);
            };

            setTimeout(funcOpen, 1000 * popup.data('delay'));

            overlay.on('click', funcClose);
            close.on('click', funcClose);
        }
    });

    var quantity = function () {
        $('.js-quantity').change(function () {
            var current = parseInt($(this).val(), 10);

            if (current > $(this).attr('max')) {
                current = parseInt($(this).attr('max'));
            } else if (current < $(this).attr('min')) {
                current = parseInt($(this).attr('min'));
            }

            $(this).val(current);
        });

        $('.js-quantity-plus').on('click', function () {
            var quantityField = $(this).next('.js-quantity');
            var current = parseInt(quantityField.val(), 10);
            var max = parseInt(quantityField.attr('max'), 10);

            if (current < max) {
                current = current + 1;
            } else {
                current = max;
            }

            quantityField.val(current);
        });

        $('.js-quantity-minus').on('click', function () {
            var quantityField = $(this).prev('.js-quantity');
            var current = parseInt(quantityField.val(), 10);
            var min = parseInt(quantityField.attr('min'), 10);

            if (current > min) {
                current = current - 1;
            } else {
                current = min;
            }

            quantityField.val(current);
        });
    };

    var countdown = function () {
        var counters = $('[data-countdown]');

        var render = function () {
            // Get todays date and time
            var now = new Date().getTime();

            $(counters).each(function () {
                var $this = $(this);

                // Set the date we're counting down to
                var countDownDate = new Date($this.data('countdown')).getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                var days = 0;
                var hours = 0;
                var minutes = 0;
                var seconds = 0;

                if (distance > 0) {
                    // Time calculations for days, hours, minutes and seconds
                    days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
                    minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
                    seconds = Math.floor(distance % (1000 * 60) / 1000);
                }

                $this.find('.days .counting').html(days);
                $this.find('.hours .counting').html(hours);
                $this.find('.minutes .counting').html(minutes);
                $this.find('.seconds .counting').html(seconds);

                var pluralDays = $this.data('plural-days').split(',');
                var pluralHours = $this.data('plural-hours').split(',');
                var pluralMinutes = $this.data('plural-minutes').split(',');
                var pluralSeconds = $this.data('plural-seconds').split(',');

                $this.find('.days .plural').html(plural(days, pluralDays[0], pluralDays[1], pluralDays[2]));
                $this.find('.hours .plural').html(plural(hours, pluralHours[0], pluralHours[1], pluralHours[2]));
                $this.find('.minutes .plural').html(plural(minutes, pluralMinutes[0], pluralMinutes[1], pluralMinutes[2]));
                $this.find('.seconds .plural').html(plural(seconds, pluralSeconds[0], pluralSeconds[1], pluralSeconds[2]));
            });
        };

        setInterval(render, 1000);

        function plural(number, one, two, five) {
            var n = Math.abs(number);

            n %= 100;

            if (n >= 5 && n <= 20) {
                return five;
            }

            n %= 10;

            if (n === 1) {
                return one;
            }

            if (n >= 2 && n <= 4) {
                return two;
            }

            return five;
        }
    };

    $(document).ready(function () {
        sidebarBasket();
        sidebarFilter();
        quantity();
        countdown();
    });

    $(document).on('ajaxSuccess', function (event, context, data, status) {
        // Перегружаем скрипты, что-бы они работали
        // после подгрузки содержимого на странице
        sidebarBasket();
        sidebarFilter();
        quantity();
        countdown();
    });


    $(function () {
        var location = window.location.href.split('?')[0].split('#')[0].replace(/\/\s*$/, '');

        $('a[href="' + location + '"]').addClass('active');
    });


    // Product slider reload
    $(function () {
        var images = $('.js-product-single-images');
        var thumbnails = $('.js-product-single-thumbnails');
        var dropdown = $('select[name="variant_id"]');

        images.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            if (typeof window.event !== 'undefined' && (window.event.type === 'click' || window.event.type === 'change')) {
                $(this).slick('slickSetOption', 'autoplay', false, 'refresh');
            }

            dropdown.prop('selectedIndex', nextSlide);
        });

        dropdown.on('change', function () {
            images.slick('slickGoTo', $(this).prop('selectedIndex'));
        });

        function sliderProductSingle() {
            images.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                asNavFor: thumbnails,
                dots: false,
                adaptiveHeight: true,
                infinite: true,
                arrows: true,
                autoplay: true,
                autoplaySpeed: 3000
            });

            thumbnails.slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: images,
                dots: false,
                focusOnSelect: true,
                infinite: true,
                centerMode: true,
                centerPadding: '0px'
            });

            if (thumbnails.slick('getSlick').slideCount < 6) {
                thumbnails.css('display', 'none');
            }
        }

        $(document).ready(function () {
            sliderProductSingle();
        });
    });

    $(function () {
        $('.instagram-slider').slick({
            adaptiveHeight: true,
            infinite: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]
        });
    });

    $(function () {
        $('.navigation-menu').each(function () {
            var $this = $(this);

            $this.find('ul').before('<a href="#" class="menu-mobile">' + $this.data('mobile-title') + '</a>');
        });

        $('.menu-mobile').on('click', function (event) {
            event.preventDefault();

            $(this).next('ul').toggleClass('show');
        });
    });

    $(document).on('ajaxSuccess', function (event, context, data, status, jqXHR) {
        if (typeof status.urlCurrentPage !== 'undefined') {
            window.history.replaceState(null, null, status.urlCurrentPage);

            // gtag('event', 'ajax');
        }

        if (typeof status.urlPrevPage !== 'undefined') {
            if ($('head > link[rel="prev"]').length === 0) {
                $('head').append('<link rel="prev">');
            }

            $('link[rel="prev"]').attr('href', status.urlPrevPage);
        } else {
            $('link[rel="prev"]').remove();
        }

        if (typeof status.urlNextPage !== 'undefined') {
            if ($('head > link[rel="next"]').length === 0) {
                $('head').append('<link rel="next">');
            }

            $('link[rel="next"]').attr('href', status.urlNextPage);
        } else {
            $('link[rel="next"]').remove();
        }

        if (typeof status.sendConversion !== 'undefined') {
            gtag('event', 'conversion', {
                'send_to': 'AW-824347047/JEBxCKCGqnsQp5OKiQM',
                'value': status.sendConversion.value,
                'currency': 'UAH',
                'transaction_id': status.sendConversion.transaction_id
            });
        }
    });

    $(document).on('ajaxPromise', '[data-scrollable]', function () {
        var scrollTo = $(this).data('scrollable');

        if (scrollTo === '') {
            $('html,body').animate({scrollTop: 0}, 200);
        } else {
            $('html,body').animate({scrollTop: $(scrollTo).offset().top - 30}, 200);
        }
    });
}(jQuery);