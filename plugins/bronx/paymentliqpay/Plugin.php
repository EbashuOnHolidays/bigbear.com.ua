<?php namespace Bronx\PaymentLiqPay;

use Backend;
use System\Classes\PluginBase;

/**
 * PaymentLiqPay Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PaymentLiqPay',
            'description' => 'No description provided yet...',
            'author'      => 'Bronx',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Bronx\PaymentLiqPay\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'bronx.paymentliqpay.some_permission' => [
                'tab' => 'PaymentLiqPay',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'paymentliqpay' => [
                'label'       => 'PaymentLiqPay',
                'url'         => Backend::url('bronx/paymentliqpay/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['bronx.paymentliqpay.*'],
                'order'       => 500,
            ],
        ];
    }
}
