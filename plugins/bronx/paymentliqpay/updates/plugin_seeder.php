<?php namespace Bronx\PaymentLiqPay\Updates;

use Bronx\Shop\Models\OrderPaymentGateway;
use October\Rain\Database\Updates\Seeder;

class Plugin_Seeder extends Seeder
{
    public function run()
    {
        $orderPaymentGateway = OrderPaymentGateway::create([
            'name'  => 'LiqPay (предоплата)',
            'class' => 'LiqPay',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Неуспешный платеж. Некорректно заполнены данные',
            'code'  => 'error',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Неуспешный платеж',
            'code'  => 'failure',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Платеж возвращен',
            'code'  => 'reversed',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Тестовый платеж',
            'code'  => 'sandbox',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Подписка успешно оформлена',
            'code'  => 'subscribed',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Успешный платеж',
            'code'  => 'success',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Подписка успешно деактивирована',
            'code'  => 'unsubscribed',
            'color' => '#4CAF50',
        ]);
    }
}