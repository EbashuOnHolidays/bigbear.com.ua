<?php namespace Bronx\Telegram;

use Bronx\Telegram\Classes\AuthenticateManager;
use Bronx\Telegram\Classes\EventListener;
use Bronx\Telegram\Components\Widget;
use Bronx\Telegram\Facades\Authenticate;
use Bronx\Telegram\Models\Settings;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\App;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    public $elevated = true;

    public function pluginDetails()
    {
        return [
            'name'        => 'bronx.telegram::lang.plugin.name',
            'description' => 'bronx.telegram::lang.plugin.description',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-telegram',
        ];
    }

    public function register()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('TelegramAuthenticate', Authenticate::class);

        App::singleton('telegram.authenticate', function () {
            return AuthenticateManager::instance();
        });
    }

    public function boot()
    {
        EventListener::boot();
    }

    public function registerComponents()
    {
        return [
            Widget::class => 'bronxTelegramWidget',
        ];
    }

    public function registerPermissions()
    {
        return [
            'telegram.settings' => [
                'tab'   => 'bronx.telegram::lang.permissions.settings.tab',
                'label' => 'bronx.telegram::lang.permissions.settings.name',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'bronx.telegram::lang.settings.name',
                'description' => 'bronx.telegram::lang.settings.description',
                'category'    => SettingsManager::CATEGORY_NOTIFICATIONS,
                'icon'        => 'icon-telegram',
                'class'       => Settings::class,
                'order'       => 400,
                'keywords'    => 'telegram notification',
                'permissions' => [
                    'telegram.settings',
                ],
            ],
        ];
    }
}