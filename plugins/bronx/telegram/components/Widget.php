<?php namespace Bronx\Telegram\Components;

use Bronx\Telegram\Models\Settings;
use Cms\Classes\ComponentBase;

class Widget extends ComponentBase
{
    public $name;
    public $size;
    public $userpic;
    public $radius;
    public $requestAccess;

    public function componentDetails()
    {
        return [
            'name'        => 'bronx.telegram::lang.components.widget.name',
            'description' => 'bronx.telegram::lang.components.widget.description',
        ];
    }

    public function defineProperties()
    {
        return [
            'size'          => [
                'title'   => 'bronx.telegram::lang.components.widget.widget_size',
                'type'    => 'dropdown',
                'default' => 'small',
                'options' => [
                    'small'  => 'bronx.telegram::lang.components.widget.small',
                    'medium' => 'bronx.telegram::lang.components.widget.medium',
                    'large'  => 'bronx.telegram::lang.components.widget.large',
                ],
            ],
            'userpic'       => [
                'title'   => 'bronx.telegram::lang.components.widget.user_picture',
                'type'    => 'checkbox',
                'default' => true,
            ],
            'radius'        => [
                'title'   => 'bronx.telegram::lang.components.widget.corner_radius',
                'default' => 20,
            ],
            'requestAccess' => [
                'title'       => 'bronx.telegram::lang.components.widget.request_access',
                'description' => 'bronx.telegram::lang.components.widget.request_access_comment',
                'type'        => 'checkbox',
                'default'     => false,
            ],
        ];
    }

    public function onRun()
    {
        $this->name = Settings::get('telegram_name', null);
        $this->size = $this->property('size');
        $this->userpic = $this->property('userpic');
        $this->radius = $this->property('radius');
        $this->requestAccess = $this->property('requestAccess');
    }
}