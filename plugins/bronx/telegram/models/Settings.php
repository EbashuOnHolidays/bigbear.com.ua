<?php namespace Bronx\Telegram\Models;

use Illuminate\Support\Facades\Event;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_telegram_settings';
    public $settingsFields = 'fields.yaml';

    use Validation;
    public $rules = [
        'telegram_name'     => 'required',
        'telegram_token'    => 'required',
        'max_connections'   => 'required|numeric|min:1|max:100',
        'is_enable_webhook' => 'boolean',

        'trace_log_input'  => 'boolean',
        'trace_log_output' => 'boolean',
    ];

    public function initSettingsData()
    {
        $this->telegram_name = null;
        $this->telegram_token = null;
        $this->max_connections = 40;
        $this->is_enable_webhook = 0;

        $this->trace_log_input = false;
        $this->trace_log_output = false;
    }

    public function afterSave()
    {
        if (Settings::get('is_enable_webhook', false) == true) {
            Event::fire('bronx.telegram.setWebhook');
        } else {
            Event::fire('bronx.telegram.deleteWebhook');
        }
    }
}