<?php

return [
    'plugin'      => [
        'name'        => 'Telegram',
        'description' => 'Integration with Telegram Bot',
    ],
    'components'  => [
        'widget' => [
            'name'                   => 'Authorization',
            'description'            => 'Widget for authorization',
            'widget_size'            => 'Widget size',
            'small'                  => 'Small',
            'medium'                 => 'Medium',
            'large'                  => 'Large',
            'user_picture'           => 'Show user\'s photo',
            'corner_radius'          => 'Corner radius',
            'request_access'         => 'Request access',
            'request_access_comment' => 'To send messages from your bot',
        ],
    ],
    'permissions' => [
        'settings' => [
            'name' => 'Manage plugin settings',
            'tab'  => 'Bronx Telegram',
        ],
    ],
    'settings'    => [
        'name'        => 'Telegram',
        'description' => 'Telegram bot settings',
        'form'        => [
            'connection_section' => [
                'label'   => 'Connection settings',
                'comment' => 'Documentations: https://core.telegram.org/bots/api#authorizing-your-bot',
            ],
            'telegram_name'      => [
                'label'       => 'Telegram bot name',
                'placeholder' => 'For example: father_bot',
            ],
            'telegram_token'     => [
                'label'       => 'Telegram bot token',
                'placeholder' => 'For example: 123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11',
            ],
            'max_connections'    => [
                'label'       => 'Maximum number of connections',
                'placeholder' => 'Maximum allowed number of simultaneous HTTPS connections to the webhook for update delivery',
            ],
            'is_enable_webhook'     => [
                'label'   => 'Webhook status',
                'comment' => 'This will allow the website to receive information sent to the Telegram bot. Verify that the Firewall is configured correctly',
                'options' => [
                    'disable' => 'Disabled',
                    'enable'  => 'Enabled',
                ],
            ],
            'trace_log_section'  => [
                'label'   => 'Logging settings',
                'comment' => 'The log will be saved in the "Event Log"',
            ],
            'trace_log_input'    => [
                'label' => 'Log inbound messages (webhook)',
            ],
            'trace_log_output'   => [
                'label' => 'Log outgoing messages',
            ],
        ],
    ],
    'form'        => [
        'tab'                 => 'Telegram',
        'telegram_id'         => 'Chat id',
        'telegram_username'   => 'Nickname',
        'telegram_first_name' => 'Name',
        'telegram_last_name'  => 'Surname',
        'telegram_photo_url'  => 'Photo url',
        'telegram_auth_date'  => 'Date of registration',
    ],
    'view'        => [
        'flash'        => [
            'login_success'  => 'Account successfully connected to Telegram',
            'logout_success' => 'Account successfully unconnected from Telegram',
            'error'          => 'Something went wrong. Repeat one more time',
        ],
        'login'        => [
            'title'       => 'Authorization required',
            'subtitle'    => 'Link your account to Telegram',
            'description' => 'This will allow you to receive notifications quickly and track the activity of your account.',
        ],
        'logout'       => [
            'title'       => 'Exit',
            'subtitle'    => 'Untie your account from Telegram',
            'description' => 'You will no longer receive notifications in Telegram. You can login again at any time',
            'logout'      => 'Logout',
        ],
        'notification' => [
            'subject_label' => 'Subject',
            'subject'       => 'Authorization in the system',
            'timestamp'     => 'Date and time',
            'login'         => 'You are logged in as',
            'ip_address'    => 'IP address',
        ],
    ],
];