<?php

return [
    'plugin'      => [
        'name'        => 'Telegram',
        'description' => 'Интеграция сайта с Telegram Bot',
    ],
    'components'  => [
        'widget' => [
            'name'                   => 'Авторизация',
            'description'            => 'Виджет для авторизации пользователей',
            'widget_size'            => 'Размер виджета',
            'small'                  => 'Маленький',
            'medium'                 => 'Средний',
            'large'                  => 'Большой',
            'user_picture'           => 'Показать фото пользователя',
            'corner_radius'          => 'Угловой радиус',
            'request_access'         => 'Запрос доступа',
            'request_access_comment' => 'Для отправки сообщений от имени бота',
        ],
    ],
    'permissions' => [
        'settings' => [
            'name' => 'Управление настройками плагина',
            'tab'  => 'Bronx Telegram',
        ],
    ],
    'settings'    => [
        'name'        => 'Telegram',
        'description' => 'Настройки Telegram Bot',
        'form'        => [
            'connection_section' => [
                'label'   => 'Настройки соединения',
                'comment' => 'Документация: https://core.telegram.org/bots/api#authorizing-your-bot',
            ],
            'telegram_name'      => [
                'label'       => 'Telegram bot name',
                'placeholder' => 'Например: father_bot',
            ],
            'telegram_token'     => [
                'label'       => 'Telegram bot token',
                'placeholder' => 'Например: 123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11',
            ],
            'max_connections'    => [
                'label'       => 'Максимальное количество соединений',
                'placeholder' => 'Максимально допустимое количество одновременных HTTPS-подключений к webhook для доставки обновлений',
            ],
            'is_enable_webhook'     => [
                'label'   => 'Статус webhook',
                'comment' => 'Это позволит веб-сайту получать информацию отправленную в Telegram bot. Убедитесь в правильной настройке Firewall',
                'options' => [
                    'disable' => 'Отключен',
                    'enable'  => 'Включен',
                ],
            ],
            'trace_log_section'  => [
                'label'   => 'Настройки ведения журнала',
                'comment' => 'Журнал будет сохранен в "Журнал событий"',
            ],
            'trace_log_input'    => [
                'label' => 'Вести журнал входящих сообщений (webhook)',
            ],
            'trace_log_output'   => [
                'label' => 'Вести журнал исходящих сообщений',
            ],
        ],
    ],
    'form'        => [
        'tab'                 => 'Telegram',
        'telegram_id'         => 'Chat id',
        'telegram_username'   => 'Ник',
        'telegram_first_name' => 'Имя',
        'telegram_last_name'  => 'Фамилия',
        'telegram_photo_url'  => 'Путь к фотографии',
        'telegram_auth_date'  => 'Дата регистрации',
    ],
    'view'        => [
        'flash'        => [
            'login_success'  => 'Аккаунт успешно позвязан к Телеграму',
            'logout_success' => 'Аккаунт успешно отвязан от Телеграма',
            'error'          => 'Что-то пошло не так. Повторите еще раз',
        ],
        'login'        => [
            'title'       => 'Требуется авторизация',
            'subtitle'    => 'Свяжите свою учетную запись с Telegram',
            'description' => 'Это позволит вам быстро получать уведомления и отслеживать активность вашей учетной записи.',
        ],
        'logout'       => [
            'title'       => 'Выход',
            'subtitle'    => 'Отвяжите свою учетную запись от Telegram',
            'description' => 'Вы больше не будете получать уведомления в телеграм. Вы сможете авторизоваться заново в любое время',
            'logout'      => 'Выйти',
        ],
        'notification' => [
            'subject_label' => 'Тема',
            'subject'       => 'Авторизация в системе',
            'timestamp'     => 'Дата и время',
            'login'         => 'Вы вошли как',
            'ip_address'    => 'IP адрес',
        ],
    ],
];