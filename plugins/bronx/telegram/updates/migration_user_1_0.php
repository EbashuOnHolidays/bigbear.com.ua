<?php namespace Bronx\Telegram\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_User_1_0 extends Migration
{
    public function up()
    {
        if (Schema::hasTable('bronx_user_tab_user')) {
            Schema::table('bronx_user_tab_user', function (Blueprint $table) {
                if (!Schema::hasColumn($table->getTable(), 'telegram_id')) {
                    $table->string('telegram_id')->index()->nullable();
                }

                if (!Schema::hasColumn($table->getTable(), 'telegram_first_name')) {
                    $table->string('telegram_first_name')->nullable();
                }

                if (!Schema::hasColumn($table->getTable(), 'telegram_last_name')) {
                    $table->string('telegram_last_name')->nullable();
                }

                if (!Schema::hasColumn($table->getTable(), 'telegram_username')) {
                    $table->string('telegram_username')->nullable();
                }

                if (!Schema::hasColumn($table->getTable(), 'telegram_photo_url')) {
                    $table->string('telegram_photo_url')->nullable();
                }

                if (!Schema::hasColumn($table->getTable(), 'telegram_auth_date')) {
                    $table->timestamp('telegram_auth_date')->nullable();
                }
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('bronx_user_tab_user')) {
            Schema::table('bronx_user_tab_user', function (Blueprint $table) {
                if (Schema::hasColumn($table->getTable(), 'telegram_id')) {
                    $table->dropColumn('telegram_id');
                }

                if (Schema::hasColumn($table->getTable(), 'telegram_first_name')) {
                    $table->dropColumn('telegram_first_name');
                }

                if (Schema::hasColumn($table->getTable(), 'telegram_last_name')) {
                    $table->dropColumn('telegram_last_name');
                }

                if (Schema::hasColumn($table->getTable(), 'telegram_username')) {
                    $table->dropColumn('telegram_username');
                }

                if (Schema::hasColumn($table->getTable(), 'telegram_photo_url')) {
                    $table->dropColumn('telegram_photo_url');
                }

                if (Schema::hasColumn($table->getTable(), 'telegram_auth_date')) {
                    $table->dropColumn('telegram_auth_date');
                }
            });
        }
    }
}