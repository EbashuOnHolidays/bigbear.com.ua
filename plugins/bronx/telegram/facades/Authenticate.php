<?php namespace Bronx\Telegram\Facades;

use October\Rain\Support\Facade;

class Authenticate extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'telegram.authenticate';
    }
}