<?php

use Backend\Facades\BackendAuth;
use Bronx\Telegram\Classes\TelegramRecipient;
use Bronx\Telegram\Facades\Authenticate;
use Bronx\Telegram\Models\Settings;
use Bronx\User\Facades\Authorization;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use October\Rain\Support\Facades\Config;
use October\Rain\Support\Facades\Flash;
use System\Models\File;

Route::group([
    'middleware' => 'web',
    'prefix'     => 'telegram',
], function () {
    Route::post('webhook', [
        'as' => 'telegram_webhook',
        function () {
            $data = json_decode(file_get_contents('php://input'), true);

            if (Settings::get('trace_log_input') == true) {
                trace_log($data);
            }

            TelegramRecipient::request($data);
        },
    ]);

    Route::get('auth', [
        'as' => 'telegram_auth',
        function () {
            // array_filter() - this function for unset null values
            $userData = array_filter([
                'id'         => Input::get('id', null),
                'first_name' => Input::get('first_name', null),
                'last_name'  => Input::get('last_name', null),
                'username'   => Input::get('username', null),
                'photo_url'  => Input::get('photo_url', null),
                'auth_date'  => Input::get('auth_date', null),
            ]);

            $userHash = [
                'hash' => Input::get('hash', null),
            ];

            $check = Authenticate::checkTelegramHash($userData, $userHash['hash']);

            if ($check === true) {
                $userCredentials = Authenticate::toUserCredentials($userData);
                $gatewayCredentials = Authenticate::toGatewayCredentials($userData);

                Authorization::authorization('telegram_id', $userCredentials, $gatewayCredentials);
            }

            return Redirect::back();
        },
    ]);
});

Route::group([
    'middleware' => 'web',
    'prefix'     => Config::get('cms.backendUri', 'backend'),
], function () {
    Route::get('telegram/auth', [
        'as' => 'telegram_auth_backend',
        function () {
            $user = BackendAuth::getUser();

            if ($user) {
                // array_filter() - this function for unset null values
                $userData = array_filter([
                    'id'         => Input::get('id', null),
                    'first_name' => Input::get('first_name', null),
                    'last_name'  => Input::get('last_name', null),
                    'username'   => Input::get('username', null),
                    'photo_url'  => Input::get('photo_url', null),
                    'auth_date'  => Input::get('auth_date', null),
                ]);

                $userHash = [
                    'hash' => Input::get('hash', null),
                ];

                $check = Authenticate::checkTelegramHash($userData, $userHash['hash']);

                if ($check === true) {
                    $gatewayCredentials = Authenticate::toGatewayCredentials($userData);

                    $user->fill($gatewayCredentials);

                    if (!$user->avatar()->first() && $user->telegram_photo_url) {
                        trace_log($user->telegram_photo_url);
                        try {
                            $file = new File;
                            $file->fromUrl($user->telegram_photo_url);

                            $user->avatar()->add($file);
                        } catch (Exception $exception) {
                            trace_log($exception->getMessage());
                        }
                    }

                    $user->save();

                    Flash::success(Lang::get('bronx.telegram::lang.view.flash.login_success'));
                } else {
                    Flash::success(Lang::get('bronx.telegram::lang.view.flash.error'));
                }
            }

            return Redirect::back();
        },
    ]);
});