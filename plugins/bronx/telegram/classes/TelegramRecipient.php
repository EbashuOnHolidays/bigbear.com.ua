<?php namespace Bronx\Telegram\Classes;

use Illuminate\Support\Facades\Event;

class TelegramRecipient
{
    public static function request($data)
    {
        if (isset($data['message'])) {
            if (isset($data['message']['reply_to_message'])) {
                Event::fire('bronx.telegram.webhook.reply_to_message', [$data]);
            }

            if (isset($data['message']['text'])) {
                Event::fire('bronx.telegram.webhook.text', [$data]);
            }

            if (isset($data['message']['audio'])) {
                Event::fire('bronx.telegram.webhook.audio', [$data]);
            }

            if (isset($data['message']['document'])) {
                Event::fire('bronx.telegram.webhook.document', [$data]);
            }

            if (isset($data['message']['game'])) {
                Event::fire('bronx.telegram.webhook.game', [$data]);
            }

            if (isset($data['message']['photo'])) {
                Event::fire('bronx.telegram.webhook.photo', [$data]);
            }

            if (isset($data['message']['sticker'])) {
                Event::fire('bronx.telegram.webhook.sticker', [$data]);
            }

            if (isset($data['message']['video'])) {
                Event::fire('bronx.telegram.webhook.video', [$data]);
            }

            if (isset($data['message']['voice'])) {
                Event::fire('bronx.telegram.webhook.voice', [$data]);
            }

            if (isset($data['message']['video_note'])) {
                Event::fire('bronx.telegram.webhook.video_note', [$data]);
            }

            if (isset($data['message']['contact'])) {
                Event::fire('bronx.telegram.webhook.contact', [$data]);
            }

            if (isset($data['message']['location'])) {
                Event::fire('bronx.telegram.webhook.location', [$data]);
            }

            if (isset($data['message']['venue'])) {
                Event::fire('bronx.telegram.webhook.venue', [$data]);
            }
        }

        if (isset($data['edited_message'])) {
            Event::fire('bronx.telegram.webhook.edited_message', [$data]);
        }

        if (isset($data['channel_post'])) {
            Event::fire('bronx.telegram.webhook.channel_post', [$data]);
        }

        if (isset($data['edited_channel_post'])) {
            Event::fire('bronx.telegram.webhook.edited_channel_post', [$data]);
        }

        if (isset($data['inline_query'])) {
            Event::fire('bronx.telegram.webhook.inline_query', [$data]);
        }

        if (isset($data['chosen_inline_result'])) {
            Event::fire('bronx.telegram.webhook.chosen_inline_result', [$data]);
        }

        if (isset($data['callback_query'])) {
            Event::fire('bronx.telegram.webhook.callback_query', [$data]);
        }

        if (isset($data['shipping_query'])) {
            Event::fire('bronx.telegram.webhook.shipping_query', [$data]);
        }

        if (isset($data['pre_checkout_query'])) {
            Event::fire('bronx.telegram.webhook.pre_checkout_query', [$data]);
        }
    }
}