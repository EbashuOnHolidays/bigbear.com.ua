<?php namespace Bronx\Telegram\Classes;

class TelegramSender
{
    private $debug;

    private $host;
    private $port;
    private $apiUrl;

    private $protocol_part;
    private $port_part;

    private $handle;

    public function __construct($token, $debug = false)
    {
        $this->debug = $debug;

        $this->handle = curl_init();
        $this->host = 'api.telegram.org';
        $this->port = 443;

        $this->protocol_part = ($this->port == 443 ? 'https' : 'http');
        $this->port_part = ($this->port == 443 || $this->port == 80) ? '' : ':' . $this->port;

        $this->apiUrl = "{$this->protocol_part}://{$this->host}{$this->port_part}/bot{$token}";
    }

    public function request($method, $params = [])
    {
        $url = $this->apiUrl . '/' . $method;
        $query = http_build_query($params);

        curl_setopt($this->handle, CURLOPT_URL, $url);
        curl_setopt($this->handle, CURLOPT_POST, true);
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, $query);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, true);

        $response_str = curl_exec($this->handle);
        $response = json_decode($response_str, true);

        if ($response['ok'] == false || $this->debug == true) {
            trace_log($response);
        }

        return $response;
    }
}