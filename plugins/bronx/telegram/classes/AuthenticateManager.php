<?php namespace Bronx\Telegram\Classes;

use Bronx\Telegram\Models\Settings;
use Exception;
use October\Rain\Support\Traits\Singleton;

class AuthenticateManager
{
    use Singleton;
    protected static $instance;

    /**
     * @param array $userData
     * @param $userHash
     * @return bool
     * @throws Exception
     */
    public function checkTelegramHash($userData = [], $userHash)
    {
        $token = Settings::get('telegram_token', null);

        if ($token == null) {
            throw new Exception('Telegram token empty');
        }

        // Make string with data
        $data = [];
        foreach ($userData as $key => $value) {
            $data[] = $key . '=' . $value;
        }

        sort($data);
        $data = implode("\n", $data);

        // Get hash
        $key = hash('sha256', $token, true);
        $hash = hash_hmac('sha256', $data, $key);

        if (strcmp($hash, $userHash) !== 0) {
            return false;
        }

        if ((time() - $userData['auth_date']) > 86400) {
            return false;
        }

        return true;
    }

    /**
     * Convert credentials to model columns
     * @param $credentials
     * @return array
     */
    public function toUserCredentials($credentials)
    {
        return [
            'first_name' => $credentials['first_name'] ?? null,
            'last_name'  => $credentials['last_name'] ?? null,
        ];
    }

    public function toGatewayCredentials($credentials)
    {
        return [
            'telegram_id'         => $credentials['id'],
            'telegram_first_name' => $credentials['first_name'],
            'telegram_last_name'  => $credentials['last_name'] ?? null,
            'telegram_username'   => $credentials['username'] ?? null,
            'telegram_photo_url'  => $credentials['photo_url'] ?? null,
            'telegram_auth_date'  => $credentials['auth_date'] ?? null,
        ];
    }
}