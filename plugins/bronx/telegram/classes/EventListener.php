<?php namespace Bronx\Telegram\Classes;

use Backend\Classes\FormTabs;
use Backend\Controllers\Users;
use Backend\Facades\Backend;
use Backend\Facades\BackendAuth;
use Backend\Models\User as BackendUser;
use Bronx\Telegram\Models\Settings;
use Bronx\User\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use October\Rain\Support\Facades\Flash;

class EventListener
{
    public static function boot()
    {
        /**
         * Listen sender events
         */
        self::listenSenderEvents();

        /**
         * Extend model
         */
        User::extend(function ($model) {
            self::extendModel($model);
        });

        BackendUser::extend(function ($model) {
            self::extendModel($model);
        });

        /**
         * Backend user model event listen
         */
        Event::listen('backend.user.login', function ($user) {
            $telegramId = $user->telegram_id;

            if ($telegramId != null) {
                $message = View::make('bronx.telegram::notification', [
                    'timestamp'  => Carbon::now()->format('H:i:s, d.m.Y'),
                    'ip_address' => Request::getClientIp(),
                    'login'      => $user->login,
                ])->render();

                Event::fire('bronx.telegram.sendMessage', [$user->telegram_id, $message]);
            }
        });

        /**
         * Extend controller AJAX
         */
        Users::extend(function ($controller) {
            $controller->addDynamicMethod('onDetachTelegram', function () {
                $userId = Input::get('user_id', null);

                $user = BackendUser::where('id', $userId)
                    ->first();

                if ($user != null) {
                    $user->update([
                        'telegram_id'         => null,
                        'telegram_first_name' => null,
                        'telegram_last_name'  => null,
                        'telegram_username'   => null,
                        'telegram_photo_url'  => null,
                        'telegram_auth_date'  => null,
                    ]);

                    Flash::success(Lang::get('bronx.telegram::lang.view.flash.logout_success'));
                } else {
                    Flash::success(Lang::get('bronx.telegram::lang.view.flash.error'));
                }

                return Backend::redirect('backend/users/myaccount');
            });
        });

        /**
         * Extend fields
         */
        Event::listen('backend.form.extendFields', function ($widget) {
            if (!$widget->model instanceof User) {
                return;
            }

            self::extendFields($widget);
        });

        Event::listen('backend.form.extendFields', function ($widget) {
            if (!$widget->model instanceof BackendUser) {
                return;
            }

            $user = BackendAuth::getUser();

            if ($user->id == $widget->model->id) {
                if ($widget->model->telegram_id == null) {
                    $widget->addFields([
                        'telegram_partial' => [
                            'tab'     => 'bronx.telegram::lang.form.tab',
                            'span'    => 'full',
                            'type'    => 'partial',
                            'path'    => '$/bronx/telegram/views/_login_partial.htm',
                            'options' => [
                                'name' => Settings::get('telegram_name', null),
                            ],
                        ],
                    ], FormTabs::SECTION_PRIMARY);
                } else {
                    $widget->addFields([
                        'telegram_partial' => [
                            'tab'     => 'bronx.telegram::lang.form.tab',
                            'span'    => 'full',
                            'type'    => 'partial',
                            'path'    => '$/bronx/telegram/views/_logout_partial.htm',
                            'options' => [
                                'user_id' => $widget->model->id,
                            ],
                        ],
                    ], FormTabs::SECTION_PRIMARY);
                }
            }

            self::extendFields($widget);
        });
    }

    /*
     * Expandable methods
     */
    private static function extendModel($model)
    {
        $model->addDateAttribute('telegram_auth_date');

        $model->addFillable([
            'telegram_id',
            'telegram_first_name',
            'telegram_last_name',
            'telegram_username',
            'telegram_photo_url',
            'telegram_auth_date',
        ]);
    }

    private static function extendFields($widget)
    {
        $widget->addFields([
            'telegram_id' => [
                'label'    => 'bronx.telegram::lang.form.telegram_id',
                'tab'      => 'bronx.telegram::lang.form.tab',
                'span'     => 'auto',
                'disabled' => true,
            ],
        ], FormTabs::SECTION_PRIMARY);

        $widget->addFields([
            'telegram_username' => [
                'label'    => 'bronx.telegram::lang.form.telegram_username',
                'tab'      => 'bronx.telegram::lang.form.tab',
                'span'     => 'auto',
                'disabled' => true,
            ],
        ], FormTabs::SECTION_PRIMARY);

        $widget->addFields([
            'telegram_first_name' => [
                'label'    => 'bronx.telegram::lang.form.telegram_first_name',
                'tab'      => 'bronx.telegram::lang.form.tab',
                'span'     => 'auto',
                'disabled' => true,
            ],
        ], FormTabs::SECTION_PRIMARY);

        $widget->addFields([
            'telegram_last_name' => [
                'label'    => 'bronx.telegram::lang.form.telegram_last_name',
                'tab'      => 'bronx.telegram::lang.form.tab',
                'span'     => 'auto',
                'disabled' => true,
            ],
        ], FormTabs::SECTION_PRIMARY);

        $widget->addFields([
            'telegram_photo_url' => [
                'label'    => 'bronx.telegram::lang.form.telegram_photo_url',
                'tab'      => 'bronx.telegram::lang.form.tab',
                'span'     => 'auto',
                'disabled' => true,
            ],
        ], FormTabs::SECTION_PRIMARY);

        $widget->addFields([
            'telegram_auth_date' => [
                'label'    => 'bronx.telegram::lang.form.telegram_auth_date',
                'tab'      => 'bronx.telegram::lang.form.tab',
                'span'     => 'auto',
                'disabled' => true,
            ],
        ], FormTabs::SECTION_PRIMARY);
    }

    private static function listenSenderEvents()
    {
        Event::listen('bronx.telegram.sendToBackendSubscribers', function ($access, $message, $data = null) {
            $admins = BackendUser::get();
            $admins->each(function ($admin) use ($access, $message, $data) {
                if ($admin->telegram_id != null && $admin->hasAccess($access)) {
                    Event::fire('bronx.telegram.sendMessage', [$admin->telegram_id, $message, $data]);
                }
            });
        });

        Event::listen('bronx.telegram.setWebhook', function () {
            $telegram_token = Settings::get('telegram_token');
            $telegram_debug = Settings::get('trace_log_output');

            $gateway = new TelegramSender($telegram_token, $telegram_debug);

            $gateway->request('setWebhook', [
                'url'             => route('telegram_webhook'),
                'max_connections' => Settings::get('max_connections'),
            ]);
        });

        Event::listen('bronx.telegram.deleteWebhook', function () {
            $telegram_token = Settings::get('telegram_token');
            $telegram_debug = Settings::get('trace_log_output');

            $gateway = new TelegramSender($telegram_token, $telegram_debug);

            $gateway->request('deleteWebhook');
        });

        Event::listen('bronx.telegram.sendMessage', function ($id, $message, $data = []) {
            $telegram_token = Settings::get('telegram_token');
            $telegram_debug = Settings::get('trace_log_output');

            $gateway = new TelegramSender($telegram_token, $telegram_debug);

            $response = $gateway->request('sendMessage', array_merge([
                'chat_id'                  => $id,
                'text'                     => $message,
                'parse_mode'               => 'Markdown',
                'disable_web_page_preview' => true,
            ], $data));

            if ($response['ok'] == false) {
                trace_log($message);
            }
        });
    }
}