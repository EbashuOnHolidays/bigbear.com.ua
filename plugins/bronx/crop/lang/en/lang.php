<?php

return [
    'plugin'      => [
        'name'        => 'Cropping photos',
        'description' => 'Trimming original photos at startup',
    ],
    'permissions' => [
        'tab'      => 'Bronx Crop',
        'settings' => [
            'label' => 'Manage plugin settings',
        ],
    ],
    'settings'    => [
        'name'        => 'Cropping photo',
        'description' => 'Trim settings for original photos',
        'form'        => [
            'crop_mode'        => [
                'label'   => 'Cropping method',
                'comment' => 'Enabled only if change of width and/or height is enabled',
                'options' => [
                    'auto'      => 'Auto - Checks to see if an image is portrait or landscape and resizes accordingly',
                    'crop'      => 'Crop - Attempts to find the best way to crop. Whether crop is based on the image being portrait or landscape',
                    'exact'     => 'Exact - Trim the photo to the exact size',
                    'portrait'  => 'Portrait - Returns the width based on the image height',
                    'landscape' => 'Landscape - Returns the height based on the image width',
                ],
            ],
            'is_change_width'  => [
                'label' => 'Change width',
            ],
            'is_change_height' => [
                'label' => 'Change height',
            ],
            'max_width'        => [
                'label' => 'Maximum width',
            ],
            'max_height'       => [
                'label' => 'Maximum height',
            ],
            'is_make_enlarge'  => [
                'label'   => 'Make enlarged',
                'comment' => 'If the image is smaller than the maximum width and/or height, the image will be enlarged to the maximum width and/or height. We recommend that you do not include this feature without having to',
            ],
        ],
    ],
];