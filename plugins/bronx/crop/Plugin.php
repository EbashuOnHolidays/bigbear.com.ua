<?php namespace Bronx\Crop;

use Bronx\Crop\Models\Settings;
use October\Rain\Database\Attach\File;
use October\Rain\Database\Attach\Resizer;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'bronx.crop::lang.plugin.name',
            'description' => 'bronx.crop::lang.plugin.description',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-crop',
        ];
    }

    public function boot()
    {
        File::extend(function ($model) {
            $model->bindEvent('model.beforeCreate', function () use ($model) {
                if ($model->isImage()) {
                    $filePath = $model->getLocalPath();

                    $maxWidth = false;
                    $maxHeight = false;

                    $isChangeWidth = Settings::get('is_change_width');
                    $isChangeHeight = Settings::get('is_change_height');

                    $isMakeEnlarge = Settings::get('is_make_enlarge', false);

                    list($originalWidth, $originalHeight) = getimagesize($filePath);

                    if ($isChangeWidth == true) {
                        $maxWidth = Settings::get('max_width');
                    }

                    if ($isChangeHeight == true) {
                        $maxHeight = Settings::get('max_height');
                    }

                    if (
                        ($isChangeWidth == true || $isChangeHeight == true) &&
                        ($isMakeEnlarge == true || $originalWidth > $maxWidth || $originalHeight > $maxHeight)
                    ) {
                        $image = Resizer::open($filePath);

                        $image->resize($maxWidth, $maxHeight, [
                            'mode' => Settings::get('crop_mode'),
                        ]);

                        $image->save($filePath);

                        clearstatcache();

                        $model->file_size = filesize($filePath);
                    }
                }
            });
        });
    }

    public function registerPermissions()
    {
        return [
            'seo.settings' => [
                'tab'   => 'bronx.crop::lang.permissions.settings.tab',
                'label' => 'bronx.crop::lang.permissions.settings.name',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'crop' => [
                'label'       => 'bronx.crop::lang.settings.name',
                'description' => 'bronx.crop::lang.settings.description',
                'category'    => SettingsManager::CATEGORY_CMS,
                'icon'        => 'icon-crop',
                'class'       => Settings::class,
                'order'       => 280,
                'keywords'    => 'images resize crop',
                'permissions' => [
                    'compress.settings',
                ],
            ],
        ];
    }
}