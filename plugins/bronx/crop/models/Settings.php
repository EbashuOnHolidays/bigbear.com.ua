<?php namespace Bronx\Crop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_crop_settings';
    public $settingsFields = 'fields.yaml';

    use Validation;
    public $rules = [
        'crop_mode'        => 'required|in:auto,crop,exact,portrait,landscape',
        'is_change_width'  => 'boolean',
        'is_change_height' => 'boolean',
        'max_width'        => 'required|numeric|min:1',
        'max_height'       => 'required|numeric|min:1',
        'is_make_enlarge'  => 'boolean',
    ];

    public function initSettingsData()
    {
        $this->crop_mode = 'auto';

        $this->is_change_width = false;
        $this->is_change_height = false;

        $this->max_width = 800;
        $this->max_height = 600;

        $this->is_make_enlarge = false;
    }
}