<?php namespace Bronx\Helper;

use Barryvdh\Debugbar\ServiceProvider as DebugbarServiceProvider;
use Illuminate\Support\Facades\App;
use October\Rain\Support\Facades\Config;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Helper',
            'description' => '...',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-shopping-cart',
        ];
    }

    public function boot()
    {
//        $this->app['config']->set('debugbar', Config::get('bronx.helper::debugbar'));
//
//        App::register(DebugbarServiceProvider::class);
    }
}