<?php namespace Bronx\SMS;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'SMS',
            'description' => '...',
            'author'      => 'Bronx',
            'icon'        => 'icon-leaf'
        ];
    }
}