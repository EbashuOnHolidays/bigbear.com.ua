<?php namespace Bronx\Feedback\Components;

use Backend\Models\User as BackendUser;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Feedback Component',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onContact()
    {
        if (Session::token() != Input::get('_token')) {
            throw new ApplicationException('Invalid token!');
        }

        $data = [
            'subject' => Input::get('subject', null),
            'name'    => Input::get('name', null),
            'phone'   => Input::get('phone', null),
            'message' => Input::get('message', null),
        ];

        $validation = Validator::make($data, [
            'subject' => 'required',
            'name'    => 'required|between:3,15',
            'phone'   => 'required|between:10,19',
            'message' => 'max:1023',
        ], [
            'subject.required' => 'Тема обязательна для заполнения',
            'name.required'    => 'Обязательно для заполнения',
            'name.between'     => 'Не может быть короче :min и длинее :max символов',
            'phone.required'   => 'Обязателен для заполнения',
            'phone.between'    => 'Не может быть короче :min и длинее :max символов',
            'message.max'      => 'Не может быть длинее :max символов',
        ]);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $messageInfo = [
            'subject'    => $data['subject'],
            'timestamp'  => Carbon::now()->format('H:i:s, d.m.Y'),
            'ip_address' => Request::getClientIp(),
            'name'       => $data['name'],
            'phone'      => $data['phone'],
            'message'    => $data['message'],
        ];

        $admins = BackendUser::get();
        $message = View::make('bronx.feedback::message', $messageInfo)->render();

        $admins->each(function ($admin) use ($message) {
            if ($admin->telegram_id != null && $admin->hasAccess('bronx.feedback.send_orders')) {
                Event::fire('bronx.telegram.sendMessage', [$admin->telegram_id, $message]);
            }
        });

        return [
            '#_contact_form' => $this->renderPartial('static/contact/thank'),
        ];
    }
}