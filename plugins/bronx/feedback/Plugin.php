<?php namespace Bronx\Feedback;

use System\Classes\PluginBase;
use Bronx\Feedback\Components\Contact;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Feedback',
            'description' => '...',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-leaf',
        ];
    }

    public function registerComponents()
    {
        return [
            Contact::class => 'feedbackContact',
        ];
    }

    public function registerPermissions()
    {
        return [
            'bronx.feedback.send_orders' => [
                'label' => 'Отправлять сообщения обратной связи',
                'tab'   => 'Обратная связь',
                'order' => 50,
                'roles' => ['publisher'],
            ],
        ];
    }
}