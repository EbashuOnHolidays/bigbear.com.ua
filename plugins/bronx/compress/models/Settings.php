<?php namespace Bronx\Compress\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_compress_settings';
    public $settingsFields = 'fields.yaml';

    use Validation;
    public $rules = [
        'png_сompression'  => 'required|numeric|min:0|max:100',
        'jpg_сompression'  => 'required|numeric|min:0|max:100',
        'webp_сompression' => 'required|numeric|min:0|max:100',
    ];

    public function initSettingsData()
    {
        $this->png_сompression = 90;
        $this->jpg_сompression = 90;
        $this->webp_сompression = 90;
    }
}