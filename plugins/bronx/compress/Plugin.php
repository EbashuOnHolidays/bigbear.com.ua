<?php namespace Bronx\Compress;

use Bronx\Compress\Models\Settings;
use October\Rain\Database\Attach\File;
use October\Rain\Database\Attach\Resizer;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'bronx.compress::lang.plugin.name',
            'description' => 'bronx.compress::lang.plugin.description',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-compress',
        ];
    }

    public function boot()
    {
        File::extend(function ($model) {
            $model->bindEvent('model.beforeCreate', function () use ($model) {
                if ($model->isImage()) {
                    $filePath = $model->getLocalPath();

                    $image = Resizer::open($filePath);

                    switch (strtolower($model->getExtension())) {
                        case 'jpg':
                        case 'jpeg':
                            $image->setOptions([
                                'quality' => Settings::get('jpg_сompression'),
                            ]);
                            break;

                        case 'gif':
                            return;
                            break;

                        case 'png':
                            $image->setOptions([
                                'quality' => Settings::get('png_сompression'),
                            ]);
                            break;

                        case 'webp':
                            $image->setOptions([
                                'quality' => Settings::get('webp_сompression'),
                            ]);
                            break;
                    }

                    $image->save($filePath);

                    clearstatcache();

                    $model->file_size = filesize($filePath);
                }
            });
        });
    }

    public function registerPermissions()
    {
        return [
            'seo.settings' => [
                'tab'   => 'bronx.compress::lang.permissions.settings.tab',
                'label' => 'bronx.compress::lang.permissions.settings.name',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'compress' => [
                'label'       => 'bronx.compress::lang.settings.name',
                'description' => 'bronx.compress::lang.settings.description',
                'category'    => SettingsManager::CATEGORY_CMS,
                'icon'        => 'icon-compress',
                'class'       => Settings::class,
                'order'       => 270,
                'keywords'    => 'images compress',
                'permissions' => [
                    'compress.settings',
                ],
            ],
        ];
    }
}