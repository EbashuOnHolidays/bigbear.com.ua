<?php

return [
    'plugin'      => [
        'name'        => 'Compressing photos',
        'description' => 'Compressing original photos at startup',
    ],
    'permissions' => [
        'tab'      => 'Bronx Compress',
        'settings' => [
            'label' => 'Manage plugin settings',
        ],
    ],
    'settings'    => [
        'name'        => 'Compress photo',
        'description' => 'Compression settings of the original photo',
        'form'        => [
            'png_сompression'  => [
                'label'   => 'Compression ratio .PNG images',
                'options' => [
                    '0'  => '0 - compression disabled',
                    '10' => '10 - default by OctoberCMS',
                ],
            ],
            'jpg_сompression'  => [
                'label'   => 'Compression quality .JPG images',
                'options' => [
                    '90'  => '90 - default by OctoberCMS',
                    '100' => '100 - compression disabled',
                ],
            ],
            'webp_сompression' => [
                'label'   => 'Compression quality .WEBP images',
                'options' => [
                    '90'  => '90 - default by OctoberCMS',
                    '100' => '100 - compression disabled',
                ],
            ],
        ],
    ],
];