<?php namespace Bronx\Blog\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_Post_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_blog_tab_post', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('slug');
            $table->boolean('is_enable')->default(false);

            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');

            $table->text('short_content');
            $table->text('content');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_blog_tab_post');
    }
}