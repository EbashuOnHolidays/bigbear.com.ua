<?php namespace Bronx\Blog\Components;

use Bronx\Blog\Models\Post;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Input;

class PostList extends ComponentBase
{
    public $posts;

    private $urlCanonicalPage;
    private $urlCurrentPage;
    private $urlNextPage;
    private $urlPrevPage;

    private $postListPage;
    public $postListPerPage;
    private $postSinglePage;

    private $postListRequest;

    public function componentDetails()
    {
        return [
            'name'        => 'PostList Component',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [
            'postListPage'    => [
                'title' => 'Post list page',
                'type'  => 'dropdown',
            ],
            'postListPerPage' => [
                'title'   => 'Post list per page',
                'default' => 10,
            ],

            'postSinglePage' => [
                'title' => 'Post single page',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getPostListPageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    public function getPostSinglePageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    private function beforeRun()
    {
        $this->postListPage = $this->property('postListPage');
        $this->postListPerPage = $this->property('postListPerPage');

        $this->postSinglePage = $this->property('postSinglePage');

        if (!isset($this->postListRequest['page'])) {
            $this->postListRequest['page'] = Input::get('page', 1);
        }
    }

    private function afterRun()
    {
        $this->urlCanonicalPage = $this->controller->pageUrl($this->postListPage, $this->controller);
        $this->urlCurrentPage = $this->generateUrl();
        $this->urlPrevPage = $this->posts->previousPageUrl();
        $this->urlNextPage = $this->posts->nextPageUrl();

        $this->page->urlCanonicalPage = $this->urlCanonicalPage;
        $this->page->urlCurrentPage = $this->urlCurrentPage;
        $this->page->urlPrevPage = $this->urlPrevPage;
        $this->page->urlNextPage = $this->urlNextPage;
    }

    public function onRun()
    {
        $this->beforeRun();

        $this->posts = Post::isEnabled()
            ->orderBy('created_at', 'DESC')
            ->paginate($this->postListPerPage, $this->postListRequest['page']);

        $this->posts->each(function ($post) {
            $post->url = $post->setUrl($this->postSinglePage, $this->controller);
        });

        $this->afterRun();
    }

    /**
     * Формируем новый URL, возвращаем результат
     * @return array
     */
    private function refreshPartials()
    {
        return [
            '#_news_headline_wrapper'   => $this->renderPartial('news/headline'),
            '#_news_list_wrapper'       => $this->renderPartial('news/list/wrapper'),
            '#_news_pagination_wrapper' => $this->renderPartial('news/pagination/wrapper'),
            '#_news_flipping_wrapper'   => $this->renderPartial('news/flipping/wrapper'),
            'urlCurrentPage'            => $this->urlCurrentPage,
            'urlPrevPage'               => $this->urlPrevPage,
            'urlNextPage'               => $this->urlNextPage,
        ];
    }

    /*
     * Генерация URL / SEO
     */
    private function generateUrl()
    {
        $request = $this->postListRequest;

        if ($request['page'] == 1) {
            $request['page'] = null;
        }

        $url = trim($this->urlCanonicalPage . '?' . http_build_query($request), '/?');

        $url = str_replace('%5B', '[', $url);
        $url = str_replace('%5D', ']', $url);

        return $url;
    }

    /*
     * AJAX Events
     */
    public function onChangeNumPage()
    {
        $this->postListRequest['page'] = Input::get('page', 1);

        $this->onRun();

        return $this->refreshPartials();
    }
}