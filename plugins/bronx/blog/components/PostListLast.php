<?php namespace Bronx\Blog\Components;

use Bronx\Blog\Models\Post;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;

class PostListLast extends ComponentBase
{
    public $posts;

    private $postListPerPage;
    private $postSinglePage;

    public function componentDetails()
    {
        return [
            'name'        => 'PostListLast Component',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [
            'postListPerPage' => [
                'title'   => 'Post list per page',
                'default' => 3,
            ],

            'postSinglePage' => [
                'title' => 'Post single page',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getPostSinglePageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    private function beforeRun()
    {
        $this->postListPerPage = $this->property('postListPerPage');

        $this->postSinglePage = $this->property('postSinglePage');
    }

    public function onRun()
    {
        $this->beforeRun();

        $this->posts = Post::isEnabled()
            ->orderBy('created_at', 'DESC')
            ->paginate($this->postListPerPage, 1);

        $this->posts->each(function ($post) {
            $post->url = $post->setUrl($this->postSinglePage, $this->controller);
        });
    }
}