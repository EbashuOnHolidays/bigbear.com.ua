<?php namespace Bronx\Blog\Components;

use Bronx\Blog\Models\Post;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Redirect;

class PostSingle extends ComponentBase
{
    public $post;

    private $postSinglePage;
    private $postSingleSlug;

    public function componentDetails()
    {
        return [
            'name'        => 'PostSingle Component',
            'description' => '...'
        ];
    }

    public function defineProperties()
    {
        return [
            'postSinglePage' => [
                'title' => 'Post single page',
                'type'  => 'dropdown',
            ],
            'postSingleSlug' => [
                'title'   => 'Post single slug',
                'default' => '{{ :slug }}',
            ],
        ];
    }

    public function getPostSinglePageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    private function beforeRun()
    {
        $this->postSinglePage = $this->property('postSinglePage');
        $this->postSingleSlug = $this->property('postSingleSlug');
    }

    private function afterRun()
    {
        $this->page->title = $this->post->name;

        if ($this->post->meta_title != null) {
            $this->page->meta_title = $this->post->meta_title;
        }

        if ($this->post->meta_description != null) {
            $this->page->meta_description = $this->post->meta_description;
        }

        if ($this->post->meta_keywords != null) {
            $this->page->meta_keywords = $this->post->meta_keywords;
        }
    }

    public function onRun()
    {
        $this->beforeRun();

        $this->post = Post::isEnabled()
            ->where('slug', $this->postSingleSlug)
            ->first();

        if (!$this->post) {
            // Try redirect to new url
            $parts = explode('-', $this->postSingleSlug);

            $post = Post::where('id', $parts[0])
                ->first();

            if ($post) {
                return Redirect::to($post->setUrl($this->postSinglePage, $this->controller), 302);
            } else {
                return $this->controller->run('404');
            }
        }
        
        $this->afterRun();
    }
}