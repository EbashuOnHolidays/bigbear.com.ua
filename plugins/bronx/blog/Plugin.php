<?php namespace Bronx\Blog;

use Backend\Facades\Backend;
use Bronx\Blog\Components\PostList;
use Bronx\Blog\Components\PostListLast;
use Bronx\Blog\Components\PostSingle;
use Bronx\Blog\Models\Post;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Блог',
            'description' => '...',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-book',
        ];
    }

    public function registerComponents()
    {
        return [
            PostList::class     => 'blogPostList',
            PostListLast::class => 'blogPostListLast',
            PostSingle::class   => 'blogPostSingle',
        ];
    }

    public function boot()
    {
        /*
         * SITEMAP
         */
        Event::listen('pages.menuitem.listTypes', function () {
            return [
                'blogPostSingle' => 'Блог: Страница публикации',
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function ($type) {
            if ($type == 'blogPostSingle') {
                return Post::getMenuTypeInfo($type);
            }
        });

        Event::listen('pages.menuitem.resolveItem', function ($type, $item, $url, $theme) {
            if ($type == 'blogPostSingle') {
                return Post::resolveMenuItem($item, $url, $theme);
            }
        });
    }

    public function registerNavigation()
    {
        return [
            'blog' => [
                'label'   => 'Блог',
                'url'     => Backend::url('bronx/blog/posts'),
                'icon'    => 'icon-pencil',
                'iconSvg' => 'plugins/bronx/blog/assets/images/blog.svg',
                'order'   => 60,

                'sideMenu' => [
                    'posts' => [
                        'label' => 'Публикации',
                        'url'   => Backend::url('bronx/blog/posts'),
                        'icon'  => 'icon-pencil',
                    ],
                ],
            ],
        ];
    }
}