<?php namespace Bronx\Blog\Models;

use Cms\Classes\Page;
use Cms\Classes\Theme;
use October\Rain\Database\Model;
use System\Models\File;

class Post extends Model
{
    public $table = 'bronx_blog_tab_post';

    public $attachOne = [
        'relImage' => [
            File::class,
        ],
    ];

    public function setUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->slug,
        ];

        return $controller->pageUrl($pageName, $params);
    }

    /*
     * EVENT
     */
    public function afterCreate()
    {
        $this->save();
    }

    public function beforeSave()
    {
        $this->slug = str_slug(implode('-', [
            $this->id,
            $this->name,
        ]), '-');
    }

    /*
     * SCOPE
     */
    public function scopeIsEnabled($query)
    {
        $query->where('is_enable', true);
    }

    /*
     * SITEMAP
     */

    /**
     * Получение списка страниц которые выводят содержимое этой модели
     * @param $type
     * @return array
     */
    public static function getMenuTypeInfo($type)
    {
        $theme = Theme::getActiveTheme();
        $pages = Page::listInTheme($theme, true);

        $cmsPages = [];
        foreach ($pages as $page) {
            if (!$page->hasComponent('blogPostSingle')) {
                continue;
            }

            $properties = $page->getComponentProperties('blogPostSingle');
            if (!isset($properties['postSingleSlug']) || !preg_match('/{{\s*:/', $properties['postSingleSlug'])) {
                continue;
            }

            $cmsPages[] = $page;
        }

        return [
            'cmsPages' => $cmsPages,
        ];
    }

    /**
     * Генерация Sitemap для данной модели
     * @param $item
     * @param $url
     * @param $theme
     * @return array
     */
    public static function resolveMenuItem($item, $url, $theme)
    {
        $records = self::orderBy('name')
            ->get();

        $result = [];
        foreach ($records as $content) {
            $result['items'][] = [
                'title' => $content->name,
                'url'   => self::getContentUrl($item->cmsPage, $content, $theme),
                'mtime' => $content->updated_at,
            ];
        }

        return $result;
    }

    /**
     * Генерация ссылки на страницу
     * @param $pageCode
     * @param $content
     * @param $theme
     * @return string|void
     */
    protected static function getContentUrl($pageCode, $content, $theme)
    {
        $page = Page::loadCached($theme, $pageCode);

        if (!$page) {
            return;
        }

        $properties = $page->getComponentProperties('blogPostSingle');

        if (!isset($properties['postSingleSlug'])) {
            return;
        }

        if (!preg_match('/^\{\{([^\}]+)\}\}$/', $properties['postSingleSlug'], $matches)) {
            return;
        }

        $paramName = substr(trim($matches[1]), 1);

        return Page::url($page->getBaseFileName(), [$paramName => $content->slug]);
    }
}