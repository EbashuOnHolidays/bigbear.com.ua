<?php namespace Bronx\Blog\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Facades\BackendMenu;
use Backend\Classes\Controller;
use Bronx\Blog\Models\Post;
use Illuminate\Support\Facades\Input;

class Posts extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $bodyClass = '';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.Blog', 'blog', 'posts');
    }

    public function index()
    {
        $this->addCss('/plugins/bronx/blog/assets/styles/customSwitch.css');

        $this->asExtension('ListController')->index();
    }

    public function create()
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->create();
    }

    public function update($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    public function preview($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    /*
     * AJAX
     */
    public function onChangeColumnValueFast()
    {
        $checkedIds = Input::get('checked');
        $columnName = Input::get('column_name');
        $columnValue = Input::get('column_value');

        if (!empty($columnName) && !empty($checkedIds)) {
            Post::whereIn('id', $checkedIds)
                ->where($columnName, '!=', $columnValue)
                ->update([
                    $columnName => $columnValue,
                ]);
        }

        return $this->listRefresh();
    }
}