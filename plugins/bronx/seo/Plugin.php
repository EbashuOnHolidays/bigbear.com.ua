<?php namespace Bronx\SEO;

use Backend\Classes\FormTabs;
use Backend\Facades\Backend;
use Bronx\SEO\Classes\Compress;
use Bronx\SEO\Components\Widget;
use Bronx\SEO\FormWidgets\TextAreaCounter;
use Bronx\SEO\FormWidgets\TextCounter;
use Bronx\SEO\Models\Settings;
use Cms\Classes\Layout;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'bronx.seo::lang.plugin.name',
            'description' => 'bronx.seo::lang.plugin.description',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-search',
        ];
    }

    public function boot()
    {
        Event::listen('cms.page.render', function ($controller, $content) {
            return Compress::html($content);
        });

        Event::listen('cms.page.renderContent', function ($controller, $name, $content) {
            return Compress::html($content);
        });

        Event::listen('cms.page.renderPartial', function ($controller, $name, $content) {
            return Compress::html($content);
        });
    }

    public function register()
    {
        Event::listen('backend.form.extendFields', function ($widget) {
            $this->extendCMSFields($widget, Layout::class);
            $this->extendCMSFields($widget, Page::class);
        });
    }

    public function registerComponents()
    {
        return [
            Widget::class => 'bronxSEOWidget',
        ];
    }

    public function registerPermissions()
    {
        return [
            'seo.settings' => [
                'tab'   => 'bronx.seo::lang.permissions.settings.tab',
                'label' => 'bronx.seo::lang.permissions.settings.name',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'seo'     => [
                'label'       => 'bronx.seo::lang.settings.seo_name',
                'description' => 'bronx.seo::lang.settings.seo_description',
                'category'    => SettingsManager::CATEGORY_CMS,
                'icon'        => 'icon-search',
                'class'       => Settings::class,
                'order'       => 250,
                'keywords'    => 'seo helper',
                'permissions' => [
                    'seo.settings',
                ],
            ],
            'sitemap' => [
                'label'       => 'bronx.seo::lang.settings.sitemap_name',
                'description' => 'bronx.seo::lang.settings.sitemap_description',
                'category'    => SettingsManager::CATEGORY_CMS,
                'icon'        => 'icon-sitemap',
                'url'         => Backend::url('bronx/seo/definitions'),
                'order'       => 260,
                'keywords'    => 'seo helper',
                'permissions' => [
                    'seo.settings',
                ],
            ],
        ];
    }

    public function registerFormWidgets()
    {
        return [
            TextCounter::class     => [
                'label' => 'Text counter field',
                'code'  => 'textcounter',
            ],
            TextAreaCounter::class => [
                'label' => 'Textarea counter field',
                'code'  => 'textareacounter',
            ],
        ];
    }

    private function extendCMSFields($widget, $class)
    {
        if (!$widget->model instanceof $class) {
            return;
        }

        $widget->addFields([
            'settings[meta_title]'       => [
                'tab'   => 'cms::lang.editor.meta',
                'label' => 'bronx.seo::lang.settings.form.meta_title.label',
            ],
            'settings[meta_description]' => [
                'tab'   => 'cms::lang.editor.meta',
                'label' => 'bronx.seo::lang.settings.form.meta_description.label',
                'type'  => 'textarea',
                'size'  => 'tiny',
            ],
            'settings[meta_keywords]'    => [
                'tab'   => 'cms::lang.editor.meta',
                'label' => 'bronx.seo::lang.settings.form.meta_keywords.label',
            ],
            'settings[meta_canonical]'   => [
                'tab'   => 'cms::lang.editor.meta',
                'label' => 'bronx.seo::lang.settings.form.meta_canonical.label',
                'span'  => 'left',
            ],
            'settings[meta_redirect]'    => [
                'tab'   => 'cms::lang.editor.meta',
                'label' => 'bronx.seo::lang.settings.form.meta_redirect.label',
                'span'  => 'right',
            ],
            'settings[meta_index]'       => [
                'tab'     => 'cms::lang.editor.meta',
                'label'   => 'bronx.seo::lang.settings.form.meta_index.label',
                'span'    => 'left',
                'type'    => 'dropdown',
                'options' => [
                    'index'   => 'bronx.seo::lang.settings.form.meta_index.options.index',
                    'noindex' => 'bronx.seo::lang.settings.form.meta_index.options.noindex',
                ],
            ],
            'settings[meta_follow]'      => [
                'tab'     => 'cms::lang.editor.meta',
                'label'   => 'bronx.seo::lang.settings.form.meta_follow.label',
                'span'    => 'right',
                'type'    => 'dropdown',
                'options' => [
                    'follow'   => 'bronx.seo::lang.settings.form.meta_follow.options.follow',
                    'nofollow' => 'bronx.seo::lang.settings.form.meta_follow.options.nofollow',
                ],
            ],
        ], FormTabs::SECTION_PRIMARY);
    }
}