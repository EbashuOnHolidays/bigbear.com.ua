<?php namespace Bronx\SEO\Classes;

class Compress
{
    private static $html = [
        "#\n+#"                      => " ",
        "#\s+#"                      => " ",
        "#<!--[0-9A-Za-z -=_/]+-->#" => " ",
        "#\s*<\s*#"                  => "<",
        "#\s*/?>\s*#"                => ">",
    ];

    private static $json = [
        "#\n+#"     => " ",
        "#\s+#"     => " ",
        "#\s*{\s*#" => "{",
        "#\s*}\s*#" => "}",
        "#\s*:\s*#" => ":",
        "#\s*,\s*#" => ",",
    ];

    private static function replace($replace, $content)
    {
        return preg_replace(array_keys($replace), $replace, $content);
    }

    public static function html($content)
    {
        return self::replace(self::$html, $content);
    }

    public static function json($content)
    {
        return self::replace(self::$json, $content);
    }
}