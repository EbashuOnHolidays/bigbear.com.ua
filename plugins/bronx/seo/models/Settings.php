<?php namespace Bronx\SEO\Models;

use October\Rain\Database\Model;
use System\Behaviors\SettingsModel;
use System\Models\File;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_seo_settings';
    public $settingsFields = 'fields.yaml';

    public function initSettingsData()
    {
        $this->meta_index = 'index';
        $this->meta_follow = 'follow';
        $this->meta_viewport = 'width=device-width, initial-scale=1, maximum-scale=1';

        $this->is_webapp = true;
        $this->webapp_display = 'fullscreen';

        $this->humans = "/* TEAM */\nYour title: Your name.\nSite: email, link to a contact form, etc.\nTwitter: your Twitter username.\nLocation: City, Country.\n\n[...]\n\n/* THANKS */\nName: name or url\n\n[...]\n\n/* SITE */\nLast update: YYYY/MM/DD \nStandards: HTML5, CSS3,..\nComponents: Modernizr, jQuery, etc.\nSoftware: Software used for the development";
        $this->robots = "User-agent: *\n\n{% if settings.meta_index == 'index' %}Allow{% else %}Disallow{% endif %}: /\n\nSitemap: {{ host }}/sitemap.xml";
    }

    public $attachOne = [
        'relAppIcon' => [
            File::class,
        ],
    ];
}