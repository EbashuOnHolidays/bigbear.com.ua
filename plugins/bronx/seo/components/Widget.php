<?php namespace Bronx\SEO\Components;

use Bronx\SEO\Models\Settings;
use Cms\Classes\ComponentBase;

class Widget extends ComponentBase
{
    private $globalSettings;
    private $layoutSettings;
    private $pageSettings;

    public $settings = [];

    private $defaultSettings = [];

    public function componentDetails()
    {
        return [
            'name'        => 'bronx.seo::lang.components.widget.name',
            'description' => 'bronx.seo::lang.components.widget.description',
        ];
    }

    public function defineProperties()
    {
        return [
            'meta_index'  => [
                'title'   => 'bronx.seo::lang.settings.form.meta_index.label',
                'type'    => 'dropdown',
                'default' => 'index',
                'options' => [
                    'index'   => 'bronx.seo::lang.settings.form.meta_index.options.index',
                    'noindex' => 'bronx.seo::lang.settings.form.meta_index.options.noindex',
                ],
            ],
            'meta_follow' => [
                'title'   => 'bronx.seo::lang.settings.form.meta_follow.label',
                'type'    => 'dropdown',
                'default' => 'follow',
                'options' => [
                    'follow'   => 'bronx.seo::lang.settings.form.meta_follow.options.follow',
                    'nofollow' => 'bronx.seo::lang.settings.form.meta_follow.options.nofollow',
                ],
            ],
        ];
    }

    private function beforeRender()
    {
        $this->defaultSettings = [
            'meta_index'  => $this->property('meta_index'),
            'meta_follow' => $this->property('meta_follow'),
        ];
    }

    public function onRender()
    {
        $this->beforeRender();

        $layout = $this->controller->getLayout();
        $page = $this->controller->getPage();

        $this->globalSettings = Settings::instance();
        $this->layoutSettings = array_merge($this->defaultSettings, $layout->settings);
        $this->pageSettings = array_merge($this->defaultSettings, $page->settings);

        $this->settings = [
            'meta_index'       => $this->getSettingsBoolean('meta_index', 'index', 'noindex'),
            'meta_follow'      => $this->getSettingsBoolean('meta_follow', 'follow', 'nofollow'),
            'meta_title'       => $this->getSettings('meta_title'),
            'meta_description' => $this->getSettings('meta_description'),
            'meta_keywords'    => $this->getSettings('meta_keywords'),
            'meta_redirect'    => $this->getSettings('meta_redirect'),
            'meta_canonical'   => $this->getSettings('meta_canonical'),
            'meta_author'      => $this->globalSettings['meta_author'] ?? null,
            'meta_repeater'    => $this->globalSettings['meta_repeater'] ?? null,
            'meta_viewport'    => $this->globalSettings['meta_viewport'] ?? null,
            'site_title'       => $this->globalSettings['meta_title'] ?? null,
            'is_webapp'        => $this->globalSettings['is_webapp'] ?? null,
            'webapp_title'     => $this->globalSettings['webapp_title'] ?? null,
            'webapp_color'     => $this->globalSettings['webapp_color'] ?? null,
            'relAppIcon'       => $this->globalSettings['relAppIcon'] ?? null,
            'humans'           => $this->globalSettings['humans'] ?? null,
            'robots'           => $this->globalSettings['robots'] ?? null,
            'meta_first'       => $this->pageSettings['meta_first'] ?? null,
            'meta_prev'        => $this->pageSettings['meta_prev'] ?? null,
            'meta_next'        => $this->pageSettings['meta_next'] ?? null,
            'meta_last'        => $this->pageSettings['meta_last'] ?? null,
        ];
    }

    private function getSettings($key)
    {
        return $this->pageSettings[$key] ?? $this->layoutSettings[$key] ?? $this->globalSettings[$key] ?? null;
    }

    private function getSettingsBoolean($key, $true, $false)
    {
        return ($this->pageSettings[$key] == $true && $this->layoutSettings[$key] == $true && $this->globalSettings[$key] == $true) ? $true : $false;
    }
}