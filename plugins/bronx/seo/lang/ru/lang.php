<?php

return [
    'plugin'      => [
        'name'        => 'SEO',
        'description' => 'SEO помощник',
    ],
    'components'  => [
        'widget' => [
            'name'        => 'Мета информация',
            'description' => 'Вывод meta тегов о странице',
        ],
    ],
    'permissions' => [
        'settings' => [
            'name' => 'Управление настройками плагина',
            'tab'  => 'Bronx SEO',
        ],
    ],
    'formwidget'  => [
        'helpblock' => [
            'letter' => 'Длина: :length символов. Рекомендуемая длина: :maxLength символов',
            'tag'    => 'Длина: :length тегов. Рекомендуемая длина: :maxLength тегов',
        ],
    ],
    'settings'    => [
        'seo_name'            => 'META информация',
        'seo_description'     => 'Настройка мета информации о ресурсе',
        'sitemap_name'        => 'Карта сайта',
        'sitemap_description' => 'Настройки карты сайта',
        'form'                => [
            'tab_meta'           => 'META',
            'tab_graphics'       => 'Визуальное оформление',
            'tab_humans'         => 'humans.txt',
            'tab_robots'         => 'robots.txt',
            'meta_section'       => [
                'label'   => 'Meta информация о ресурсе',
                'comment' => 'Данная информация используются при выводе meta тегов если в компоненте плагина не указаны другие',
            ],
            'meta_index'         => [
                'label'   => 'Индексирование',
                'options' => [
                    'index'   => 'index: разрешить индексацию',
                    'noindex' => 'noindex: запретить индексацию',
                ],
            ],
            'meta_follow'        => [
                'label'   => 'Переход по ссылкам',
                'options' => [
                    'follow'   => 'follow: разрешить переход',
                    'nofollow' => 'nofollow: запретить переход',
                ],
            ],
            'meta_title'         => [
                'label' => 'Название ресурса (meta title)',
            ],
            'meta_author'        => [
                'label' => 'Владелец ресурса (meta author)',
            ],
            'meta_viewport'      => [
                'label' => 'Настройки окна просмотра (meta viewport)',
            ],
            'meta_description'   => [
                'label' => 'Описание (meta description)',
            ],
            'meta_keywords'      => [
                'label' => 'Ключевые слова (meta keywords)',
            ],
            'meta_canonical'     => [
                'label' => 'Канонический URL (meta canonical)',
            ],
            'meta_redirect'      => [
                'label' => 'Переадресация на URL (meta redirect)',
            ],
            'meta_repeater'      => [
                'label'  => 'Дополнительные мета теги',
                'prompt' => 'Добавить тег',
                'form'   => [
                    'name'    => [
                        'label'        => 'Meta name',
                        'commentAbove' => 'Например: google-site-verification',
                    ],
                    'content' => [
                        'label'        => 'Meta content',
                        'commentAbove' => 'Например: _X5TA_ji3wUIXDWZkab3Z5tF3TTlhLKZIbqkdy8MHZM',
                    ],
                ],
            ],
            'graphics_section'   => [
                'label'   => 'Управление графикой',
                'comment' => 'Настройте Favicon, App Icon и App Color для интеграции сайта в браузер пользователя',
            ],
            'webapp_title'       => [
                'label'        => 'Название приложения',
                'commentAbove' => 'Подпись под значком Appicon на главном экране',
            ],
            'appicon'            => [
                'label'        => 'Appicon',
                'commentAbove' => 'Загрузите .png или .jpg изображение размером от 192x192',
            ],
            'is_webapp'          => [
                'label'        => 'Открывать как мобильное приложение',
                'commentAbove' => 'Веб-приложение, добавленое на главный экран, будет запущено как мобильное приложение',
            ],
            'webapp_display'     => [
                'label'        => 'Настройка интерфейса браузера при запуске приложения',
                'commentAbove' => 'Например, вы можете скрыть интерфейс браузера и отобразить сайт полностью на весь экран',
                'options'      => [
                    'fullscreen' => 'Fullscreen: открывает веб-приложение без пользовательского интерфейса браузера и занимает всю доступную область отображения.',
                    'standalone' => 'Standalone: открывает веб-приложение, в собственном окне, отдельно от браузера, и скрывает стандартные элементы интерфейса браузера, такие как панель URL и т. д.',
                    'minimal-ui' => 'Minimal-ui: (не поддерживается Chrome) этот режим похож на полноэкранный, но предоставляет пользователю некоторые средства для доступа к минимальному набору элементов пользовательского интерфейса для управления навигацией (т. е. назад, вперед, перезагрузка и т. д.).',
                    'browser'    => 'Browser: стандартный браузер.',
                ],
            ],
            'appcolor'           => [
                'label'        => 'Основной цвет',
                'commentAbove' => 'Укажите основной цвет веб-сайта. Отображается в адресной строке некоторых мобильных браузеров',
            ],
            'humans'             => [
                'commentAbove' => 'Документация: http://humanstxt.org',
            ],
            'robots'             => [
                'commentAbove' => 'Документация: http://www.robotstxt.org/robotstxt.html',
            ],
            'type'               => [
                'label' => 'Тип',
            ],
            'url'                => [
                'label' => 'URL',
            ],
            'reference'          => [
                'label' => 'Ссылка',
            ],
            'cms_page'           => [
                'label'   => 'CMS Страница',
                'comment' => 'Выберите страницу, чтобы использовать URL-адрес.',
            ],
            'allow_nested_items' => [
                'label'   => 'Разрешить вложенные элементы',
                'comment' => 'Вложенные элементы могут быть созданы динамически статическими страницами и некоторыми другими типами элементов',
            ],
            'priority'           => [
                'label' => 'Приоритет',
            ],
            'changefreq'         => [
                'label'   => 'Частота изменений',
                'options' => [
                    'always'  => 'всегда',
                    'hourly'  => 'ежечасно',
                    'daily'   => 'ежедневно',
                    'weekly'  => 'еженедельно',
                    'monthly' => 'ежемесячно',
                    'yearly'  => 'годовой',
                    'never'   => 'никогда',
                ],
            ],
            'location'           => 'Расположение:',
            'add_item'           => 'Добавить элемент',
            'editor_title'       => 'Элемент карты сайта',
            'unnamed'            => 'Элемент без имени',
            'reference_required' => 'Ссылка на документ обязательна',
            'url_required'       => 'URL обязателен для заполнения',
            'cms_page_required'  => 'Пожалуйста, выберите страницу CMS',
        ],
    ],
];