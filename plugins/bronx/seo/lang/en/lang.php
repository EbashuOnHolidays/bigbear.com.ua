<?php

return [
    'plugin'      => [
        'name'        => 'SEO',
        'description' => 'SEO assistant',
    ],
    'components'  => [
        'widget' => [
            'name'        => 'Meta information',
            'description' => 'Output meta tags about the page',
        ],
    ],
    'permissions' => [
        'settings' => [
            'name' => 'Manage plugin settings',
            'tab'  => 'Bronx SEO',
        ],
    ],
    'formwidget'  => [
        'helpblock' => [
            'letter' => 'Length: :length symbols. Recommended length: :maxLength symbols',
            'tag'    => 'Length: :length tags. Recommended length: :maxLength tags',
        ],
    ],
    'settings'    => [
        'seo_name'            => 'META information',
        'seo_description'     => 'Configuring meta information about a resource',
        'sitemap_name'        => 'Site map',
        'sitemap_description' => 'Site map settings',
        'form'                => [
            'tab_meta'           => 'META',
            'tab_graphics'       => 'Visual design',
            'meta_section'       => [
                'label'   => 'Meta resource information',
                'comment' => 'This information is used when displaying meta tags if the plug-in component does not specify other',
            ],
            'meta_index'         => [
                'label'   => 'Indexing',
                'options' => [
                    'index'   => 'index: enable indexing',
                    'noindex' => 'noindex: prohibit indexing',
                ],
            ],
            'meta_follow'        => [
                'label'   => 'Follow links',
                'options' => [
                    'follow'   => 'follow: allow following',
                    'nofollow' => 'nofollow: prohibit following',
                ],
            ],
            'meta_title'         => [
                'label' => 'Resource name (meta title)',
            ],
            'meta_author'        => [
                'label' => 'Resource owner (meta author)',
            ],
            'meta_viewport'      => [
                'label' => 'Viewer settings (meta viewport)',
            ],
            'meta_description'   => [
                'label' => 'Description (meta description)',
            ],
            'meta_keywords'      => [
                'label' => 'Keywords (meta keywords)',
            ],
            'meta_canonical'     => [
                'label' => 'Canonical URL (meta canonical)',
            ],
            'meta_redirect'      => [
                'label' => 'Redirecting to a URL (meta redirect)',
            ],
            'meta_repeater'      => [
                'label'  => 'More meta tags',
                'prompt' => 'Add Tag',
                'form'   => [
                    'name'    => [
                        'label'        => 'Meta name',
                        'commentAbove' => 'For example: google-site-verification',
                    ],
                    'content' => [
                        'label'        => 'Meta content',
                        'commentAbove' => 'For example: _X5TA_ji3wUIXDWZkab3Z5tF3TTlhLKZIbqkdy8MHZM',
                    ],
                ],
            ],
            'graphics_section'   => [
                'label'   => 'Graphics management',
                'comment' => 'Configure Favicon, App Icon and App Color to integrate the site into the user\'s browser',
            ],
            'webapp_title'       => [
                'label'        => 'Application Name',
                'commentAbove' => 'Signed under the Appicon icon on the home screen',
            ],
            'appicon'            => [
                'label'        => 'Appicon',
                'commentAbove' => 'Upload a .png or .jpg image with a size of 192x192',
            ],
            'is_webapp'             => [
                'label'        => 'Open as a mobile app',
                'commentAbove' => 'A web application added to the main screen will be launched as a mobile application',
            ],
            'webapp_display'     => [
                'label'        => 'Configuring the browser interface when the application starts',
                'commentAbove' => 'For example, you can hide the browser interface and display the site completely in full screen',
                'options'      => [
                    'fullscreen' => 'Fullscreen: opens the web application without any browser UI and takes up the entirety of the available display area.',
                    'standalone' => 'Standalone: opens the web app in it\'s own window, separate from the browser, and hides standard browser UI elements like the URL bar, etc.',
                    'minimal-ui' => 'Minimal-ui: (not supported by Chrome) this mode is similar to fullscreen, but provides the user with some means to access a minimal set of UI elements for controlling navigation (i.e., back, forward, reload, etc).',
                    'browser'    => 'Browser: a standard browser experience.',
                ],
            ],
            'appcolor'           => [
                'label'        => 'Main color',
                'commentAbove' => 'Specify the main color of the website. Displays in the address bar of some mobile browsers',
            ],
            'humans'             => [
                'commentAbove' => 'Documentation: http://humanstxt.org',
            ],
            'robots'             => [
                'commentAbove' => 'Documentation: http://www.robotstxt.org/robotstxt.html',
            ],
            'type'               => [
                'label' => 'Type',
            ],
            'url'                => [
                'label' => 'URL',
            ],
            'reference'          => [
                'label' => 'Reference',
            ],
            'cms_page'           => [
                'label'   => 'CMS Page',
                'comment' => 'Select the page to use for the URL address.',
            ],
            'allow_nested_items' => [
                'label'   => 'Allow nested items',
                'comment' => 'Nested items could be generated dynamically by static page and some other item types',
            ],
            'priority'           => [
                'label' => 'Priority',
            ],
            'changefreq'         => [
                'label'   => 'Change frequency',
                'options' => [
                    'always'  => 'always',
                    'hourly'  => 'hourly',
                    'daily'   => 'daily',
                    'weekly'  => 'weekly',
                    'monthly' => 'monthly',
                    'yearly'  => 'yearly',
                    'never'   => 'never',
                ],
            ],
            'location'           => 'Location:',
            'add_item'           => 'Add item',
            'editor_title'       => 'Sitemap item',
            'unnamed'            => 'Unnamed item',
            'reference_required' => 'The item reference is required.',
            'url_required'       => 'The URL is required',
            'cms_page_required'  => 'Please select a CMS page',
        ],
    ],
];