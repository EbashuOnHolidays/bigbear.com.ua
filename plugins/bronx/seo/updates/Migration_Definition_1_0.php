<?php namespace Bronx\SEO\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_Definition_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_seo_tab_definition', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('theme')->nullable()->index();
            $table->mediumtext('data')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_seo_tab_definition');
    }
}