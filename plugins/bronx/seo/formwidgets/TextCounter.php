<?php namespace Bronx\SEO\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Backend\Classes\FormField;

class TextCounter extends FormWidgetBase
{
    public $defaultAlias = 'textcounter';

    public $maxLength;
    public $counterType;

    public function init()
    {
        $this->fillFromConfig([
            'maxLength',
            'counterType',
        ]);
    }

    public function render()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['field'] = $this->formField;
        $this->vars['maxLength'] = $this->maxLength ?? 60;
        $this->vars['counterType'] = $this->counterType ?? 'letter';

        return $this->makePartial('textcounter');
    }

    public function getSaveValue($value)
    {
        if ($this->formField->disabled || $this->formField->hidden) {
            return FormField::NO_SAVE_DATA;
        }

        if (!strlen($value)) {
            return null;
        }

        return $value;
    }

    public function loadAssets()
    {
        $this->addCss('css/form.css');
        $this->addJs('js/form.js');
    }
}