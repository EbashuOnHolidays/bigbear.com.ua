!function ($) {
    'use strict';

    $(document).ready(function () {
        $('[data-control="field-count-letter"]').keyup(function (event) {
            var $this = $(this);
            var $counterString = $this.next();

            var length = $this.val().length;

            applyClass($counterString, length, $this.data('max-length'));
        });

        $('[data-control="field-count-tag"]').keyup(function (event) {
            var $this = $(this);
            var $counterString = $this.next();

            var length = 0;

            if ($this.val().length > 0) {
                length = $this.val().replace(/\s/g, '').replace(/,$/g, '').split(',').length;
            }

            applyClass($counterString, length, $this.data('max-length'));
        });

        function applyClass(object, length, maxLength) {
            object.find('span').text(length);

            if (length < maxLength * 0.9) {
                object.removeClass('text-warning text-danger');
                object.addClass('text-success');
            } else if (length >= maxLength * 0.9 && length <= maxLength) {
                object.removeClass('text-success text-danger');
                object.addClass('text-warning');
            } else {
                object.removeClass('text-success text-warning');
                object.addClass('text-danger');
            }
        }
    });
}(jQuery);