<?php

use Bronx\SEO\Classes\Compress;
use Bronx\SEO\Models\Definition;
use Bronx\SEO\Models\Settings;
use Cms\Classes\Controller;
use Cms\Classes\Theme;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use October\Rain\Parse\Twig;

Route::get('robots.txt', [
    'as' => 'seo_robots',
    function () {
        $twig = new Twig();
        $settings = Settings::instance();

        return Response::make($twig->parse($settings->robots, [
            'host'     => url('/'),
            'settings' => $settings,
        ]), 200, ['Content-Type' => 'text/plain']);
    },
]);

Route::get('humans.txt', [
    'as' => 'seo_humans',
    function () {
        $twig = new Twig();
        $settings = Settings::instance();

        return Response::make($twig->parse($settings->humans, [
            'settings' => $settings,
        ]), 200, ['Content-Type' => 'text/plain']);
    },
]);

Route::get('sitemap.xml', function () {
    $themeActive = Theme::getActiveTheme()
        ->getDirName();

    try {
        $definition = Definition::where('theme', $themeActive)
            ->firstOrFail();
    } catch (ModelNotFoundException $exception) {
        Log::info('Site map not found');

        return App::make(Controller::class)
            ->setStatusCode(404)
            ->run('/404');
    }

    return Response::make($definition->generateSitemap())
        ->header("Content-Type", "application/xml");
});

Route::get('manifest.json', [
    'as' => 'seo_manifest',
    function () {
        $settings = Settings::instance();

        $content = View::make('bronx.seo::manifest', [
            'settings' => $settings,
        ])->render();

        return Response::make(Compress::json($content), 200, ['Content-Type' => 'application/json']);
    },
]);