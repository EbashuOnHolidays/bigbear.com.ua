+function ($) {
    'use strict';
    var OnChangeRecord = function () {
        this.changeCategory = function () {
            var onChangeCategoryPopup = $('<a />');

            onChangeCategoryPopup.popup({
                handler: 'onChangeCategoryForm',
                size: 'small',
                extraData: {
                    'checked': $('.control-list').listWidget('getChecked')
                }
            });
        };

        this.changeStatus = function () {
            var onChangeStatusPopup = $('<a />');

            onChangeStatusPopup.popup({
                handler: 'onChangeStatusForm',
                size: 'small',
                extraData: {
                    'checked': $('.control-list').listWidget('getChecked')
                }
            });
        };

        this.changePrice = function () {
            var onChangePricePopup = $('<a />');

            onChangePricePopup.popup({
                handler: 'onChangePriceForm',
                size: 'small',
                extraData: {
                    'checked': $('.control-list').listWidget('getChecked')
                }
            });
        };
    };

    $.onChangeRecord = new OnChangeRecord;
}(window.jQuery);