<?php namespace Bronx\Shop\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Backend\Classes\FormField;

class Unit extends FormWidgetBase
{
    public $defaultAlias = 'unit';

    public $symbol;
    public $position;

    public function init()
    {
        $this->fillFromConfig([
            'symbol',
            'position',
        ]);
    }

    public function render()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['field'] = $this->formField;
        $this->vars['symbol'] = $this->symbol ?? '$';
        $this->vars['position'] = $this->position ?? 'left';

        return $this->makePartial('unit');
    }

    public function getSaveValue($value)
    {
        if ($this->formField->disabled || $this->formField->hidden) {
            return FormField::NO_SAVE_DATA;
        }

        if (!strlen($value)) {
            return null;
        }

        return $value;
    }

    public function loadAssets()
    {
        $this->addCss('css/form.css');
    }
}