<?php namespace Bronx\Shop;

use Backend\Facades\Backend;
use Bronx\Shop\Classes\Cache;
use Bronx\Shop\Classes\CacheObject;
use Bronx\Shop\Console\CacheGenerator;
use Bronx\Shop\Models\ProductCategory;
use Bronx\Shop\Models\ProductColor;
use Bronx\Shop\Models\ProductGroup;
use Bronx\Shop\Models\ProductSize;
use Bronx\Shop\Models\ProductStatus;
use Bronx\Shop\Components\Basket;
use Bronx\Shop\Components\CategoryList;
use Bronx\Shop\Components\Catalog;
use Bronx\Shop\Console\PriceGenerator;
use Bronx\Shop\FormWidgets\Unit;
use Bronx\Shop\Models\Product;
use Bronx\Shop\Models\Settings;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Shop',
            'description' => '...',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-shopping-cart',
        ];
    }

    public function register()
    {
        /*
         * Register facades
         */
        App::singleton('shop.cache', function () {
            return Cache::instance();
        });

        App::singleton('shop.cacheObject', function () {
            return CacheObject::instance();
        });

        /*
         * Register console
         */
        $this->registerConsoleCommand('shop.cachegenerator', CacheGenerator::class);
        $this->registerConsoleCommand('shop.pricegenerator', PriceGenerator::class);
    }

    public function boot()
    {
        /*
         * SITEMAP
         */
        Event::listen('pages.menuitem.listTypes', function () {
            return [
                'projectCatalogProductCategory' => 'Интернет-магазин: категории',
                'projectCatalogProduct'         => 'Интернет-магазин: товары',
            ];
        });

        Event::listen('pages.menuitem.getTypeInfo', function ($type) {
            if ($type == 'projectCatalogProductCategory') {
                return ProductCategory::getMenuTypeInfo($type);
            } else {
                if ($type == 'projectCatalogProduct') {
                    return Product::getMenuTypeInfo($type);
                }
            }
        });

        Event::listen('pages.menuitem.resolveItem', function ($type, $item, $url, $theme) {
            if ($type == 'projectCatalogProductCategory') {
                return ProductCategory::resolveMenuItem($item, $url, $theme);
            } else {
                if ($type == 'projectCatalogProduct') {
                    return Product::resolveMenuItem($item, $url, $theme);
                }
            }
        });

        /*
         * EXTEND SORTING
         */
        Event::listen('bronx.shop.sort.values', function () {
            return [
                'ascending'  => 'Цена: от низкой к высокой',
                'descending' => 'Цена: от высокой к низкой',
            ];
        });

        Event::listen('bronx.shop.sort.offer', function (&$offers, $request) {
            $offers->rightJoin('bronx_shop_tab_product', 'bronx_shop_tab_product.id', '=', 'bronx_shop_tab_offer.product_id');
            $offers->select('bronx_shop_tab_offer.*', 'bronx_shop_tab_product.price_sale', 'bronx_shop_tab_product.price_discount');

            switch ($request['sorting']) {
                case 'descending':
                    $offers->orderBy('bronx_shop_tab_product.price_discount', 'DESC');
                    $offers->orderBy('bronx_shop_tab_product.price_sale', 'DESC');
                    $offers->orderBy('id', 'DESC');
                    break;
                case 'ascending':
                default:
                    $offers->orderBy('bronx_shop_tab_product.price_discount', 'ASC');
                    $offers->orderBy('bronx_shop_tab_product.price_sale', 'ASC');
                    $offers->orderBy('id', 'ASC');
                    break;
            }
        });

        /*
         * EXTEND COLORS
         */
        Event::listen('bronx.shop.filter.init', function ($model) {
            $productsIds = $model->lists('id');

            if ($productsIds != null) {
                $filters = ProductColor::withCount([
                    'relProduct' => function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    },
                ])
                    ->whereHas('relProduct', function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    })
                    ->get();

                return [
                    'name'   => 'Цвет',
                    'slug'   => 'color',
                    'size'   => 1,
                    'type'   => 'checkbox',
                    'values' => $filters,
                ];
            }
        }, 1000);

        Event::listen('bronx.shop.filter.selection', function (&$model, $request) {
            if (!empty($request['filter']['color'])) {
                $model->whereHas('relProductColor', function ($color) use ($request) {
                    $color->whereIn('slug', $request['filter']['color']);
                });
            }
        });

        Event::listen('bronx.shop.product.extend', function (&$model) {
            $model->with(['relProductColor']);
        });

        /*
         * EXTEND GROUPS
         */
        Event::listen('bronx.shop.filter.init', function ($model) {
            $productsIds = $model->lists('id');

            if ($productsIds != null) {
                $filters = ProductGroup::withCount([
                    'relProduct' => function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    },
                ])
                    ->whereHas('relProduct', function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    })
                    ->get();

                return [
                    'name'   => 'Группа',
                    'slug'   => 'group',
                    'size'   => 1,
                    'type'   => 'checkbox',
                    'values' => $filters,
                ];
            }
        }, 1200);

        Event::listen('bronx.shop.filter.selection', function (&$model, $request) {
            if (!empty($request['filter']['group'])) {
                $model->whereHas('relProductGroup', function ($group) use ($request) {
                    $group->whereIn('slug', $request['filter']['group']);
                });
            }
        });

        Event::listen('bronx.shop.product.extend', function (&$model) {
            $model->with(['relProductGroup']);
        });

        /*
         * EXTEND SIZES
         */
        Event::listen('bronx.shop.filter.init', function ($model) {
            $productsIds = $model->lists('id');

            if ($productsIds != null) {
                $filters = ProductSize::withCount([
                    'relProduct' => function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    },
                ])
                    ->whereHas('relProduct', function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    })
                    ->get();

                return [
                    'name'   => 'Размер',
                    'slug'   => 'size',
                    'size'   => 1,
                    'type'   => 'checkbox',
                    'values' => $filters,
                ];
            }
        }, 1000);

        Event::listen('bronx.shop.filter.selection', function (&$model, $request) {
            if (!empty($request['filter']['size'])) {
                $model->whereHas('relProductSize', function ($size) use ($request) {
                    $size->whereIn('slug', $request['filter']['size']);
                });
            }
        });

        Event::listen('bronx.shop.product.extend', function (&$model) {
            $model->with(['relProductSize']);
        });

        /*
         * EXTEND STATUSES
         */
        Event::listen('bronx.shop.filter.init', function ($model) {
            $productsIds = $model->lists('id');

            if ($productsIds != null) {
                $filters = ProductStatus::withCount([
                    'relProduct' => function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    },
                ])
                    ->whereHas('relProduct', function ($product) use ($productsIds) {
                        $product->whereIn('id', $productsIds);
                    })
                    ->get();

                return [
                    'name'   => 'Наличие',
                    'slug'   => 'status',
                    'size'   => 1,
                    'type'   => 'checkbox',
                    'values' => $filters,
                ];
            }
        }, 950);

        Event::listen('bronx.shop.filter.selection', function (&$model, $request) {
            if (!empty($request['filter']['status'])) {
                $model->whereHas('relProductStatus', function ($status) use ($request) {
                    $status->whereIn('slug', $request['filter']['status']);
                });
            }
        });

        Event::listen('bronx.shop.product.extend', function (&$model) {
            $model->with(['relProductStatus']);
        });
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            // Отключение акций по таймеру
            $products = Product::where('price_sale', '!=', 'price_discount')
                ->where('discounted_at', '<', Carbon::now())
                ->get();

            if ($products) {
                // TODO: Notification admin to messenger
                $products->each(function ($product) {
                    $product->price_discount = $product->price_sale;
                    $product->save();
                });
            }
        })->everyMinute();
    }

    public function registerComponents()
    {
        return [
            Basket::class       => 'shopBasket',
            CategoryList::class => 'shopCategoryList',
            Catalog::class      => 'shopCatalog',
        ];
    }

    public function registerPermissions()
    {
        return [
            'bronx.shop.send_orders' => [
                'label' => 'Отправлять сообщения о новых заказах',
                'tab'   => 'Магазин',
                'order' => 50,
                'roles' => ['publisher'],
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'product' => [
                'label'   => 'Магазин',
                'url'     => Backend::url('bronx/shop/products'),
                'icon'    => 'icon-shopping-cart',
                'iconSvg' => 'plugins/bronx/shop/assets/images/products.svg',
                'order'   => 52,

                'sideMenu' => [
                    'products'          => [
                        'label' => 'Товары',
                        'url'   => Backend::url('bronx/shop/products'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'productcategories' => [
                        'label' => 'Категории',
                        'url'   => Backend::url('bronx/shop/productcategories'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'productcolors'     => [
                        'label' => 'Цвета',
                        'url'   => Backend::url('bronx/shop/productcolors'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'productgroups'     => [
                        'label' => 'Группы',
                        'url'   => Backend::url('bronx/shop/productgroups'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'productsizes'      => [
                        'label' => 'Размеры',
                        'url'   => Backend::url('bronx/shop/productsizes'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'productstatuses'   => [
                        'label' => 'Статусы',
                        'url'   => Backend::url('bronx/shop/productstatuses'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                ],
            ],
            'order'   => [
                'label'   => 'Заказы',
                'url'     => Backend::url('bronx/shop/orders'),
                'icon'    => 'icon-shopping-cart',
                'iconSvg' => 'plugins/bronx/shop/assets/images/orders.svg',
                'order'   => 54,

                'sideMenu' => [
                    'orders'                => [
                        'label' => 'Заказы',
                        'url'   => Backend::url('bronx/shop/orders'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'orderdeliverygateways' => [
                        'label' => 'Шлюзы доставки',
                        'url'   => Backend::url('bronx/shop/orderdeliverygateways'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'orderdeliverystatuses' => [
                        'label' => 'Статусы доставки',
                        'url'   => Backend::url('bronx/shop/orderdeliverystatuses'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'orderpaymentgateways'  => [
                        'label' => 'Шлюзы оплаты',
                        'url'   => Backend::url('bronx/shop/orderpaymentgateways'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'orderpaymentstatuses'  => [
                        'label' => 'Статусы оплаты',
                        'url'   => Backend::url('bronx/shop/orderpaymentstatuses'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Интернет-магазин',
                'description' => 'Управление настройками интернет-магазина',
                'category'    => 'Bronx',
                'icon'        => 'icon-cog',
                'class'       => Settings::class,
                'order'       => 100,
            ],
        ];
    }

    public function registerFormWidgets()
    {
        return [
            Unit::class => [
                'label' => 'Unit field',
                'code'  => 'unit',
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters'   => [
                'numeric'  => function ($amount, $decimals = 0, $decPoint = '.', $thousandsSep = ' ') {
                    return number_format($amount, $decimals, $decPoint, $thousandsSep);
                },
                'plural'   => function ($number, $params) {
                    $cases = [2, 0, 1, 1, 1, 2];

                    return $params[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
                },
            ],
            'functions' => [
                'getTrigger'    => function ($key, $default = null) {
                    return Session::get('trigger.' . $key, $default);
                },
                'putTrigger'    => function ($key, $value) {
                    return Session::put('trigger.' . $key, $value);
                },
                'forgetTrigger' => function ($key) {
                    return Session::forget('trigger.' . $key);
                },
            ],
        ];
    }
}