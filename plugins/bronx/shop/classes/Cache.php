<?php namespace Bronx\Shop\Classes;

use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache as CacheBase;
use October\Rain\Extension\ExtendableTrait;
use October\Rain\Support\Traits\Singleton;

class Cache
{
    use Singleton;
    protected static $instance;

    use ExtendableTrait;

    /*
     * BASIC METHODS
     */
    public function has($key)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        return CacheBase::has($key);
    }

    public function get($key)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        return CacheBase::get($key);
    }

    public function put($key, $value, $time)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        return CacheBase::put($key, $value, $time);
    }

    public function remember($key, $time, Closure $value)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        return CacheBase::remember($key, $time, $value);
    }

    public function forever($key, $value)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        return CacheBase::forever($key, $value);
    }

    public function forget($key)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        return CacheBase::forget($key);
    }

    public function unset($key, $unsetKey)
    {
        if (is_array($key)) {
            $key = implode('_', $key);
        }

        $data = Cache::pull($key);

        unset($data[$unsetKey]);

        Cache::put($key, $data,60);
    }

    /*
     * ADDITIONAL METHODS
     */
    public function attempt($key, $class, Closure $callback)
    {
        $data = [];

        if ($this->has($key)) {
            $dataIndex = $this->get($key);

            if (is_array($dataIndex)) {
                foreach ($dataIndex as $modelKey) {
                    $data[] = $this->get($modelKey);
                }
            } else {
                $data = $this->getAndSaveModel($key, $class, $callback);
            }
        } else {
            $data = $this->getAndSaveModel($key, $class, $callback);
        }

        return $data;
    }

    private function getAndSaveModel($key, $class, Closure $callback)
    {
        $data = $this->convertCallback($class, $callback);

        $dataIndex = array_keys($data);

        // Save indexes to Cache
        $this->forever($key, $dataIndex);

        // Save models to Cache
        foreach ($data as $modelKey => $modelValue) {
            $this->forever($modelKey, $modelValue);
        }

        return $data;
    }

    public function convertCallback($class, Closure $callback)
    {
        $data = $callback($class);

        return $this->convertObject($data);
    }

    public function convertObject($object)
    {
        $objectData = [];

        if ($object instanceof Collection) {
            foreach ($object as $model) {
                $objectData = array_merge($objectData, $this->convertModel($model));
            }
        } else {
            $objectData = $this->convertModel($object);
        }

        return $objectData;
    }

    private function convertModel($model)
    {
        $fields = array_keys($model->attributes + $model->relationsToArray());

        if (empty($fields)) {
            return null;
        }

        $modelData = [];
        $modelTag = str_replace('\\', '_', get_class($model));
        $modelKey = $modelTag . '_id_' . $model->id;

        foreach ($fields as $field) {
            $modelData[$modelKey][$field] = $model->{$field};
        }

        return $modelData;
    }
}