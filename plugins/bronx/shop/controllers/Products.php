<?php namespace Bronx\Shop\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Behaviors\ReorderController;
use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use Bronx\Shop\Models\Product;
use Bronx\Shop\Models\ProductCategory;
use Bronx\Shop\Models\ProductStatus;
use Illuminate\Support\Facades\Input;
use October\Rain\Support\Facades\Flash;

class Products extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
        ReorderController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $bodyClass = '';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.Shop', 'product', 'products');
    }

    public function index()
    {
        $this->addCss('/plugins/bronx/shop/assets/styles/customSwitch.css');
        $this->addCss('/plugins/bronx/shop/assets/styles/form.css');
        $this->addJs('/plugins/bronx/shop/assets/scripts/onChangeRecord.js');

        $this->asExtension('ListController')->index();
    }

    public function create()
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->create();
    }

    public function update($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    public function preview($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    /*
     * AJAX
     */
    public function onRefreshCacheObject()
    {
        Flash::success('Кэш объектов успешно обновлен');
    }

    public function onClearCacheKey()
    {
        Flash::success('Кэш ключей успешно очищен');
    }

    public function onClearCacheObject()
    {
        Flash::success('Кэш объектов успешно очищен');
    }

    public function onChangeCategoryForm()
    {
        $this->vars['checked'] = Input::get('checked');
        $this->vars['categories'] = ProductCategory::listsNested('name', 'id');

        return $this->makePartial('popup_change_category_form');
    }

    public function onChangeStatusForm()
    {
        $this->vars['checked'] = Input::get('checked');
        $this->vars['statuses'] = ProductStatus::lists('name', 'id');

        return $this->makePartial('popup_change_status_form');
    }

    public function onChangePriceForm()
    {
        $this->vars['checked'] = Input::get('checked');

        return $this->makePartial('popup_change_price_form');
    }

    public function onChangeColumnValueFast()
    {
        $checkedIds = Input::get('checked');
        $columnName = Input::get('column_name');
        $columnValue = Input::get('column_value');

        if (!empty($columnName) && !empty($checkedIds)) {
            Product::whereIn('id', $checkedIds)
                ->where($columnName, '!=', $columnValue)
                ->update([
                    $columnName => $columnValue,
                ]);
        }

        return $this->listRefresh();
    }

    public function onReplicate()
    {
        $checkedIds = Input::get('checked');

        if ($checkedIds != null) {
            $products = Product::whereIn('id', $checkedIds)
                ->get();

            $products->each(function ($product) {
                $replicateProduct = $product->replicate();
                $replicateProduct->is_enable = false;
                $replicateProduct->save();
            });
        }

        return $this->listRefresh();
    }
}