<?php namespace Bronx\Shop\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Behaviors\ReorderController;
use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use Bronx\Shop\Models\ProductCategory;
use Illuminate\Support\Facades\Input;

class ProductCategories extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
        ReorderController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $bodyClass = '';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.Shop', 'product', 'productcategories');
    }

    public function index()
    {
        $this->addCss('/plugins/bronx/shop/assets/styles/customSwitch.css');

        $this->asExtension('ListController')->index();
    }

    public function create()
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->create();
    }

    public function update($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    public function preview($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    /*
     * AJAX
     */
    public function onChangeColumnValueFast()
    {
        $checkedIds = Input::get('checked');
        $columnName = Input::get('column_name');
        $columnValue = Input::get('column_value');

        if (!empty($columnName) && !empty($checkedIds)) {
            $record = ProductCategory::whereIn('id', $checkedIds)
                ->where($columnName, '!=', $columnValue)
                ->get();

            $record->each(function ($record) use ($columnName, $columnValue) {
                $record->{$columnName} = $columnValue;
                $record->save();
            });
        }

        return $this->listRefresh();
    }
}