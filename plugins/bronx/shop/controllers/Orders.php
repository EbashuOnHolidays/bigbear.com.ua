<?php namespace Bronx\Shop\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Behaviors\RelationController;
use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class Orders extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
        RelationController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $bodyClass;

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.Shop', 'order', 'orders');
    }

    public function create()
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->create();
    }

    public function update($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    public function preview($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    /*
     * EXTEND RELATION CONTROLLER
     */
    public function relationExtendPivotWidget($widget, $field, $model)
    {
        if ($field == 'relProduct') {
            $widget->fields["pivot[price_purchase]"]['default'] = $widget->model->price_purchase;
            $widget->fields["pivot[price_markup]"]['default'] = $widget->model->price_markup;
            $widget->fields["pivot[price_discount]"]['default'] = $widget->model->price_discount;
            $widget->fields["pivot[total_quantity]"]['default'] = 1;
        }
    }
}