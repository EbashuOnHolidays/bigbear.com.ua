<?php namespace Bronx\Shop\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use Bronx\Shop\Models\ProductStatus;
use Illuminate\Support\Facades\Input;
use October\Rain\Support\Facades\Flash;

class ProductStatuses extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.Shop', 'product', 'productstatuses');
    }

    public function index()
    {
        $this->addCss('/plugins/bronx/shop/assets/styles/customSwitch.css');

        $this->asExtension('ListController')->index();
    }

    /*
     * AJAX
     */
    public function onChangeColumnValueFast()
    {
        $checkedIds = Input::get('checked');
        $columnName = Input::get('column_name');
        $columnValue = Input::get('column_value');

        if (!empty($columnName) && !empty($checkedIds)) {
            ProductStatus::whereIn('id', $checkedIds)
                ->update([
                    $columnName => $columnValue,
                ]);

            Flash::success('Выбранные записи успешно изменены.');
        } else {
            Flash::error('Нет выбранных записей для изменения.');
        }

        return $this->listRefresh();
    }
}