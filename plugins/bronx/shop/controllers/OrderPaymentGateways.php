<?php namespace Bronx\Shop\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use Bronx\Shop\Models\OrderPaymentGateway;
use Illuminate\Support\Facades\Input;

class OrderPaymentGateways extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.Shop', 'order', 'orderpaymentgateways');
    }

    public function index()
    {
        $this->addCss('/plugins/bronx/shop/assets/styles/customSwitch.css');

        $this->asExtension('ListController')->index();
    }

    /*
     * AJAX
     */
    public function onChangeColumnValueFast()
    {
        $checkedIds = Input::get('checked');
        $columnName = Input::get('column_name');
        $columnValue = Input::get('column_value');

        if (!empty($columnName) && !empty($checkedIds)) {
            OrderPaymentGateway::whereIn('id', $checkedIds)
                ->where($columnName, '!=', $columnValue)
                ->update([
                    $columnName => $columnValue,
                ]);
        }

        return $this->listRefresh();
    }
}