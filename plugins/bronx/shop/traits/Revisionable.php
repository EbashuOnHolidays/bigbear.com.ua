<?php namespace Bronx\Shop\Traits;

use Backend\Facades\BackendAuth;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Facades\DB;

trait Revisionable
{
    public static function bootRevisionable()
    {
        if (!property_exists(get_called_class(), 'revisionable')) {
            throw new Exception(sprintf(
                'You must define a $revisionable property in %s to use the Revisionable trait.', get_called_class()
            ));
        }

        static::extend(function ($model) {
            $model->bindEvent('model.afterUpdate', function () use ($model) {
                $model->revisionableAfterUpdate();
            });
        });
    }

    public function revisionableAfterUpdate()
    {
        $relationObject = $this->relRevision();
        $revisionModel = $relationObject->getRelated();
        $user = BackendAuth::getUser();

        $toSave = [];
        $dirty = $this->getDirty();
        foreach ($dirty as $attribute => $value) {
            if (!in_array($attribute, $this->revisionable)) {
                continue;
            }

            $toSave[] = [
                'field'             => $attribute,
                'old_value'         => array_get($this->original, $attribute),
                'new_value'         => $value,
                'revisionable_type' => $relationObject->getMorphClass(),
                'revisionable_id'   => $this->getKey(),
                'user_id'           => $user->id ?? null,
                'cast'              => $this->revisionableGetCastType($attribute),
                'created_at'        => new DateTime,
                'updated_at'        => new DateTime,
            ];
        }

        // Nothing to do
        if (!count($toSave)) {
            return;
        }

        Db::table($revisionModel->getTable())
            ->insert($toSave);

        foreach ($dirty as $attribute => $value) {
            $toDelete = $relationObject
                ->where('field', $attribute)
                ->orderBy('id', 'desc')
                ->skip(10)
                ->limit(64)
                ->get();

            foreach ($toDelete as $record) {
                $record->delete();
            }
        }
    }

    protected function revisionableGetCastType($attribute)
    {
        if (in_array($attribute, $this->getDates())) {
            return 'date';
        }

        return null;
    }
}