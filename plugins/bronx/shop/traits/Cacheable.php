<?php namespace Bronx\Shop\Traits;

trait Cacheable
{
    public function getCachedField(): array
    {
        if (empty($this->cached) || !is_array($this->cached)) {
            $this->cached = [];
        }

        return $this->cached;
    }
}