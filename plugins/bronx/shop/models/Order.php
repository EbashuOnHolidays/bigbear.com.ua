<?php namespace Bronx\Shop\Models;

use Bronx\User\Models\User;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class Order extends Model
{
    public $table = 'bronx_shop_tab_order';

    use Validation;
    public $rules = [
        'customer_name'             => 'required',
        'customer_surname'          => 'required',
        'customer_phone'            => 'required',
        'recipient_name'            => 'required_if:is_gift,1',
        'recipient_surname'         => 'required_if:is_gift,1',
        'recipient_phone'           => 'required_if:is_gift,1',
        'address'                   => 'required',
        'order_delivery_gateway_id' => 'required',
        'order_delivery_status_id'  => 'required',
        'order_payment_gateway_id'  => 'required',
        'order_payment_status_id'   => 'required',
    ];

    public $hasMany = [
        'relOrderDeliveryLog' => [
            OrderDeliveryLog::class,
            'delete' => true,
        ],
        'relOrderPaymentLog'  => [
            OrderPaymentLog::class,
            'delete' => true,
        ],
    ];

    public $belongsTo = [
        'relOrderDeliveryGateway' => [
            OrderDeliveryGateway::class,
            'key' => 'order_delivery_gateway_id',
        ],
        'relOrderDeliveryStatus'  => [
            OrderDeliveryStatus::class,
            'key' => 'order_delivery_status_id',
        ],
        'relOrderPaymentGateway'  => [
            OrderPaymentGateway::class,
            'key' => 'order_payment_gateway_id',
        ],
        'relOrderPaymentStatus'   => [
            OrderPaymentStatus::class,
            'key' => 'order_payment_status_id',
        ],
        'relUser'                 => [
            User::class,
            'key' => 'user_id',
        ],
    ];

    public $belongsToMany = [
        'relProduct' => [
            Product::class,
            'table'      => 'bronx_shop_rel_order_product',
            'key'        => 'order_id',
            'otherKey'   => 'product_id',
            'timestamps' => true,
            'pivot'      => [
                'price_purchase',
                'price_markup',
                'price_discount',
                'price_profit',
                'price_old',
                'price_new',
                'total_quantity',
                'total_price_purchase',
                'total_price_markup',
                'total_price_discount',
                'total_price_profit',
                'total_price_old',
                'total_price_new',
            ],
        ],
    ];

    /*
     * AJAX
     */
    public function beforeSave()
    {
        $this->total_quantity = 0;
        $this->total_price_purchase = 0;
        $this->total_price_markup = 0;
        $this->total_price_discount = 0;
        $this->total_price_profit = 0;
        $this->total_price_old = 0;
        $this->total_price_new = 0;

        $products = $this->relProduct()
            ->get();

        $products->each(function ($product) {
            $this->total_quantity += $product->pivot->total_quantity;
            $this->total_price_purchase += $product->pivot->total_price_purchase;
            $this->total_price_markup += $product->pivot->total_price_markup;
            $this->total_price_discount += $product->pivot->total_price_discount;
            $this->total_price_profit += $product->pivot->total_price_profit;
            $this->total_price_old += $product->pivot->total_price_old;
            $this->total_price_new += $product->pivot->total_price_new;
        });
    }

    public function afterSave()
    {
        if ($this->isDirty(['order_delivery_gateway_id', 'order_delivery_status_id'])) {
            $this->relOrderDeliveryLog()->create([
                'order_delivery_gateway_id' => $this->order_delivery_gateway_id,
                'order_delivery_status_id'  => $this->order_delivery_status_id,
            ]);
        }

        if ($this->isDirty(['order_payment_gateway_id', 'order_payment_status_id'])) {
            $this->relOrderPaymentLog()->create([
                'order_payment_gateway_id' => $this->order_payment_gateway_id,
                'order_payment_status_id'  => $this->order_payment_status_id,
            ]);
        }
    }

    public function getOrderDeliveryGatewayIdOptions()
    {
        return [null => '-- не указано --'] + OrderDeliveryGateway::lists('name', 'id');
    }

    public function getOrderDeliveryStatusIdOptions()
    {
        $statuses = OrderDeliveryStatus::where('order_delivery_gateway_id', $this->order_delivery_gateway_id)
            ->lists('name', 'id');

        if ($statuses == null) {
            $statuses = [null => '-- не указано --'];
        }

        return $statuses;
    }

    public function getOrderPaymentGatewayIdOptions()
    {
        return [null => '-- не указано --'] + OrderPaymentGateway::lists('name', 'id');
    }

    public function getOrderPaymentStatusIdOptions()
    {
        $statuses = OrderPaymentStatus::where('order_payment_gateway_id', $this->order_payment_gateway_id)
            ->lists('name', 'id');

        if ($statuses == null) {
            $statuses = [null => '-- не указано --'];
        }

        return $statuses;
    }
}