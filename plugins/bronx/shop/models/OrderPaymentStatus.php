<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class OrderPaymentStatus extends Model
{
    public $table = 'bronx_shop_tab_order_payment_status';

    use Validation;
    public $rules = [
        'name'  => 'required',
        'code'  => 'required',
        'color' => 'required',
    ];

    public $hasMany = [
        'relOrder'           => [
            Order::class,
            'delete' => true,
        ],
        'relOrderCount'      => [
            Order::class,
            'count' => true,
        ],
        'relOrderPaymentLog' => [
            OrderPaymentLog::class,
            'delete' => true,
        ],
    ];

    public $belongsTo = [
        'relOrderPaymentGateway' => [
            OrderPaymentGateway::class,
            'key' => 'order_payment_gateway_id',
        ],
    ];
}