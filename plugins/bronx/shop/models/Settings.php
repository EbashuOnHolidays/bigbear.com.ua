<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'shop_settings';
    public $settingsFields = 'fields.yaml';

    public function initSettingsData()
    {
        $this->category_clever_redirect = true;
        $this->product_clever_redirect = true;
    }
}