<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class OrderDeliveryStatus extends Model
{
    public $table = 'bronx_shop_tab_order_delivery_status';

    use Validation;
    public $rules = [
        'name'  => 'required',
        'code'  => 'required',
        'color' => 'required',
    ];

    public $hasMany = [
        'relOrder'            => [
            Order::class,
            'delete' => true,
        ],
        'relOrderCount'       => [
            Order::class,
            'count' => true,
        ],
        'relOrderDeliveryLog' => [
            OrderDeliveryLog::class,
            'delete' => true,
        ],
    ];

    public $belongsTo = [
        'relOrderDeliveryGateway' => [
            OrderDeliveryGateway::class,
            'key' => 'order_delivery_gateway_id',
        ],
    ];
}