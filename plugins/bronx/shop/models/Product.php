<?php namespace Bronx\Shop\Models;

use Bronx\Shop\Traits\Revisionable;
use Bronx\Shop\Traits\Sluggable;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\Nullable;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;
use October\Rain\Exception\ApplicationException;

class Product extends Model
{
    public $table = 'bronx_shop_tab_product';

    public static $snakeAttributes = false;

    use Sortable;

    use Sluggable;
    protected $slugs = [
        'slug' => [
            'name',
            'relProductSize.slug',
            'relProductColor.slug',
        ],
    ];

    use Validation;
    public $rules = [
        'name'               => 'required',
        'name_h1'            => 'required',
        'relProductCategory' => 'required',
        'relProductColor'    => 'required',
        'relProductGroup'    => 'required',
        'relProductSize'     => 'required',
        'relProductStatus'   => 'required',
        'price_purchase'     => 'required',
        'weight'             => 'required',
        'volume'             => 'required',
    ];

    use Revisionable;
    protected $revisionable = [
        'slug',
    ];

    use Nullable;
    protected $nullable = [
        'short_description',
        'description',
        'characteristic',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    public $hasMany = [
        'relBasket' => [
            Basket::class,
            'delete' => true,
        ],
    ];

    public $belongsTo = [
        'relProductCategory' => [
            ProductCategory::class,
            'key' => 'product_category_id',
        ],
        'relProductColor'    => [
            ProductColor::class,
            'key' => 'product_color_id',
        ],
        'relProductGroup'    => [
            ProductGroup::class,
            'key' => 'product_group_id',
        ],
        'relProductSize'     => [
            ProductSize::class,
            'key' => 'product_size_id',
        ],
        'relProductStatus'   => [
            ProductStatus::class,
            'key' => 'product_status_id',
        ],
    ];

    public $belongsToMany = [
        'relOrder' => [
            Order::class,
            'table'      => 'bronx_shop_rel_order_product',
            'key'        => 'product_id',
            'otherKey'   => 'order_id',
            'timestamps' => true,
            'pivot'      => [
                'price_purchase',
                'price_markup',
                'price_discount',
                'price_profit',
                'price_old',
                'price_new',
                'total_quantity',
                'total_price_purchase',
                'total_price_markup',
                'total_price_discount',
                'total_price_profit',
                'total_price_old',
                'total_price_new',
            ],
        ],
    ];

    public $morphMany = [
        'relRevision' => [
            SystemRevision::class,
            'name' => 'revisionable',
        ],
    ];

    public $attachOne = [
        'relImageFront' => [
            SystemFile::class,
            'delete' => true,
        ],
        'relImageBack'  => [
            SystemFile::class,
            'delete' => true,
        ],
    ];

    public $attachMany = [
        'relImages' => [
            SystemFile::class,
            'delete' => true,
        ],
    ];

    public function takeUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->takeSlug(),
        ];

        return $controller->pageUrl($pageName, $params);
    }

    public function takeSlug()
    {
        $slug = $this->slug;
        $category = $this->relProductCategory()
            ->first();

        if ($category != null) {
            $parents = $category->getParentsAndSelf()
                ->reverse();

            $parents->each(function ($parent) use (&$slug) {
                $slug = $parent->slug . '/' . $slug;
            });
        }

        return $slug;
    }

    /*
     * EVENT
     */
    public function beforeSave()
    {
        if (!isset($this->relations['pivot'])) {
            if ($this->discounted_at == null) {
                $this->discounted_at = $this->created_at;
            }

            if ($this->price_markup == null) {
                $this->price_markup = ceil($this->price_purchase * 0.40 / 10) * 10;
            }

            if ($this->price_discount == null) {
                $this->price_discount = 0;
            }

            $this->price_profit = $this->price_markup - $this->price_discount;
            $this->price_old = $this->price_purchase + $this->price_markup;
            $this->price_new = $this->price_purchase + $this->price_profit;

            if ($this->price_profit <= 0) {
                throw new ApplicationException('Прибыль не может быть отрицательной');
            }
        } else {
            $price_purchase = $this->pivot->price_purchase;
            $price_markup = $this->pivot->price_markup;
            $price_discount = $this->pivot->price_discount;
            $price_profit = $price_markup - $price_discount;
            $price_old = $price_purchase + $price_markup;
            $price_new = $price_purchase + $price_profit;

            $total_quantity = $this->pivot->total_quantity;
            $total_price_purchase = $price_purchase * $total_quantity;
            $total_price_markup = $price_markup * $total_quantity;
            $total_price_discount = $price_discount * $total_quantity;
            $total_price_profit = $price_profit * $total_quantity;
            $total_price_old = $price_old * $total_quantity;
            $total_price_new = $price_new * $total_quantity;


            $this->pivot->price_purchase = $price_purchase;
            $this->pivot->price_markup = $price_markup;
            $this->pivot->price_discount = $price_discount;
            $this->pivot->price_profit = $price_profit;
            $this->pivot->price_old = $price_old;
            $this->pivot->price_new = $price_new;

            $this->pivot->total_quantity = $total_quantity;
            $this->pivot->total_price_purchase = $total_price_purchase;
            $this->pivot->total_price_markup = $total_price_markup;
            $this->pivot->total_price_discount = $total_price_discount;
            $this->pivot->total_price_profit = $total_price_profit;
            $this->pivot->total_price_old = $total_price_old;
            $this->pivot->total_price_new = $total_price_new;

            // Сохраняем связь
            $this->pivot->save();

            // Сохраняем заказ
//            $this->relations["pivot"]->parent->save();
        }
    }

    /*
     * SCOPE
     */
    public function scopeIsEnabled($query)
    {
        return $query->where('is_enable', true);
    }

    public function scopeIsDisabled($query)
    {
        return $query->where('is_enable', false);
    }

    /*
     * SITEMAP
     */
    public static function getMenuTypeInfo($type)
    {
        $theme = Theme::getActiveTheme();
        $pages = Page::listInTheme($theme, true);

        $cmsPages = [];
        foreach ($pages as $page) {
            if (!$page->hasComponent('shopCatalog')) {
                continue;
            }

            $properties = $page->getComponentProperties('shopCatalog');
            if (!isset($properties['catalogSlug']) || !preg_match('/{{\s*:/', $properties['catalogSlug'])) {
                continue;
            }

            $cmsPages[] = $page;
        }

        return [
            'cmsPages' => $cmsPages,
        ];
    }

    public static function resolveMenuItem($item, $url, $theme)
    {
        $records = self::orderBy('id')
            ->get();

        $result = [];
        foreach ($records as $record) {
            $result['items'][] = [
                'title' => $record->name,
                'url'   => self::getContentUrl($item->cmsPage, $record, $theme),
                'mtime' => $record->updated_at,
            ];
        }

        return $result;
    }

    protected static function getContentUrl($pageCode, $record, $theme)
    {
        $page = Page::loadCached($theme, $pageCode);

        if (!$page) {
            return;
        }

        $properties = $page->getComponentProperties('shopCatalog');

        if (!isset($properties['catalogSlug'])) {
            return;
        }

        if (!preg_match('/^\{\{([^\}]+)\}\}$/', $properties['catalogSlug'], $matches)) {
            return;
        }

        $paramName = substr(trim($matches[1]), 1);

        return Page::url($page->getBaseFileName(), [$paramName => $record->takeSlug()]);
    }
}