<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;

class OrderPaymentLog extends Model
{
    public $table = 'bronx_shop_tab_order_payment_log';

    public $fillable = [
        'order_payment_gateway_id',
        'order_payment_status_id',
    ];

    public $belongsTo = [
        'relOrder'               => [
            Order::class,
            'key' => 'order_id',
        ],
        'relOrderPaymentGateway' => [
            OrderPaymentGateway::class,
            'key' => 'order_payment_gateway_id',
        ],
        'relOrderPaymentStatus'  => [
            OrderPaymentStatus::class,
            'key' => 'order_payment_status_id',
        ],
    ];
}