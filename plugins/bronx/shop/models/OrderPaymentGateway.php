<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class OrderPaymentGateway extends Model
{
    public $table = 'bronx_shop_tab_order_payment_gateway';

    use Validation;
    public $rules = [
        'name'  => 'required',
        'class' => 'required',
    ];

    public $hasMany = [
        'relOrder'              => [
            Order::class,
            'delete' => true,
        ],
        'relOrderCount'         => [
            Order::class,
            'count' => true,
        ],
        'relOrderPaymentLog'    => [
            OrderPaymentLog::class,
            'delete' => true,
        ],
        'relOrderPaymentStatus' => [
            OrderPaymentStatus::class,
            'delete' => true,
        ],
    ];
}