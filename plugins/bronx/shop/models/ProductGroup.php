<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Models\Revision;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;

class ProductGroup extends Model
{
    public $table = 'bronx_shop_tab_product_group';

    use Sortable;

    use Sluggable;
    protected $slugs = [
        'slug' => 'name'
    ];

    use Validation;
    public $rules = [
        'name' => 'required',
        'slug' => 'unique:bronx_shop_tab_product_group,slug',
    ];

    public $hasMany = [
        'relProduct'      => [
            Product::class,
        ],
        'relProductCount' => [
            Product::class,
            'count' => true,
        ],
    ];

    public $morphMany = [
        'revision_history' => [
            Revision::class,
            'name' => 'revisionable',
        ],
    ];
}