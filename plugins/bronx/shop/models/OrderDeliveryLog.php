<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;

class OrderDeliveryLog extends Model
{
    public $table = 'bronx_shop_tab_order_delivery_log';

    public $fillable = [
        'order_delivery_gateway_id',
        'order_delivery_status_id',
    ];

    public $belongsTo = [
        'relOrder'                => [
            Order::class,
            'key' => 'order_id',
        ],
        'relOrderDeliveryGateway' => [
            OrderDeliveryGateway::class,
            'key' => 'order_delivery_gateway_id',
        ],
        'relOrderDeliveryStatus'  => [
            OrderDeliveryStatus::class,
            'key' => 'order_delivery_status_id',
        ],
    ];
}