<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class ProductColor extends Model
{
    public $table = 'bronx_shop_tab_product_color';

    use Validation;
    public $rules = [
        'name'            => 'required',
        'slug'            => 'required|unique:bronx_shop_tab_product_color,slug',
        'color_primary'   => 'required',
        'color_secondary' => '',
    ];

    public $hasMany = [
        'relProduct'      => [
            Product::class,
            'delete' => true,
        ],
        'relProductCount' => [
            Product::class,
            'count' => true,
        ],
    ];
}