<?php namespace Bronx\Shop\Models;

use Bronx\Shop\Traits\Revisionable;
use Bronx\Shop\Traits\Sluggable;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\NestedTree;
use October\Rain\Database\Traits\Validation;

class ProductCategory extends Model
{
    public $table = 'bronx_shop_tab_product_category';

    public static $snakeAttributes = false;

    public static $cacheKey = 'product_category';

    use NestedTree;

    use Sluggable;
    protected $slugs = [
        'slug' => 'name',
    ];

    use Validation;
    public $rules = [
        'name'        => 'required',
        'name_h1'     => 'required',
        'name_filter' => 'required',
        'slug'        => 'unique:bronx_shop_tab_product_category,slug',
    ];

    use Revisionable;
    protected $revisionable = [
        'slug',
    ];

    public $hasMany = [
        'relProduct'      => [
            Product::class,
            'delete' => true,
        ],
        'relProductCount' => [
            Product::class,
            'count' => true,
        ],
    ];

    public $morphMany = [
        'relRevision' => [
            SystemRevision::class,
            'name' => 'revisionable',
        ],
    ];

    public $attachOne = [
        'relImage' => [
            SystemFile::class,
            'delete' => true,
        ],
    ];

    public function takeUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->takeSlug(),
        ];

        return $controller->pageUrl($pageName, $params);
    }

    public function takeSlug()
    {
        $slug = $this->slug;
        $parents = $this->getParents();

        if ($parents != null) {
            $parents = $parents->reverse();

            $parents->each(function ($parent) use (&$slug) {
                $slug = $parent->slug . '/' . $slug;
            });
        }

        return $slug;
    }

    /*
     * EVENT
     */
    public function beforeSave()
    {
        if ($this->getRight() !== null && $this->getLeft() !== null) {
            $parents = $this->getParents();
            $leftCol = $this->getLeftColumnName();
            $rightCol = $this->getRightColumnName();
            $left = $this->getLeft();
            $right = $this->getRight();

            /*
             * Disable children
             */
            if ($this->is_enable == true) {
                // Проверяем включенность всех предков
                $parents->each(function ($parent) {
                    if ($parent->is_enable == false) {
                        $this->is_enable = false;
                        return;
                    }
                });
            } else {
                // Отключаем всех потомков
                $this->newQuery()
                    ->where($leftCol, '>', $left)
                    ->where($rightCol, '<', $right)
                    ->update([
                        'is_enable' => false,
                    ]);
            }
        }
    }

    /*
     * SCOPE
     */
    public function scopeIsEnabled($query)
    {
        return $query->where('is_enable', true);
    }

    public function scopeIsDisabled($query)
    {
        return $query->where('is_enable', false);
    }

    /*
     * SITEMAP
     */
    public static function getMenuTypeInfo($type)
    {
        $theme = Theme::getActiveTheme();
        $pages = Page::listInTheme($theme, true);

        $cmsPages = [];
        foreach ($pages as $page) {
            if (!$page->hasComponent('shopCatalog')) {
                continue;
            }

            $properties = $page->getComponentProperties('shopCatalog');
            if (!isset($properties['catalogSlug']) || !preg_match('/{{\s*:/', $properties['catalogSlug'])) {
                continue;
            }

            $cmsPages[] = $page;
        }

        return [
            'cmsPages' => $cmsPages,
        ];
    }

    public static function resolveMenuItem($item, $url, $theme)
    {
        $records = self::orderBy('id')
            ->get();

        $result = [];
        foreach ($records as $record) {
            $result['items'][] = [
                'title' => $record->name,
                'url'   => self::getContentUrl($item->cmsPage, $record, $theme),
                'mtime' => $record->updated_at,
            ];
        }

        return $result;
    }

    protected static function getContentUrl($pageCode, $record, $theme)
    {
        $page = Page::loadCached($theme, $pageCode);

        if (!$page) {
            return;
        }

        $properties = $page->getComponentProperties('shopCatalog');

        if (!isset($properties['catalogSlug'])) {
            return;
        }

        if (!preg_match('/^\{\{([^\}]+)\}\}$/', $properties['catalogSlug'], $matches)) {
            return;
        }

        $paramName = substr(trim($matches[1]), 1);

        return Page::url($page->getBaseFileName(), [$paramName => $record->takeSlug()]);
    }
}