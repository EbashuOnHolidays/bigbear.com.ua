<?php namespace Bronx\Shop\Models;

use System\Models\Revision as RevisionBase;

class SystemRevision extends RevisionBase
{
    public $table = 'bronx_shop_tab_system_revision';
}