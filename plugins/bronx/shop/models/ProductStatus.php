<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class ProductStatus extends Model
{
    public $table = 'bronx_shop_tab_product_status';

    use Validation;
    public $rules = [
        'name'  => 'required',
        'slug'  => 'required|unique:bronx_shop_tab_product_status,slug',
        'color' => 'required',
    ];

    public $hasMany = [
        'relProduct'      => [
            Product::class,
            'delete' => true,
        ],
        'relProductCount' => [
            Product::class,
            'count' => true,
        ],
    ];
}