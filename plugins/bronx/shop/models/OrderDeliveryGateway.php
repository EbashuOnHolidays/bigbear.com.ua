<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

class OrderDeliveryGateway extends Model
{
    public $table = 'bronx_shop_tab_order_delivery_gateway';

    use Validation;
    public $rules = [
        'name'  => 'required',
        'class' => 'required',
    ];

    public $hasMany = [
        'relOrder'               => [
            Order::class,
            'delete' => true,
        ],
        'relOrderCount'          => [
            Order::class,
            'count' => true,
        ],
        'relOrderDeliveryLog'    => [
            OrderDeliveryLog::class,
            'delete' => true,
        ],
        'relOrderDeliveryStatus' => [
            OrderDeliveryStatus::class,
            'delete' => true,
        ],
    ];
}