<?php namespace Bronx\Shop\Models;

use October\Rain\Database\Model;
use Bronx\User\Models\User;

class Basket extends Model
{
    public $table = 'bronx_shop_tab_basket';

    protected $fillable = [
        'user_id',
        'offer_id',
        'quantity',
    ];

    public $belongsTo = [
        'relOffer' => [
            Offer::class,
            'key' => 'offer_id',
        ],
        'relUser' => [
            User::class,
            'key' => 'user_id',
        ],
    ];
}