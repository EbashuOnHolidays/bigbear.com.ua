<?php namespace Bronx\Shop\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Bronx\Shop\Models\Product;

class PriceGenerator extends Command
{
    protected $name = 'shop:pricegenerator';
    protected $description = '...';

    public function handle()
    {
        $products = Product::get();

        $products->each(function ($product) {
            $product->price_sale = ceil($product->price_purchase * 1.3 / 10) * 10;
            $product->price_discount = ceil($product->price_purchase * 1.3 / 10) * 10;
//            $product->discounted_at = Carbon::now()->addDays(mt_rand(4, 6))->addHours(mt_rand(0,23))->addMinutes(mt_rand(0, 59));
            $product->save();
        });
    }

    protected function getArguments()
    {
        return [];
    }

    protected function getOptions()
    {
        return [];
    }
}