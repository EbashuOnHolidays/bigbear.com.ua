<?php namespace Bronx\Shop\Console;

use Bronx\Shop\Models\ProductColor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CacheGenerator extends Command
{
    protected $name = 'shop:cachegenerator';
    protected $description = '...';

    private $cacheFilter = null;
    private $cacheSlug = null;

    public function handle()
    {
        $colors = ProductColor::with(['relProduct'])
            ->get();

        $colors->each(function ($color) {
            $products = $color->relProduct()->lists('id');

            Cache::put('filter-color-' . $color->id, $products, 1);
        });

        echo "\nRET\n\n";

        $colors->each(function ($color) {
            $products = Cache::get('filter-color-' . $color->id);

            var_dump($products);
        });

        $this->output->writeln('Hello world!');
    }

    protected function getArguments()
    {
        return [];
    }

    protected function getOptions()
    {
        return [];
    }
}