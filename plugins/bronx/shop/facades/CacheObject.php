<?php namespace Bronx\Shop\Facades;

use October\Rain\Support\Facade;

class CacheObject extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'shop.cacheValue';
    }
}