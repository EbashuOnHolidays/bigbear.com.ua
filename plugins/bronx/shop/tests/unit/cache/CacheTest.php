<?php namespace Bronx\Shop\Tests\Unit\Cache;

use Bronx\Shop\Facades\Cache;
use Bronx\Shop\Models\Product;
use Bronx\Shop\Models\ProductCategory;
use PluginTestCase;
use System\Classes\PluginManager;

class CacheTest extends PluginTestCase
{
    public function setUp()
    {
        parent::setUp();

        // Get the plugin manager
        $pluginManager = PluginManager::instance();

        // Register the plugins to make features like file configuration available
        $pluginManager->registerAll(true);

        // Boot all the plugins to test with dependencies of this plugin
        $pluginManager->bootAll(true);
    }

    public function tearDown()
    {
        parent::tearDown();

        // Get the plugin manager
        $pluginManager = PluginManager::instance();

        // Ensure that plugins are registered again for the next test
        $pluginManager->unregisterAll();
    }

    public function testSaveObjectToCache()
    {
        $categories = Cache::attempt('some_key', ProductCategory::class, function ($class) {
            return $class::get();
        });
    }
}