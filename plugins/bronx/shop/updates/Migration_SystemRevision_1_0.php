<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_SystemRevision_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_system_revision', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->string('field')->nullable()->index();
            $table->string('cast')->nullable();
            $table->text('old_value')->nullable();
            $table->text('new_value')->nullable();
            $table->string('revisionable_type');
            $table->integer('revisionable_id');

            $table->timestamps();

            $table->index(['revisionable_id', 'revisionable_type'], 'revisionable_index');
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_system_revision');
    }
}