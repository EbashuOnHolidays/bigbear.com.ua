<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_OrderPaymentGateway_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_order_payment_gateway', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('class');

            $table->boolean('is_enable')->default(false);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_order_payment_gateway');
    }
}