<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_RelOrderProduct_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_rel_order_product', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('order_id');
            $table->integer('product_id');
            $table->primary(['order_id', 'product_id'], 'order_product_key');

            $table->float('price_purchase')->default(0);
            $table->float('price_markup')->default(0);
            $table->float('price_discount')->default(0);
            $table->float('price_profit')->default(0);

            $table->float('price_old')->default(0);
            $table->float('price_new')->default(0);

            $table->integer('total_quantity')->default(1);
            $table->float('total_price_purchase')->default(0);
            $table->float('total_price_markup')->default(0);
            $table->float('total_price_discount')->default(0);
            $table->float('total_price_profit')->default(0);

            $table->float('total_price_old')->default(0);
            $table->float('total_price_new')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_rel_order_product');
    }
}