<?php namespace Bronx\Shop\Updates;

use October\Rain\Support\Facades\Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class Migration_Product_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_product', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_category_id')->index();
            $table->integer('product_color_id')->index();
            $table->integer('product_group_id')->index();
            $table->integer('product_size_id')->index();
            $table->integer('product_status_id')->index();

            $table->string('name');
            $table->string('name_h1');
            $table->string('slug')->index();

            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->text('characteristic')->nullable();

            $table->float('price_purchase');
            $table->float('price_markup');
            $table->float('price_discount');
            $table->float('price_profit');

            $table->float('price_old');
            $table->float('price_new');

            $table->float('weight')->default(1);
            $table->float('volume')->default(1);

            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();

            $table->boolean('is_enable')->default(false);

            $table->integer('sort_order')->nullable();

            $table->timestamps();
            $table->timestamp('discounted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_product');
    }
}