<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_OrderPaymentStatus_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_order_payment_status', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_payment_gateway_id');

            $table->string('name');
            $table->string('code')->index();
            $table->string('color');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_order_payment_status');
    }
}