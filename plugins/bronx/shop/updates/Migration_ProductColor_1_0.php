<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_ProductColor_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_product_color', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->index()->unique();
            $table->string('color_primary');
            $table->string('color_secondary')->nullable();

            $table->integer('sort_order')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_product_color');
    }
}