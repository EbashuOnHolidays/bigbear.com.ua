<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_Basket_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_basket', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('product_id')->index();

            $table->integer('quantity')->default(1);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_basket');
    }
}