<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_OrderDeliveryLog_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_order_delivery_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('order_delivery_gateway_id');
            $table->integer('order_delivery_status_id');

            $table->text('comment')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_order_delivery_log');
    }
}