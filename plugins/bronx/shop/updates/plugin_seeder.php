<?php namespace Bronx\Shop\Updates;

use Bronx\Shop\Classes\DeliveryGateway;
use Bronx\Shop\Classes\PaymentGateway;
use Bronx\Shop\Models\ProductCategory;
use Bronx\Shop\Models\ProductColor;
use Bronx\Shop\Models\ProductGroup;
use Bronx\Shop\Models\ProductSize;
use Bronx\Shop\Models\ProductStatus;
use Bronx\Shop\Models\OrderDeliveryGateway;
use Bronx\Shop\Models\OrderPaymentGateway;
use October\Rain\Database\Updates\Seeder;

class Plugin_Seeder extends Seeder
{
    public function run()
    {
        $category = ProductCategory::create([
            'name'        => 'Медведи',
            'name_h1'     => 'Медведи маленьких, средних и больщих размеров',
            'name_filter' => 'Медведь',
        ]);

        $category->children()->create([
            'name'        => 'Маленькие мишки 50-70 см',
            'name_h1'     => 'Каталог маленьких медведей 50-70 см',
            'name_filter' => 'Маленькие мишки 50-70 см',
        ]);

        $category->children()->create([
            'name'        => 'Средние медведи 80-110 см',
            'name_h1'     => 'Каталог средних медведей 80-110 см',
            'name_filter' => 'Средние медведи 80-110 см',
        ]);

        $category->children()->create([
            'name'        => 'Большие мишки 120-170 см',
            'name_h1'     => 'Каталог больших медведей 120-170 см',
            'name_filter' => 'Большие мишки 120-170 см',
        ]);

        $category->children()->create([
            'name'        => 'Огромные медведи 180-250 см',
            'name_h1'     => 'Каталог огромных медведей 180-250 см',
            'name_filter' => 'Огромные медведи 180-250 см',
        ]);

        ProductCategory::create([
            'name'        => 'Сердце',
            'name_h1'     => 'Сердце',
            'name_filter' => 'Сердце',
        ]);

        ProductCategory::create([
            'name'        => 'Панды, Бегемоты, Зайцы',
            'name_h1'     => 'Панды, Бегемоты, Зайцы',
            'name_filter' => 'Панда, Бегемот, Заяц',
        ]);

        ProductCategory::create([
            'name'        => 'Другие животные',
            'name_h1'     => 'Другие животные',
            'name_filter' => 'Другие животные',
        ]);

        ProductCategory::create([
            'name'        => 'Плюшевые подушки',
            'name_h1'     => 'Подушки',
            'name_filter' => 'Подушки',
        ]);

        ProductCategory::create([
            'name'        => 'Детские рюкзачки',
            'name_h1'     => 'Рюкзачки',
            'name_filter' => 'Рюкзачки',
        ]);


        ProductColor::create([
            'name'          => 'Белый',
            'slug'          => 'white',
            'color_primary' => '#FFFFFF',
        ]);

        ProductColor::create([
            'name'          => 'Серый',
            'slug'          => 'gray',
            'color_primary' => '#9E9E9E',
        ]);

        ProductColor::create([
            'name'          => 'Капучино',
            'slug'          => 'cappuccino',
            'color_primary' => '#616161',
        ]);

        ProductColor::create([
            'name'            => 'Чёрно-белый',
            'slug'            => 'black-and-white',
            'color_primary'   => '#212121',
            'color_secondary' => '#FFFFFF',
        ]);

        ProductColor::create([
            'name'          => 'Молочный',
            'slug'          => 'milky',
            'color_primary' => '#FFFAE8',
        ]);

        ProductColor::create([
            'name'          => 'Персиковый (бежевый)',
            'slug'          => 'peach',
            'color_primary' => '#FFD180',
        ]);

        ProductColor::create([
            'name'          => 'Рыжий (карамельный)',
            'slug'          => 'redhead',
            'color_primary' => '#FF9800',
        ]);

        ProductColor::create([
            'name'          => 'Коричневый',
            'slug'          => 'brown',
            'color_primary' => '#795548',
        ]);

        ProductColor::create([
            'name'          => 'Шоколадный',
            'slug'          => 'chocolate',
            'color_primary' => '#3E2723',
        ]);

        ProductColor::create([
            'name'          => 'Розовый',
            'slug'          => 'pink',
            'color_primary' => '#F48FB1',
        ]);

        ProductColor::create([
            'name'          => 'Голубой',
            'slug'          => 'light-blue',
            'color_primary' => '#03A9F4',
        ]);

        ProductColor::create([
            'name'          => 'Красный',
            'slug'          => 'red',
            'color_primary' => '#C62828',
        ]);

        ProductColor::create([
            'name'          => 'Салатовый',
            'slug'          => 'light-green',
            'color_primary' => '#99ff99',
        ]);

        ProductColor::create([
            'name'          => 'Болотный',
            'slug'          => 'swamp-green',
            'color_primary' => '#698339',
        ]);


        ProductGroup::create([
            'name' => 'Мишка Рафаэль (Монти)',
        ]);

        ProductGroup::create([
            'name' => 'Медвадь Нестор',
        ]);

        ProductGroup::create([
            'name' => 'Мишка Томми',
        ]);

        ProductGroup::create([
            'name' => 'Медвадь Потап',
        ]);

        ProductGroup::create([
            'name' => 'Медвежонок Барни',
        ]);

        ProductGroup::create([
            'name' => 'Мишка Бант',
        ]);

        ProductGroup::create([
            'name' => 'Медведь Пух',
        ]);

        ProductGroup::create([
            'name' => 'Мишка Ветли',
        ]);

        ProductGroup::create([
            'name' => 'Лежачий медвежонок',
        ]);

        ProductGroup::create([
            'name' => 'Медвежонок Тини',
        ]);

        ProductGroup::create([
            'name' => 'Панда Украина',
        ]);

        ProductGroup::create([
            'name' => 'Зайчик Тотти',
        ]);

        ProductGroup::create([
            'name' => 'Кролик Роджер',
        ]);

        ProductGroup::create([
            'name' => 'Зайка Мими',
        ]);

        ProductGroup::create([
            'name' => 'Сердце',
        ]);

        ProductGroup::create([
            'name' => 'Медведь Бойд',
        ]);

        ProductGroup::create([
            'name' => 'Медведь Тедди',
        ]);

        ProductGroup::create([
            'name' => 'Мишка Физзи Мун',
        ]);

        ProductGroup::create([
            'name' => 'Панда Роналд',
        ]);

        ProductGroup::create([
            'name' => 'Зайка Кнопка',
        ]);

        ProductGroup::create([
            'name' => 'Бегемот Тоша',
        ]);

        ProductGroup::create([
            'name' => 'Собака Бассет',
        ]);


        ProductSize::create([
            'name' => '15 см',
            'slug' => '15-sm',
        ]);

        ProductSize::create([
            'name' => '20 см',
            'slug' => '20-sm',
        ]);

        ProductSize::create([
            'name' => '25 см',
            'slug' => '25-sm',
        ]);

        ProductSize::create([
            'name' => '30 см',
            'slug' => '30-sm',
        ]);

        ProductSize::create([
            'name' => '40 см',
            'slug' => '40-sm',
        ]);

        ProductSize::create([
            'name' => '45 см',
            'slug' => '45-sm',
        ]);

        ProductSize::create([
            'name' => '50 см',
            'slug' => '50-sm',
        ]);

        ProductSize::create([
            'name' => '55 см',
            'slug' => '55-sm',
        ]);

        ProductSize::create([
            'name' => '60 см',
            'slug' => '60-sm',
        ]);

        ProductSize::create([
            'name' => '65 см',
            'slug' => '65-sm',
        ]);

        ProductSize::create([
            'name' => '70 см',
            'slug' => '70-sm',
        ]);

        ProductSize::create([
            'name' => '75 см',
            'slug' => '75-sm',
        ]);

        ProductSize::create([
            'name' => '80 см',
            'slug' => '80-sm',
        ]);

        ProductSize::create([
            'name' => '85 см',
            'slug' => '85-sm',
        ]);

        ProductSize::create([
            'name' => '90 см',
            'slug' => '90-sm',
        ]);

        ProductSize::create([
            'name' => '95 см',
            'slug' => '95-sm',
        ]);

        ProductSize::create([
            'name' => '100 см',
            'slug' => '100-sm',
        ]);

        ProductSize::create([
            'name' => '110 см',
            'slug' => '110-sm',
        ]);

        ProductSize::create([
            'name' => '120 см',
            'slug' => '120-sm',
        ]);

        ProductSize::create([
            'name' => '125 см',
            'slug' => '125-sm',
        ]);

        ProductSize::create([
            'name' => '130 см',
            'slug' => '130-sm',
        ]);

        ProductSize::create([
            'name' => '140 см',
            'slug' => '140-sm',
        ]);

        ProductSize::create([
            'name' => '150 см',
            'slug' => '150-sm',
        ]);

        ProductSize::create([
            'name' => '160 см',
            'slug' => '160-sm',
        ]);

        ProductSize::create([
            'name' => '170 см',
            'slug' => '170-sm',
        ]);

        ProductSize::create([
            'name' => '180 см',
            'slug' => '180-sm',
        ]);

        ProductSize::create([
            'name' => '200 см',
            'slug' => '200-sm',
        ]);

        ProductSize::create([
            'name' => '220 см',
            'slug' => '220-sm',
        ]);

        ProductSize::create([
            'name' => '240 см',
            'slug' => '240-sm',
        ]);

        ProductSize::create([
            'name' => '250 см',
            'slug' => '250-sm',
        ]);


        ProductStatus::create([
            'name'              => 'В наличии',
            'slug'              => 'in-stock',
            'color'             => '#4CAF50',
            'is_enable_visible' => true,
            'is_enable_order'   => true,
        ]);

        ProductStatus::create([
            'name'              => 'Ожидается поставка',
            'slug'              => 'awaiting-delivery',
            'color'             => '#FF5722',
            'is_enable_visible' => true,
            'is_enable_order'   => false,
        ]);

        ProductStatus::create([
            'name'              => 'Под заказ',
            'slug'              => 'under-the-order',
            'color'             => '#FFC107',
            'is_enable_visible' => true,
            'is_enable_order'   => true,
        ]);

        ProductStatus::create([
            'name'              => 'Нет в наличии',
            'slug'              => 'not-available',
            'color'             => '#F44336',
            'is_enable_visible' => false,
            'is_enable_order'   => false,
        ]);

        ProductStatus::create([
            'name'              => 'Снято с производства',
            'slug'              => 'discontinued',
            'color'             => '#607D8B',
            'is_enable_visible' => true,
            'is_enable_order'   => false,
        ]);


        $orderDeliveryGateway = OrderDeliveryGateway::create([
            'name'  => 'Самовывоз',
            'class' => DeliveryGateway::class,
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'На обработке',
            'code'  => 'processing',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Ожидает в пункте выдачи',
            'code'  => 'waiting',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Не получено',
            'code'  => 'error',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Получено',
            'code'  => 'success',
            'color' => '#4CAF50',
        ]);


        $orderPaymentGateway = OrderPaymentGateway::create([
            'name'  => 'При получении',
            'class' => PaymentGateway::class,
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Ожидает оплаты от получателя',
            'code'  => 'wait',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Не оплачено',
            'code'  => 'error',
            'color' => '#4CAF50',
        ]);

        $orderPaymentGateway->relOrderPaymentStatus()->create([
            'name'  => 'Оплачено',
            'code'  => 'success',
            'color' => '#4CAF50',
        ]);
    }
}