<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_ProductSize_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_product_size', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->index()->unique();

            $table->integer('sort_order')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_product_size');
    }
}