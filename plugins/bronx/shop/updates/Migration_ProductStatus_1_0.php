<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_ProductStatus_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_product_status', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('name');
            $table->string('slug');
            $table->string('color');

            $table->boolean('is_enable_order')->default(true);
            $table->boolean('is_enable_visible')->default(true);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_product_status');
    }
}