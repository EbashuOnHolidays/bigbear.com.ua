<?php namespace Bronx\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_Order_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_shop_tab_order', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_delivery_gateway_id');
            $table->integer('order_delivery_status_id');
            $table->integer('order_payment_gateway_id');
            $table->integer('order_payment_status_id');
            $table->integer('user_id')->nullable();

            $table->string('customer_name');
            $table->string('customer_surname');
            $table->string('customer_phone');

            $table->string('recipient_name')->nullable();
            $table->string('recipient_surname')->nullable();
            $table->string('recipient_phone')->nullable();

            $table->string('address')->nullable();
            $table->string('comment')->nullable();

            $table->boolean('is_gift')->default(false);

            $table->integer('total_quantity')->default(1);
            $table->float('total_price_purchase')->default(0);
            $table->float('total_price_markup')->default(0);
            $table->float('total_price_discount')->default(0);
            $table->float('total_price_profit')->default(0);

            $table->float('total_price_old')->default(0);
            $table->float('total_price_new')->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_shop_tab_order');
    }
}