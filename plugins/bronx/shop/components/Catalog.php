<?php namespace Bronx\Shop\Components;

use Bronx\Shop\Models\Product;
use Bronx\Shop\Models\ProductCategory;
use Bronx\Shop\Models\SystemRevision;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class Catalog extends ComponentBase
{
    public $isProduct = false;
    public $isCategory = false;

    public $category;
    public $product;
    public $colors;
    public $recommends;

    public $parentCategories;
    public $childrenCategories;
    public $categories;
    public $filters;
    public $sortings;
    public $products;

    private $urlCanonicalPage;
    private $urlCurrentPage;
    private $urlNextPage;
    private $urlPrevPage;

    private $catalogPage;
    private $catalogSlug;
    private $catalogSlugName;
    private $productListPerPage;
    private $productListSorting;

    private $internalRequest = [];
    private $externalRequest = [];
    private $temporaryRequest = [];

    public function componentDetails()
    {
        return [
            'name'        => 'Каталог',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [
            'catalogPage'        => [
                'title' => 'Catalog page',
                'type'  => 'dropdown',
            ],
            'catalogSlug'        => [
                'title'   => 'Product list slug',
                'default' => '{{ :slug }}',
            ],
            'productListPerPage' => [
                'title'   => 'Product list per page',
                'default' => 12,
            ],
            'productListSorting' => [
                'title' => 'Product list sorting',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getCatalogPageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    public function getProductListSortingOptions()
    {
        return collect(Event::fire('bronx.shop.sort.values'))
            ->mapWithKeys(function ($item) {
                return $item;
            })->toArray();
    }

    private function beforeRun()
    {
        if ($this->isCategory == true) {
            $this->catalogPage = $this->property('catalogPage');
            $this->productListPerPage = $this->property('productListPerPage');
            $this->productListSorting = $this->property('productListSorting');

            if (!isset($this->internalRequest['page'])) {
                $this->internalRequest['page'] = Input::get('page', 1);
            }

            if (!isset($this->externalRequest['sorting'])) {
                $this->externalRequest['sorting'] = Input::get('sorting', $this->productListSorting);
            }

            $this->externalRequest['filter'] = Input::get('filter', []);

            $this->requestTrigger('filter');
        } elseif ($this->isProduct == true) {

        }
    }

    private function afterRun()
    {
        if ($this->isCategory == true) {
            $this->urlCanonicalPage = $this->controller->pageUrl($this->catalogPage, $this->controller);
            $this->urlCurrentPage = $this->generateUrl();
            $this->urlPrevPage = $this->products->previousPageUrl();
            $this->urlNextPage = $this->products->nextPageUrl();

            $this->page->urlCanonicalPage = $this->urlCanonicalPage;
            $this->page->urlCurrentPage = $this->urlCurrentPage;
            $this->page->urlPrevPage = $this->urlPrevPage;
            $this->page->urlNextPage = $this->urlNextPage;

            /*
             * Run replacer
             */
            $replace = [
                '%v_count%' => $this->products->total(),
            ];

            foreach ($replace as $key => $value) {
                $this->category->name = str_replace($key, $value, $this->category->name);
                $this->category->name_h1 = str_replace($key, $value, $this->category->name_h1);
                $this->category->name_filter = str_replace($key, $value, $this->category->name_filter);
                $this->category->meta_title = str_replace($key, $value, $this->category->meta_title);
                $this->category->meta_description = str_replace($key, $value, $this->category->meta_description);
            }

            /*
             * Set page titles
             */
            $this->page->title = $this->category->name;

            if ($this->category->meta_title != null) {
                $this->page->meta_title = $this->category->meta_title;
            }

            if ($this->category->meta_description != null) {
                $this->page->meta_description = $this->category->meta_description;
            }

            if ($this->category->meta_keywords != null) {
                $this->page->meta_keywords = $this->category->meta_keywords;
            }
        } elseif ($this->isProduct == true) {
            /*
             * Run replacer
             */
            $replace = [
                '%p_name%'   => $this->product->name,
                '%p_color%'  => $this->product->relProductColor()->first()->name,
                '%p_group%'  => $this->product->relProductGroup()->first()->name,
                '%p_size%'   => $this->product->relProductSize()->first()->name,
                '%p_status%' => $this->product->relProductStatus()->first()->name,
                '%p_weight%' => $this->product->weight,
                '%p_volume%' => $this->product->volume,
            ];

            foreach ($replace as $key => $value) {
                $this->product->name = str_replace($key, $value, $this->product->name);
                $this->product->short_description = str_replace($key, $value, $this->product->short_description);
                $this->product->description = str_replace($key, $value, $this->product->description);
                $this->product->characteristic = str_replace($key, $value, $this->product->characteristic);
                $this->product->meta_title = str_replace($key, $value, $this->product->meta_title);
                $this->product->meta_description = str_replace($key, $value, $this->product->meta_description);
                $this->product->meta_keywords = str_replace($key, $value, $this->product->meta_keywords);
            }

            /*
             * Set page titles
             */
            $this->page->title = $this->product->name;

            if ($this->product->meta_title != null) {
                $this->page->meta_title = $this->product->meta_title;
            }

            if ($this->product->meta_description != null) {
                $this->page->meta_description = $this->product->meta_description;
            }

            if ($this->product->meta_keywords != null) {
                $this->page->meta_keywords = $this->product->meta_keywords;
            }
        }
    }

    public function onRun()
    {
        /**
         * Init component
         */
        $this->catalogSlug = $this->property('catalogSlug');
        $this->catalogSlugName = $this->paramName('catalogSlug');

        /*
         * Get slug of page
         */
        $segments = explode('/', $this->catalogSlug);
        $slug = end($segments);

        /*
         * Get content indexes
         */
        $records = $this->getContentIndexes();

        if (empty($records[$slug])) {
            $slug = $this->getContentRevisions($slug);

            if (empty($records[$slug])) {
                return $this->controller->run('404');
            }
        }

        /*
         * Get type of page
         */
        switch ($records[$slug]['type']) {
            case 'product':
                $this->isProduct = true;
                break;
            case 'category':
                $this->isCategory = true;
                break;
            default:
                return $this->controller->run('404');
        }

        $this->beforeRun();

        /**
         * Run the processing
         */
        if ($this->isCategory == true) {
            /**
             * Make checks
             */
            $this->category = ProductCategory::find($records[$slug]['id']);

            // Check exist category
            if ($this->category == null) {
                return $this->controller->run('404');
            }

            // Check equal current page slug* with category slug
            if ($this->catalogSlug != $this->category->takeSlug()) {
                return Redirect::to($this->category->takeUrl($this->catalogPage, $this->controller), 302);
            }

            /**
             * Get categories for breadcrumb
             */
            $this->parentCategories = $this->category
                ->getParents();

            $this->parentCategories->each(function ($category) {
                $category->url = $category->takeUrl($this->catalogPage, $this->controller);
            });

            /**
             * Get categories for eloquent
             */
            $this->childrenCategories = $this->category
                ->getAllChildrenAndSelf();

            $this->childrenCategories->each(function ($category) {
                $category->url = $category->takeUrl($this->catalogPage, $this->controller);
            });

            /**
             * Get categories for navigation
             */
            $this->categories = $this->category
                ->getChildren();

            if ($this->categories->isEmpty()) {
                $this->categories = $this->category
                    ->getSiblingsAndSelf();
            }

            $this->categories->each(function ($category) {
                $category->url = $category->takeUrl($this->catalogPage, $this->controller);
            });

            /**
             * Build request of goods for formation queries
             */
            $this->products = Product::where('is_enable', true);

            Event::fire('bronx.shop.product.extend', [&$this->products]);

            $this->products->whereIn('product_category_id', $this->childrenCategories->lists('id'));

            /**
             * Prepare sortings
             */
            $this->sortings = collect(Event::fire('bronx.shop.sort.values'))
                ->mapWithKeys(function ($item) {
                    return $item;
                })->toArray();

            /**
             * Prepare filters
             */
            $this->filters = Event::fire('bronx.shop.filter.init', [$this->products]);

            Event::fire('bronx.shop.filter.selection', [&$this->products, $this->externalRequest]);

            /**
             * Prepare list of goods
             */
            $this->products = $this->products->paginate($this->productListPerPage, $this->internalRequest['page']);

            $this->products->each(function ($product) {
                $product->url = $product->takeUrl($this->catalogPage, $this->controller);
            });
        } else {
            if ($this->isProduct == true) {
                /**
                 * Make checks
                 */
                $this->product = Product::where('is_enable', true);

                Event::fire('bronx.shop.product.extend', [&$this->product]);

                $this->product = $this->product->find($records[$slug]['id']);

                // Check exist category
                if ($this->product == null) {
                    return $this->controller->run('404');
                }

                // Check equal current page slug* with product slug
                if ($this->catalogSlug != $this->product->takeSlug()) {
                    return Redirect::to($this->product->takeUrl($this->catalogPage, $this->controller), 301);
                }

//            $this->colors = collect($this->product->relOffer()->get())
//                ->mapWithKeys(function ($item) {
//                    $color = $item->relColor()->first();
//
//                    return [$color->slug => $color->name];
//                })->toArray();

                /**
                 * Formation category list for breadcrumb
                 */
                $this->parentCategories = $this->product
                    ->relProductCategory()
                    ->first()
                    ->getParentsAndSelf();

                $this->parentCategories->each(function ($category) {
                    $category->url = $category->takeUrl($this->catalogPage, $this->controller);
                });

//            // Подгружаем рекомендуемые товары
//            $this->recommends = Product::whereNotIn('product_id', [$this->product->id]);
//
//            Event::fire('bronx.shop.extend.offer', [&$this->recommends]);
//
//            $this->recommends = $this->recommends
//                ->inRandomOrder()
//                ->take(6)
//                ->get();
//
//            $this->recommends->each(function ($offer) {
//                $offer->relProduct->url = $offer->relProduct->takeUrl($this->catalogPage, $this->controller);
//            });
            } else {
                return $this->controller->run('404');
            }
        }

        $this->afterRun();
    }

    private function requestTrigger($trigger)
    {
        if (isset($this->temporaryRequest[$trigger])) {
            $request = $this->externalRequest[$trigger];

            if (isset($request[$this->temporaryRequest[$trigger]['name']])) {
                // Производим поиск значения в строке
                $key = array_search($this->temporaryRequest[$trigger]['value'], $request[$this->temporaryRequest[$trigger]['name']]);

                if ($key !== false) {
                    unset($request[$this->temporaryRequest[$trigger]['name']][$key]);
                } else {
                    $request[$this->temporaryRequest[$trigger]['name']][] = $this->temporaryRequest[$trigger]['value'];
                }
            } else {
                $request[$this->temporaryRequest[$trigger]['name']][] = $this->temporaryRequest[$trigger]['value'];
            }

            // Сортирует массив фильтров без привязки к числовому ключу. Предотвращает ситуации
            // использования в url параметров фильтров с большими, не последовательными числами
            sort($request[$this->temporaryRequest[$trigger]['name']]);

            $this->externalRequest[$trigger] = $request;
        }
    }

    /**
     * Формируем новый URL, возвращаем результат
     * @return array
     */
    private function refreshContent()
    {
        return [
            '#_product_headline_wrapper'   => $this->renderPartial('shop/catalog/headline'),
            '#_product_list_wrapper'       => $this->renderPartial('shop/catalog/wrapper'),
            '#_product_liveloader_wrapper' => $this->renderPartial('shop/catalog/liveloader/wrapper'),
            'urlCurrentPage'               => $this->urlCurrentPage,
            'urlPrevPage'                  => $this->urlPrevPage,
            'urlNextPage'                  => $this->urlNextPage,
        ];
    }

    private function appendContent()
    {
        return [
            '#_product_headline_wrapper'   => $this->renderPartial('shop/catalog/headline'),
            '@#_product_list_wrapper'      => $this->renderPartial('shop/catalog/wrapper'),
            '#_product_liveloader_wrapper' => $this->renderPartial('shop/catalog/liveloader/wrapper'),
            'urlCurrentPage'               => $this->urlCurrentPage,
            'urlPrevPage'                  => $this->urlPrevPage,
            'urlNextPage'                  => $this->urlNextPage,
        ];
    }

    /*
     * Генерация URL
     */
    private function generateUrl()
    {
        if ($this->externalRequest['sorting'] == $this->productListSorting) {
            $this->externalRequest['sorting'] = null;
        }

        if ($this->externalRequest['filter'] == null) {
            $this->externalRequest['filter'] = null;
        }

        $url = trim($this->urlCanonicalPage . '?' . http_build_query($this->externalRequest), '/?');

        $url = str_replace('%5B', '[', $url);
        $url = str_replace('%5D', ']', $url);

        return $url;
    }

    /*
     * AJAX Events
     */
    public function onChangeNumPage()
    {
        $this->internalRequest['page'] = Input::get('page', 1);

        $this->onRun();

        return $this->appendContent();
    }

    public function onChangeSorting()
    {
        $this->externalRequest['sorting'] = Input::get('sorting', null);
        $this->internalRequest['page'] = 1;

        $this->onRun();

        return $this->refreshContent();
    }

    public function onChangeFilter()
    {
        $this->temporaryRequest = [
            'filter' => [
                'name'  => Input::get('name', null),
                'value' => Input::get('value', null),
            ],
        ];
        $this->internalRequest['page'] = 1;

        $this->onRun();

        return $this->refreshContent();
    }

    /*
     * CACHE
     */
    private function getContentIndexes()
    {
        $products = Product::get();
        $products = $products->mapWithKeys(function ($item) {
            return [
                $item['slug'] => [
                    'id'   => $item['id'],
                    'type' => 'product',
                ],
            ];
        });

        $categories = ProductCategory::get();
        $categories = $categories->mapWithKeys(function ($item) {
            return [
                $item['slug'] => [
                    'id'   => $item['id'],
                    'type' => 'category',
                ],
            ];
        });

        return array_merge($products->all(), $categories->all());
    }

    private function getContentRevisions($slug, $id = 0)
    {
        $record = SystemRevision::where('id', '>', $id)
            ->where('field', $this->catalogSlugName)
            ->where('old_value', $slug)
            ->whereIn('revisionable_type', [
                Product::class,
                ProductCategory::class,
            ])
            ->first();

        if ($record == null) {
            return $slug;
        } else {
            return $this->getContentRevisions($record->new_value, $record->id);
        }
    }
}