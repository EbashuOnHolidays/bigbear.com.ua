<?php namespace Bronx\Shop\Components;

use Bronx\Shop\Facades\Cache;
use Bronx\Shop\Models\ProductCategory;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;

class CategoryList extends ComponentBase
{
    public $categories;

    private $catalogPage;

    private $categoryRoot;
    private $categoryGetNested;
    private $categoryPerPage;
    private $categoryOrderBy;
    private $categoryOrderSort;

    public function componentDetails()
    {
        return [
            'name'        => 'CategoryList',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [
            'catalogPage' => [
                'title' => 'Product list page',
                'type'  => 'dropdown',
            ],

            'categoryRoot'      => [
                'title' => 'Category list root',
                'type'  => 'dropdown',
            ],
            'categoryGetNested' => [
                'title'   => 'Category list get nested',
                'type'    => 'checkbox',
                'default' => false,
            ],
            'categoryPerPage'   => [
                'title'   => 'Category list per page',
                'default' => 10,
            ],
            'categoryOrderBy'   => [
                'title'   => 'Category list order by',
                'type'    => 'dropdown',
                'default' => 'id',
                'options' => [
                    'id'         => 'Record id',
                    'nest_left'  => 'Sort order',
                    'created_at' => 'Created at',
                    'updated_at' => 'Updated at',
                ],
            ],
            'categoryOrderSort' => [
                'title'   => 'Category list order sort',
                'type'    => 'dropdown',
                'default' => 'asc',
                'options' => [
                    'asc'  => 'ASC',
                    'desc' => 'DESC',
                ],
            ],
        ];
    }

    public function getCategoryRootOptions()
    {
        return [null => '- all categories -'] + ProductCategory::lists('name', 'id');
    }

    public function getCatalogPageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    private function beforeRun()
    {
        $this->catalogPage = $this->property('catalogPage');

        $this->categoryRoot = $this->property('categoryRoot');
        $this->categoryGetNested = $this->property('categoryGetNested');
        $this->categoryPerPage = $this->property('categoryPerPage');
        $this->categoryOrderBy = $this->property('categoryOrderBy');
        $this->categoryOrderSort = $this->property('categoryOrderSort');
    }

    private function getCacheKey()
    {
        return implode('-', [
            'shopCategoryList',
            $this->categoryRoot,
            $this->categoryGetNested,
            $this->categoryPerPage,
            $this->categoryOrderBy,
            $this->categoryOrderSort,
        ]);
    }

    public function onRun()
    {
        $this->beforeRun();

        $categories = ProductCategory::isEnabled()
            ->with('relImage')
            ->where('parent_id', $this->categoryRoot)
            ->orderBy($this->categoryOrderBy, $this->categoryOrderSort);

        if ($this->categoryPerPage != 0) {
            $categories->take($this->categoryPerPage);
        }

        $categories = $categories->get();

        if ($this->categoryGetNested == true) {
            $categories = $categories->toNested();
        }

        $this->categories = $this->setCategoriesLinks($categories);
    }

    private function setCategoriesLinks($categories)
    {
        return $categories->each(function ($category) {
            $category->url = $category->takeUrl($this->catalogPage, $this->controller);

            if ($this->categoryGetNested == true && $category->children != null) {
                $this->setCategoriesLinks($category->children);
            }
        });
    }
}