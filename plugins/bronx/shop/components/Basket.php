<?php namespace Bronx\Shop\Components;

use Backend\Models\User as BackendUser;
use Bronx\Shop\Models\Product as ProductModel;
use Bronx\User\Facades\Authorization;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use October\Rain\Exception\ApplicationException;
use October\Rain\Exception\ValidationException;
use October\Rain\Support\Facades\Flash;

class Basket extends ComponentBase
{
    public $products;

    public $totalCost = 0;
    public $totalQuantity = 0;
    private $totalProfit = 0;

    private $catalogPage;

    public function componentDetails()
    {
        return [
            'name'        => 'Basket',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [
            'catalogPage' => [
                'title' => 'Product single page',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getCatalogPageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    private function beforeRun()
    {
        $this->catalogPage = $this->property('catalogPage');
    }

    public function onRun()
    {
        $this->beforeRun();

        // Получем список идентификаторов вариантов и их количество
        $offerIds = Session::get('basket.product.offer_id', null);
        $offerQuantity = Session::get('basket.product.quantity', null);

        // Очищаем корзину для целостности данных в случае возможного сбоя
        $this->clearBasket();

        if ($offerIds != null && $offerQuantity != null && count($offerIds) == count($offerQuantity)) {
            // Получаем список товаров по идентификаторам вариантов
            $this->products = ProductModel::with([
                    'relOffer' => function ($offer) use ($offerIds) {
                        $offer->with('relColor')
                            ->whereIn('id', $offerIds);
                    },
                    'relSize',
                ])
                ->whereHas('relOffer', function ($offer) use ($offerIds) {
                    $offer->whereIn('id', $offerIds);
                })
                ->get();

            if ($this->products) {
                $this->products->each(function ($product) use ($offerQuantity) {
                    $product->url = $product->takeUrl($this->catalogPage, $this->controller);

                    // Получаем полную информацию о кажом варианте, делаем расчеты
                    $product->relOffer->each(function ($offer) use ($product, $offerQuantity) {
                        $offer->price_purchase = $product->price_purchase;
                        $offer->price_sale = $product->price_sale;
                        $offer->price_discount = $product->price_discount;

                        $offer->quantity = $offerQuantity[$offer->id];
                        $offer->total_cost = $offer->price_discount * $offer->quantity;
                        $offer->total_profit = ($offer->price_discount - $offer->price_purchase) * $offer->quantity;

                        $this->totalQuantity += $offer->quantity;
                        $this->totalCost += $offer->total_cost;
                        $this->totalProfit += $offer->total_profit;

                        // Добавляем вариант товара и его количество в корзину
                        Session::put('basket.product.offer_id.' . $offer->id, $offer->id);
                        Session::put('basket.product.quantity.' . $offer->id, $offer->quantity);
                    });
                });
            }
        }
    }

    // Добавление товаров в корзину
    public function onAddProduct()
    {
        $data = [
            'offer_id' => Input::get('offer_id', null),
            'quantity'   => Input::get('quantity', 1),
        ];

        $validation = Validator::make($data, [
            'offer_id' => 'required|numeric|exists:bronx_shop_tab_offer,id',
            'quantity'   => 'required|numeric|between:1,99',
        ], [
            'offer_id.required' => 'Товар не найден',
            'offer_id.numeric'  => 'Идентификатор товара имеет не верный формат',
            'offer_id.exists'   => 'Товар не найден',
            'quantity.required'   => 'Количество товара не указано',
            'quantity.numeric'    => 'Количество товара имеет не верный формат',
            'quantity.between'    => 'Количество должно быть между :min и :max',
        ]);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        if (!Session::has('basket.product.offer_id.' . $data['offer_id'])) {
            Session::put('basket.product.offer_id.' . $data['offer_id'], $data['offer_id']);
            Session::put('basket.product.quantity.' . $data['offer_id'], $data['quantity']);
        }

        if ($user = Authorization::get()) {
            $basket = $user->relBasket()
                ->where('offer_id', $data['offer_id'])
                ->first();

            if (!$basket) {
                $basket = $user->relBasket()->create([
                    'offer_id' => $data['offer_id'],
                    'quantity'   => $data['quantity'],
                ]);
            }
        }

        $this->onRun();

        // TODO: TEMP
        $orderInfo = [
            'subject'       => 'Товар добавлен в корзину',
            'timestamp'     => Carbon::now()->format('H:i:s, d.m.Y'),
            'ip_address'    => Request::getClientIp(),
            'products'      => $this->products,
            'totalQuantity' => $this->totalQuantity,
            'totalCost'     => $this->totalCost,
            'totalProfit'   => $this->totalProfit,
        ];

        $admins = BackendUser::get();
        $message = View::make('bronx.shop::basket.content', $orderInfo)->render();

        $admins->each(function ($admin) use ($message) {
            if ($admin->telegram_id != null && $admin->hasAccess('bronx.shop.send_orders')) {
                Event::fire('bronx.telegram.sendMessage', [$admin->telegram_id, $message]);
            }
        });

        return $this->refreshPartials();
    }

    // Удаление товаров из корзины
    public function onRemoveProduct()
    {
        $data = [
            'offer_id' => Input::get('offer_id', null),
        ];

        $validation = Validator::make($data, [
            'offer_id' => 'required|numeric|exists:bronx_shop_tab_offer,id',
        ], [
            'offer_id.required' => 'Товар не найден',
            'offer_id.numeric'  => 'Идентификатор товара имеет не верный формат',
            'offer_id.exists'   => 'Товар не найден',
        ]);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        if (Session::has('basket.product.offer_id.' . $data['offer_id'])) {
            Session::forget('basket.product.offer_id.' . $data['offer_id']);
            Session::forget('basket.product.quantity.' . $data['offer_id']);
        }

        if ($user = Authorization::get()) {
            $basket = $user->relBasket()
                ->where('offer_id', $data['offer_id'])
                ->first();

            if ($basket) {
                $basket->delete();
            }
        }

        $this->onRun();

        return $this->refreshPartials();
    }


    public function onBasketQuantity()
    {
        $offerIds = Session::get('basket.product.offer_id');
        $offerQuantity = Input::get('quantity');

        if (is_array($offerIds) && is_array($offerQuantity)) {
            foreach ($offerIds as $offerId) {
                $data = [
                    'quantity' => $offerQuantity[$offerId],
                ];

                $validation = Validator::make($data, [
                    'quantity' => 'required|numeric|between:1,99',
                ], [
                    'quantity.required' => 'Количество товара не указано',
                    'quantity.numeric'  => 'Количество товара имеет не верный формат',
                    'quantity.between'  => 'Количество должно быть между :min и :max',
                ]);

                if ($validation->fails()) {
                    throw new ValidationException($validation);
                }

                if (Session::has('basket.product.offer_id.' . $offerId)) {
                    Session::put('basket.product.quantity.' . $offerId, $data['quantity']);
                }

                if ($user = Authorization::get()) {
                    $basket = $user->relBasket()
                        ->where('offer_id', $offerId)
                        ->first();

                    if ($basket) {
                        $basket->update([
                            'quantity' => $data['quantity'],
                        ]);
                    }
                }
            }
        }

        $this->onRun();

        return $this->refreshPartials();
    }

    public function onRefreshPartials()
    {
        $this->onRun();

        return $this->refreshPartials();
    }

    // Отправка заказа в телеграм
    public function onSendCheckout()
    {
        if (Session::token() != Input::get('_token')) {
            throw new ApplicationException('Invalid token!');
        }

        $data = [
            'subject' => Input::get('subject', null),
            'phone'   => Input::get('phone', null),
            'comment' => Input::get('comment', null),
        ];

        $validation = Validator::make($data, [
            'subject' => 'required',
            'phone'   => 'required|between:10,19',
            'comment' => 'max:1023',
        ], [
            'subject.required' => 'Тема обязательна для заполнения',
            'phone.required'   => 'Обязателен для заполнения',
            'phone.between'    => 'Не может быть короче :min и длинее :max символов',
            'comment.max'      => 'Не может быть длинее :max символов',
        ]);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $this->onRun();

        if (!$this->products) {
            Flash::error('Корзина пуста');

            return $this->refreshPartials();
        }

        $orderInfo = [
            'subject'       => $data['subject'],
            'timestamp'     => Carbon::now()->format('H:i:s, d.m.Y'),
            'ip_address'    => Request::getClientIp(),
            'phone'         => $data['phone'],
            'comment'       => $data['comment'],
            'products'      => $this->products,
            'totalQuantity' => $this->totalQuantity,
            'totalCost'     => $this->totalCost,
            'totalProfit'   => $this->totalProfit,
        ];

        $admins = BackendUser::get();
        $message = View::make('bronx.shop::order.new.admin', $orderInfo)->render();

        $admins->each(function ($admin) use ($message) {
            if ($admin->telegram_id != null && $admin->hasAccess('bronx.shop.send_orders')) {
                Event::fire('bronx.telegram.sendMessage', [$admin->telegram_id, $message]);
            }
        });

        $user = Authorization::get();
        if ($user) {
            $message = View::make('bronx.shop::order.new.owner', $orderInfo)->render();
            Event::fire('bronx.telegram.sendMessage', [$user->telegram_id, $message]);
        }

        $this->clearBasket();

        return $this->refreshPartials([
            '#_checkout_form' => $this->renderPartial('shop/checkout/thank'),
//            'sendConversion'  => [
//                'value'          => $this->totalCost,
//                'transaction_id' => $orderId,
//            ],
        ]);
    }

    private function clearBasket()
    {
        $this->products = null;

        $this->totalCost = 0;
        $this->totalQuantity = 0;
        $this->totalProfit = 0;

        Session::forget('basket.product.offer_id');
        Session::forget('basket.product.quantity');

        Session::put('basket.total.cost', 0);
        Session::put('basket.total.quantity', 0);
    }

    private function refreshPartials(array $partial = [])
    {
        return array_merge([
            '#_sidebar_basket_toggle'  => $this->renderPartial('shop/sidebar/basket/toggle'),
            '#_sidebar_basket_wrapper' => $this->renderPartial('shop/sidebar/basket/wrapper'),
            '#_basket_list'            => $this->renderPartial('shop/basket/wrapper'),
            '#_checkout_list'          => $this->renderPartial('shop/checkout/item'),
        ], $partial);
    }
}