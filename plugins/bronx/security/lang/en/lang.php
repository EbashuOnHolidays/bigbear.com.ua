<?php

return [
    'plugin'      => [
        'name'        => 'Security',
        'description' => 'Security enhancements',
    ],
    'permissions' => [
        'tab'      => 'Bronx Security',
        'settings' => [
            'label' => 'Manage plugin settings',
        ],
    ],
    'settings'    => [
        'name'        => 'Security',
        'description' => 'Manage security settings',
        'form'        => [
            'is_enable_redirect'                     => [
                'label'   => 'Redirect enabled',
                'comment' => 'Automatically redirect all non-secure HTTP connections to secure',
            ],
            'is_enable_trusted_proxies'              => [
                'label' => 'Trusted proxies enabled',
            ],
            'is_enable_trim_string'                  => [
                'label'   => 'Trim all input strings',
                'comment' => 'This middleware automatically removes spaces in lines received from external sources',
            ],
            'is_enable_convert_empty_string_to_null' => [
                'label'   => 'Set empty strings to null',
                'comment' => 'These middleware will automatically convert any empty string fields to null',
            ],
            'redirect_code'                          => [
                'label'   => 'Redirect code',
                'options' => [
                    '301' => '301 - constant redirect',
                    '302' => '302 - temporary redirect',
                ],
            ],
        ],
    ],
];