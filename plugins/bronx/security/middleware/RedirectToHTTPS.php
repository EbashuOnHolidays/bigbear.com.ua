<?php namespace Bronx\Security\Middleware;

use Bronx\Security\Models\Settings;
use Closure;

class RedirectToHTTPS
{
    public function handle($request, Closure $next)
    {
        if (!$request->secure()) {
            return redirect()->secure($request->getRequestUri(), Settings::get('redirect_code'));
        }

        return $next($request);
    }
}