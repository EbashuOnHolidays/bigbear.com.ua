<?php namespace Bronx\Security\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Request;

class SetTrustedProxies
{
    public function handle($request, Closure $next)
    {
        $request->setTrustedProxies(
            ['127.0.0.1', $request->server->get('REMOTE_ADDR')],
            Request::HEADER_X_FORWARDED_ALL
        );

        return $next($request);
    }
}