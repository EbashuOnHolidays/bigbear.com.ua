<?php namespace Bronx\Security\Models;

use October\Rain\Database\Model;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_security_settings';
    public $settingsFields = 'fields.yaml';

    public function initSettingsData()
    {
        $this->is_enable_redirect = false;
        $this->is_enable_trusted_proxies = false;
        $this->is_enable_trim_string = false;
        $this->is_enable_convert_empty_string_to_null = false;
        $this->redirect_code = 302;
    }
}