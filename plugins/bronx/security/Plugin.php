<?php namespace Bronx\Security;

use Bronx\Security\Middleware\RedirectToHTTPS;
use Bronx\Security\Middleware\SetTrustedProxies;
use Bronx\Security\Models\Settings;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\TrimStrings;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    public $elevated = true;

    public function pluginDetails()
    {
        return [
            'name'        => 'bronx.security::lang.plugin.name',
            'description' => 'bronx.security::lang.plugin.description',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-lock',
        ];
    }

    public function boot()
    {
        if (php_sapi_name() === 'cli' || Settings::get('is_enable_redirect', false) == true) {
            $this->app[Kernel::class]
                ->prependMiddleware(RedirectToHTTPS::class);
        }

        if (php_sapi_name() === 'cli' || Settings::get('is_enable_trusted_proxies', false) == true) {
            $this->app[Kernel::class]
                ->prependMiddleware(SetTrustedProxies::class);
        }

        if (php_sapi_name() === 'cli' || Settings::get('is_enable_trim_string', false) == true) {
            $this->app[Kernel::class]
                ->prependMiddleware(TrimStrings::class);
        }

        if (php_sapi_name() === 'cli' || Settings::get('is_enable_convert_empty_string_to_null', false) == true) {
            $this->app[Kernel::class]
                ->prependMiddleware(ConvertEmptyStringsToNull::class);
        }
    }

    public function registerPermissions()
    {
        return [
            'security.settings' => [
                'tab'   => 'bronx.security::lang.permissions.tab',
                'label' => 'bronx.security::lang.permissions.settings.label',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'security_settings' => [
                'label'       => 'bronx.security::lang.settings.name',
                'description' => 'bronx.security::lang.settings.description',
                'category'    => SettingsManager::CATEGORY_SYSTEM,
                'icon'        => 'icon-user-secret',
                'class'       => Settings::class,
                'order'       => 500,
                'keywords'    => 'security redirect https',
                'permissions' => [
                    'security.settings',
                ],
            ],
        ];
    }
}