<?php namespace Bronx\NovaPoshta;

use Backend\Facades\Backend;
use Bronx\NovaPoshta\Models\Settings;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Нова Пошта',
            'description' => '...',
            'author'      => 'Bronx',
            'icon'        => 'icon-leaf'
        ];
    }

    public function boot()
    {
        Event::fire('bronx.shop.register.delivery', [
            'novaposhta'
        ]);
    }

    public function registerNavigation()
    {
        return [];

        return [
            'shop' => [
                'label'   => 'Новая почта',
                'url'     => Backend::url('bronx/shop/products'),
                'icon'    => 'icon-shopping-cart',
                'iconSvg' => 'plugins/bronx/novaposhta/assets/icon.svg',
                'order'   => 50,

                'sideMenu' => [
                    'products'   => [
                        'label' => 'Товары',
                        'url'   => Backend::url('bronx/shop/products'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'categories' => [
                        'label' => 'Категории',
                        'url'   => Backend::url('bronx/shop/categories'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                    'productstatuses' => [
                        'label' => 'Статусы товаров',
                        'url'   => Backend::url('bronx/shop/productstatuses'),
                        'icon'  => 'icon-shopping-cart',
                    ],
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Новая почта',
                'description' => '...',
                'category'    => 'Bronx',
                'icon'        => 'icon-cog',
                'class'       => Settings::class,
                'order'       => 200,
                'keywords'    => 'novaposhta notification',
            ],
        ];
    }
}