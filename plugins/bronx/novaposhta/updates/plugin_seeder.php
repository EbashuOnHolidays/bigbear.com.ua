<?php namespace Bronx\NovaPoshta\Updates;

use Bronx\Shop\Models\OrderDeliveryGateway;
use October\Rain\Database\Updates\Seeder;

class Plugin_Seeder extends Seeder
{
    public function run()
    {
        $orderDeliveryGateway = OrderDeliveryGateway::create([
            'name'  => 'Новая почта',
            'class' => 'NovaPoshta',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Нова пошта очікує надходження від відправника',
            'code'  => '1',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Видалено',
            'code'  => '2',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Номер не знайдено',
            'code'  => '3',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення у місті ХХXХ . (Статус для межобластных отправлений)',
            'code'  => '4',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення у місті ХХXХ . (Статус для услуг локал стандарт и локал экспресс - доставка в пределах города)',
            'code'  => '41',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення прямує до міста YYYY',
            'code'  => '5',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення у місті YYYY, орієнтовна доставка до ВІДДІЛЕННЯ - XXX dd - mm . Очікуйте додаткове повідомлення про прибуття',
            'code'  => '6',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Прибув на відділення',
            'code'  => '7',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Прибув на відділення',
            'code'  => '8',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення отримано',
            'code'  => '9',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення отримано % DateReceived %. Протягом доби ви одержите SMS - повідомлення про надходження грошового переказу та зможете отримати його в касі відділення «Нова пошта».',
            'code'  => '10',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення отримано % DateReceived %. Грошовий переказ видано одержувачу',
            'code'  => '11',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відправлення передано до огляду отримувачу',
            'code'  => '14',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'На шляху до одержувача',
            'code'  => '101',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відмова одержувача',
            'code'  => '102',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відмова одержувача',
            'code'  => '103',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Змінено адресу',
            'code'  => '104',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Припинено зберігання',
            'code'  => '105',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Одержано і є ТТН грошовий переказ',
            'code'  => '106',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Нараховується плата за зберігання',
            'code'  => '107',
            'color' => '#4CAF50',
        ]);

        $orderDeliveryGateway->relOrderDeliveryStatus()->create([
            'name'  => 'Відмова одержувача',
            'code'  => '108',
            'color' => '#4CAF50',
        ]);
    }
}