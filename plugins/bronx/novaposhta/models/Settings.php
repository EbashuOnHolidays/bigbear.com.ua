<?php namespace Bronx\NovaPoshta\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_telegram_settings';
    public $settingsFields = 'fields.yaml';

    use Validation;
    public $rules = [
        'novaposhta_token'  => 'required',
    ];

    public function initSettingsData()
    {
        $this->novaposhta_token = null;
    }
}