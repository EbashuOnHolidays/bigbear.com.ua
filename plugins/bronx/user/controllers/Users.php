<?php namespace Bronx\User\Controllers;

use Backend\Behaviors\FormController;
use Backend\Behaviors\ListController;
use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class Users extends Controller
{
    public $implement = [
        FormController::class,
        ListController::class,
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $bodyClass = '';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Bronx.User', 'user', 'users');
    }

    public function index()
    {
        $this->asExtension('ListController')->index();
    }

    public function create()
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->create();
    }

    public function update($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }

    public function preview($recordId = null)
    {
        $this->bodyClass = 'compact-container breadcrumb-flush';

        $this->asExtension('FormController')->update($recordId);
    }
}