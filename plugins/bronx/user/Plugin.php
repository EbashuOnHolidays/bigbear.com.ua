<?php namespace Bronx\User;

use Backend\Facades\Backend;
use Backend\Facades\BackendAuth;
use Bronx\User\Classes\AuthorizationManager;
use Bronx\User\Components\UserSession;
use Bronx\User\Facades\Authorization;
use Bronx\User\Middleware\Auth;
use Bronx\User\Models\Settings;
use Bronx\User\Models\User;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'User',
            'description' => '...',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-users',
        ];
    }

    public function register()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('Authorization', Authorization::class);

        App::singleton('user.authorization', function () {
            return AuthorizationManager::instance();
        });

        $this->app['router']->aliasMiddleware('auth', Auth::class);
    }

    public function boot()
    {
        Event::listen('bronx.telegram.webhook.contact', function ($data) {
            $user = User::where('telegram_id', $data['message']['contact']['user_id'])
                ->first();

            if ($user != null) {
                $user->update([
                    'phone' => '+' . $data['message']['contact']['phone_number'],
                ]);
            }
        });
    }

    public function registerComponents()
    {
        return [
            UserSession::class => 'userSession',
        ];
    }

    public function registerNavigation()
    {
        return [
            'user' => [
                'label'   => 'Пользователи',
                'url'     => Backend::url('bronx/user/users'),
                'icon'    => 'icon-users',
                'iconSvg' => 'plugins/bronx/user/assets/images/user.svg',
                'order'   => 50,

                'sideMenu' => [
                    'users' => [
                        'label' => 'Пользователи',
                        'url'   => Backend::url('bronx/user/users'),
                        'icon'  => 'icon-users',
                    ],
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Пользователи',
                'description' => 'Управление настройками пользователей',
                'category'    => 'Bronx',
                'icon'        => 'icon-cog',
                'class'       => Settings::class,
                'order'       => 120,
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'isAdmin' => function () {
                    return BackendAuth::check();
                },
                'isUser'  => function () {
                    return Authorization::check();
                },
                'getUser' => function () {
                    return Authorization::get();
                },
            ],
        ];
    }
}