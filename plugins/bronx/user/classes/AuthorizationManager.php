<?php namespace Bronx\User\Classes;

use Bronx\User\Models\SystemFile;
use Exception;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use October\Rain\Support\Traits\Singleton;
use Bronx\User\Models\User;

class AuthorizationManager
{
    use Singleton;
    protected static $instance;

    private $user = null;
    private $sessionKey = 'auth_key';

    public function get()
    {
        if (is_null($this->user)) {
            $this->check();
        }

        return $this->user;
    }

    /**
     * Check user authorization
     * @return bool
     */
    public function check()
    {
        if ($this->user) {
            return true;
        }

        $userSessionArray = Session::get($this->sessionKey, null);
        $userCookieArray = Cookie::get($this->sessionKey, null);

        if (!$userSessionArray && !$userCookieArray) {
            return false;
        }

        if (!is_array($userSessionArray) || !is_array($userCookieArray)) {
            return false;
        }

        if (count($userSessionArray) !== 2 || count($userCookieArray) !== 2) {
            return false;
        }

        if (array_diff_assoc($userSessionArray, $userCookieArray) !== []) {
            return false;
        }

        list($id, $persistCode) = $userSessionArray;

        $user = User::find($id);

        if (!$user) {
            return false;
        }

        if (!$user->checkPersistCode($persistCode)) {
            return false;
        }

        $this->user = $user;
        $this->user->touchLastSeenAt();

        return true;
    }

    /**
     * @param       $key
     * @param array $userCredentials
     * @param array $gatewayCredentials
     * @return User
     * @throws Exception
     */
    public function authorization($key, array $userCredentials = [], array $gatewayCredentials = [])
    {
        try {
            // Search user
            $user = User::where($key, $gatewayCredentials[$key])
                ->first();

            if (!$user) {
                // Register new user
                $user = new User;
            }

            $user->fill(array_merge($userCredentials, $gatewayCredentials));

            if (!$user->relAvatar()->first() && $user->telegram_photo_url) {
                try {
                    $file = new SystemFile;
                    $file->fromUrl($user->telegram_photo_url);

                    $user->relAvatar()->add($file);
                } catch (Exception $exception) {
                }
            }

            $user->save();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

        $sessionValue = [
            $user->getKey(),
            $user->getPersistCode(),
        ];

        Session::put($this->sessionKey, $sessionValue);
        Cookie::queue(Cookie::forever($this->sessionKey, $sessionValue));

        $user->touchLastLoginAt();

        return $this->user = $user;
    }

    public function logout()
    {
        if ($this->check()) {
            $this->user->touchLastSeenAt();
            $this->user->resetPersistCode();
        }

        $this->user = null;

        $this->reset();
    }

    /**
     * Reset user session and cookie
     */
    private function reset()
    {
        Session::forget($this->sessionKey);
        Cookie::queue(Cookie::forget($this->sessionKey));
    }
}