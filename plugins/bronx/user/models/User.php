<?php namespace Bronx\User\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Hashable;
use October\Rain\Database\Traits\Validation;
use RuntimeException;

class User extends Model
{
    public $table = 'bronx_user_tab_user';

    protected $hidden = [
        'persist_code',
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'slug',
    ];

    protected $guarded = [
        'persist_code',
    ];

    protected $jsonable = [
        'permissions',
    ];

    use Hashable;
    protected $hashable = [
        'persist_code',
    ];

    use Validation;
    public $rules = [];

    public $attachOne = [
        'relAvatar' => [
            SystemFile::class,
        ],
    ];

    public function setUrl($pageName, $controller)
    {
        $params = [
            'id'   => $this->id,
            'slug' => $this->slug,
        ];

        return $controller->pageUrl($pageName, $params);
    }

    /*
     * Events
     */
    public function afterCreate()
    {
        $this->save();
    }

    public function beforeSave()
    {
        $this->slug = str_slug(implode(' ', [
            $this->id,
            $this->first_name,
            $this->last_name,
        ]), '-');
    }

    /*
     * Authorization
     */
    public function getPersistCode()
    {
        $this->persist_code = $this->getRandomString();

        $this->forceSave();

        return $this->persist_code;
    }

    public function checkPersistCode($persistCode)
    {
        if (!$persistCode || !$this->persist_code) {
            return false;
        }

        return $persistCode == $this->persist_code;
    }

    public function resetPersistCode()
    {
        $this->persist_code = null;

        $this->forceSave();
    }

    /*
     * Touches
     */
    public function touchLastLoginAt()
    {
        $this->timestamps = false;

        $this->last_login_at = $this->freshTimestamp();
        $this->last_seen_at = $this->freshTimestamp();
        $this->save();

        $this->timestamps = true;
    }

    public function touchLastSeenAt()
    {
        if ($this->isOnline()) {
            return;
        }

        $this->timestamps = false;

        $this->last_seen_at = $this->freshTimestamp();
        $this->save();

        $this->timestamps = true;
    }

    public function getLastSeenAt()
    {
        return $this->last_seen_at ?: $this->created_at;
    }

    /*
     * Flags
     */
    public function isOnline()
    {
        return $this->getLastSeenAt() > $this->freshTimestamp()->subMinutes(5);
    }

    /*
     * Helpers
     */
    public function getRandomString($length = 42)
    {
        /*
         * Use OpenSSL (if available)
         */
        if (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length * 2);

            if ($bytes === false) {
                throw new RuntimeException('Unable to generate a random string');
            }

            return substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $length);
        }

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}