<?php namespace Bronx\User\Models;

use October\Rain\Database\Model;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_user_settings';
    public $settingsFields = 'fields.yaml';

    public function initSettingsData()
    {
    }
}