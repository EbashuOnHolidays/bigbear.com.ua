<?php namespace Bronx\User\Middleware;

use Bronx\User\Facades\Authorization;
use Closure;

class Auth
{
    public function handle($request, Closure $next)
    {
        if (!Authorization::check()) {
            return response()->json([
                'status'   => 'error',
                'response' => 'Need Authorization',
            ], 401);
        }

        return $next($request);
    }
}