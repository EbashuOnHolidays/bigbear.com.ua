<?php namespace Bronx\User\Facades;

use October\Rain\Support\Facade;

class Authorization extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'user.authorization';
    }
}