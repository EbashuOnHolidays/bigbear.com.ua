<?php namespace Bronx\User\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use October\Rain\Support\Facades\Schema;

class Migration_User_1_0 extends Migration
{
    public function up()
    {
        Schema::create('bronx_user_tab_user', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('referal_id')->nullable();

            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();

            $table->string('slug');

            $table->string('persist_code')->nullable();

            $table->timestamps();
            $table->timestamp('last_login_at')->nullable();
            $table->timestamp('last_seen_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bronx_user_tab_user');
    }
}