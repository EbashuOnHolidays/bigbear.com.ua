<?php namespace Bronx\User\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Redirect;
use Bronx\User\Facades\Authorization;

class UserSession extends ComponentBase
{
    public $user;

    private $userLogoutPage;

    public function componentDetails()
    {
        return [
            'name'        => 'UserSession',
            'description' => '...',
        ];
    }

    public function defineProperties()
    {
        return [
            'userLogoutPage' => [
                'title' => 'User logout page',
                'type'  => 'dropdown',
            ],
        ];
    }

    public function getUserLogoutPageOptions()
    {
        return Page::sortBy('baseFileName')
            ->lists('url', 'baseFileName');
    }

    private function beforeRun()
    {
        $this->userLogoutPage = $this->property('userLogoutPage');
    }

    public function onRun()
    {
        $this->beforeRun();

        $this->user = Authorization::get();
    }

    public function onLogout()
    {
        $this->beforeRun();

        Authorization::logout();

        return Redirect::to($this->controller->pageUrl($this->userLogoutPage));
    }
}