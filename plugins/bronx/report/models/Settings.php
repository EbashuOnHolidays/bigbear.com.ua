<?php namespace Bronx\Report\Models;

use October\Rain\Database\Model;
use System\Behaviors\SettingsModel;

class Settings extends Model
{
    public $implement = [
        SettingsModel::class,
    ];

    public $settingsCode = 'bronx_report_settings';
    public $settingsFields = 'fields.yaml';

    public function initSettingsData()
    {
        $this->is_disabled_in_debug = 0;
        $this->is_disabled_for_admins = 0;
        $this->disabled_exception_classes = [
            [
                'class_name' => 'October\Rain\Exception\AjaxException',
            ], [
                'class_name' => 'October\Rain\Exception\ValidationException',
            ],
        ];
    }
}