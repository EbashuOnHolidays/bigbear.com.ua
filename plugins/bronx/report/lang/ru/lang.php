<?php

return [
    'plugin'      => [
        'name'        => 'Отчет',
        'description' => 'Отчет о возникновании ошибок на сайте',
    ],
    'permissions' => [
        'tab'          => 'Bronx Report',
        'settings'     => [
            'label' => 'Управление настройками плагина',
        ],
        'notification' => [
            'label' => 'Отправка оповещений об ошибках',
        ],
    ],
    'settings'    => [
        'name'        => 'Отчеты об ошибках',
        'description' => 'Настройки оповещений',
        'form'        => [
            'is_disabled_in_debug'       => [
                'label'   => 'Отключено в режиме отладки',
                'comment' => 'Если сайт работает в режиме отладки, уведомление не будет отправлено',
            ],
            'is_disabled_for_admins'     => [
                'label'   => 'Отключено для администраторов',
                'comment' => 'Если пользователь авторизовался в backend, уведомление отправлено не будет',
            ],
            'disabled_exception_classes' => [
                'label'        => 'Отключенные уведомления',
                'commentAbove' => 'Эти исключения будут проигнорированы',
                'repeater'     => [
                    'class_name' => [
                        'label'        => 'Название класса',
                        'commentAbove' => 'Например: October\Rain\Exception\AjaxException',
                    ],
                ],
            ],
        ],
    ],
];