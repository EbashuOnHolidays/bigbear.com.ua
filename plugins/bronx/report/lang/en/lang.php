<?php

return [
    'plugin'      => [
        'name'        => 'Report',
        'description' => 'Report on the occurrence of errors on the site',
    ],
    'permissions' => [
        'tab'          => 'Bronx Report',
        'settings'     => [
            'label' => 'Manage plugin settings',
        ],
        'notification' => [
            'label' => 'Sending error notifications',
        ],
    ],
    'settings'    => [
        'name'        => 'Bug reports',
        'description' => 'Notification settings',
        'form'        => [
            'is_disabled_in_debug'       => [
                'label'   => 'Disabled in debug mode',
                'comment' => 'If the site working in debug mode, notification will not be sent',
            ],
            'is_disabled_for_admins'     => [
                'label'   => 'Disabled for admins',
                'comment' => 'If the user is authorized in the backend, notification will not be sent',
            ],
            'disabled_exception_classes' => [
                'label'        => 'Disabled notification',
                'commentAbove' => 'This exceptions will be ignored',
                'repeater'     => [
                    'class_name' => [
                        'label'        => 'Class name',
                        'commentAbove' => 'For example: October\Rain\Exception\AjaxException',
                    ],
                ],
            ],
        ],
    ],
];