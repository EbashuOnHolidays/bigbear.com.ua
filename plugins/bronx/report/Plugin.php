<?php namespace Bronx\Report;

use Backend\Facades\BackendAuth;
use Bronx\Report\Models\Settings;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    public $elevated = true;

    public $require = [
        'Bronx.Telegram',
    ];

    public function pluginDetails()
    {
        return [
            'name'        => 'bronx.report::lang.plugin.name',
            'description' => 'bronx.report::lang.plugin.description',
            'author'      => 'Alexander Shapoval',
            'icon'        => 'icon-bug',
        ];
    }

    public function boot()
    {
        App::error(function (Exception $exception) {
            $settings = Settings::instance();

            if (config('app.debug') == true && $settings->is_disabled_in_debug == true) {
                return;
            }

            if (BackendAuth::check() == true && $settings->is_disabled_for_admins == true) {
                return;
            }

            $exception_class_name = get_class($exception);
            $disabled_classes = $settings->disabled_exception_classes;

            if (!collect($disabled_classes)->flatten()->contains($exception_class_name)) {
                $message = View::make('bronx.report::report', [
                        'url'             => Request::url(),
                        'ip'              => Request::getClientIp(),
                        'user_agent'      => Request::header('User-Agent'),
                        'date_and_time'   => Carbon::now()->format('H:i:s, d.m.Y'),
                        'exception_class' => $exception_class_name,
                        'code'            => $exception->getCode(),
                        'file'            => $exception->getFile(),
                        'line'            => $exception->getLine(),
                        'error'           => $exception->getMessage(),
                        'decode_error'    => $this->decodeError('cp1251', $exception->getMessage()),
                    ])
                    ->render();

                Event::fire('bronx.telegram.sendToBackendSubscribers', ['report.notification', $message, [
                    'parse_mode' => 'HTML',
                ]]);
            }
        });
    }

    private function decodeError($charset, $inputString)
    {
        $outputString = preg_replace_callback('/\\\\u([a-f0-9]{4})/i', function ($m) {
            return chr(hexdec($m[1]) - 1072 + 224);
        }, $inputString);

        return iconv($charset, 'utf-8', $outputString);
    }

    public function registerPermissions()
    {
        return [
            'report.settings' => [
                'tab'   => 'bronx.report::lang.permissions.tab',
                'label' => 'bronx.report::lang.permissions.settings.label',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
            'report.notification' => [
                'tab'   => 'bronx.report::lang.permissions.tab',
                'label' => 'bronx.report::lang.permissions.notification.label',
                'order' => 100,
                'roles' => [
                    'developer',
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'bronx.report::lang.settings.name',
                'description' => 'bronx.report::lang.settings.description',
                'category'    => SettingsManager::CATEGORY_NOTIFICATIONS,
                'icon'        => 'icon-bug',
                'class'       => Settings::class,
                'order'       => 500,
                'keywords'    => 'report notification bug',
                'permissions' => [
                    'report.settings',
                ],
            ],
        ];
    }
}